import React, { Component, useContext, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import LoginForm from "../authentication/pages/LoginForm.js";
import HopitalCreation from "../adminstration/hospital";
import UserManagement from '../adminstration/userManagement'
import Location from "../adminstration/location";
import EICUHomePage from "../common/EICUHomePage";
import { Context } from "../../../App";
import { LocationContext } from "../../../App";
import ChangePassword from "../authentication/pages/ChangePassword.jsx";
import LocationService from '../../services/administration/LoationService';
import StasisInfo from "../adminstration/stasis/index.js";
import DischargeSummary from '../adminstration/patientInfo/dischargeSummary'
export const USER_AUTH_DATA = "userAuthData";
var cities = require('../masters/Cities.json');
function RouterApp() {
  const [context, setContext] = useContext(Context);
  const [locationContext, setLocationContext] = useContext(LocationContext);

  
  useEffect(() => {
    if (context.logindata === "") {
      var userData = localStorage.getItem(USER_AUTH_DATA);
      if (userData) {
        setContext({ logindata: JSON.parse(userData) });
      }
    }
  }, [context.logindata === ""]);

  useEffect(() => {
    var filename = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
    if (locationContext.locations === "" && filename!=="login") {
        LocationService.fetchMenus().then((resp) => {
        if (resp != undefined && resp.data !== undefined && resp.data.res !== undefined && resp.data.res !== null ) {
          setLocationContext({locations:resp.data.res});
        }else{
          setLocationContext({});
        }
      });
  
    }
  }, [locationContext.locations === ""]);


  var filename = window.location.href.substring(
    window.location.href.lastIndexOf("/") + 1
  );
  if (filename === "") {
    window.location = "/eicu/login";
  }
  return (
    <>
      <Router>
        <>
          <Switch>
            <Route path="/" exact component={LoginForm} />
            <Route path="/eicu/admin/home" exact component={EICUHomePage} />
            <Route path="/eicu/login" exact component={LoginForm} />
            <Route path="/eicu/admin/changePw" exact component={ChangePassword} />
            <Route path="/eicu/admin/discharge" exact component={DischargeSummary} />
            <Route
              path="/eicu/admin/hospital"
              exact
              component={HopitalCreation}
            />
            <Route
              path="/eicu/admin/user"
              exact
              component={UserManagement}
            />
            <Route
              path="/eicu/admin/location/:hspCode/:bedNum"
              exact
              component={Location}
            />
            <Route path="/eicu/stasis/:hospitalId" exact component={StasisInfo} />
          </Switch>
        </>
      </Router>
    </>
  );
}

export default RouterApp;
