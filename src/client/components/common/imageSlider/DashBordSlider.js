import React from 'react';
// import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
//import slide1 from "../../../assets/images/newBnr.jpg";
import slide2 from "../../../assets/images/bannerTop.jpg";
import slide3 from "../../../assets/images/bannerTop2.jpg";
import { Fade } from 'react-slideshow-image';
// const slideImages = [
//     '../../../assets/images/newBnr.jpg',
//     '../../../assets/images/bannerTop.jpg',
// ];

// const DashBordSlider = () => {
//     return (
//       <div className="dashbordSlider">
//         <Slide easing="ease">
//           <div className="each-slide">
//             <div style={{'backgroundImage': `url(${slide1})`}}>
//             </div>
//           </div>
//           <div className="each-slide">
//             <div style={{'backgroundImage': `url(${slide2})`}}>
//             </div>
//           </div>
//         </Slide>
//       </div>
//     )
// };

// export default DashBordSlider;


const DashBordSlider = () => {


    return (
        <div className="dashbordSlider">
        <div className="slide-container">
          <Fade>
            <div className="each-fade">
              <div>
                <img src={slide1} />
              </div>
            </div>
            <div className="each-fade">
              <div>
                <img src={slide2} />
              </div>
            </div>
            <div className="each-fade">
              <div>
                <img src={slide3} />
              </div>
            </div>

          </Fade>
        </div>
      </div>
    );
  };

  export default DashBordSlider;

