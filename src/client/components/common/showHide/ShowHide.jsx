import React, { Component } from 'react';
import Icon from '../icomoonIcon/Icon';
class ShowHide extends Component {

    constructor(props) {
        super(props);
        this.state = {
          visible: this.props.visible || false,
        };
        this.handleClick = this.handleClick.bind(this)
    }
    handleClick(e) {
    	this.setState({
        visible:!this.state.visible
      });
    }
        render() {
          const{
            className = 'penal',
          } = this.props
          const divClass = this.state.visible ? className : className +  ' hide'
            return <>
                <div className="showHideLayout">
                <h5  className={"show-tg-line d-flex justify-content-between m-0 "+(this.state.visible ?  'up' : 'down')}><span className="panelTitle"><Icon className="arrow" size={16} icon={this.props.icon} /> {this.props.title}</span> <span className="clickAction" onClick={this.handleClick}><Icon className="arrow" color="#203152" size={16} icon={this.state.visible ?  'minus' : 'plus'} /></span></h5>
                {/* <ReactCSSTransitionGroup transitionName="example"> */}
                  <div className={divClass}> {this.props.children}</div>
                {/* </ReactCSSTransitionGroup> */}

              </div>

            </>
        }
    }


export default ShowHide;
