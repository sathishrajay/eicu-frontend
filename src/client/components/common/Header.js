import React, {useState, useRef , useEffect, useContext} from 'react';
import Icon from '../common/icomoonIcon/Icon';
import { useHistory } from 'react-router-dom';
import Select from 'react-select';
import useOutsideClick from '../common/OutsideClick';
import { useMediaQuery } from "react-responsive";
import { Link } from 'react-router-dom';
import { Context } from "../../../App";
import Profile from '../adminstration/profile';
import AuthenticationService from '../../services/authService/AuthenticationService';
import { Alert, Col, Form, Modal } from "react-bootstrap";
export const USER_AUTH_DATA = "userAuthData";
export const USER_ID = "userId";


function Header(props) {
  const { state} = props || {
    bookingFlow : true,
    adminstration:true,
    dashBorad:true,
    rolemanagement:true
  }
/* load language type from local storage */
  let lan = localStorage.getItem('language');

  /* if language is null put language type in loclastore */
  if(lan===null|| lan===undefined){
    localStorage.setItem('language', 'en');
    lan='en';
  }
  //console.log('selected language in header js is----' + lan);
  if (lan === 'ar') {
    var { cnf, review, search } = require('../../../locale/ar/index');
  } else {
    var { cnf, review, search } = require('../../../locale/en/index');
  }
  const history = useHistory();
  const [context, setContext] = useContext(Context);
  const [activeHeader, setActiveHeader,] = useState(props.activeHeader);
  const [mainNav, setmainNav,] = useState(false);
  const [language_list, setLanguage_list,] = useState([{ label: "EN", value: "en" },{ label: "AR", value: "ar" }]);
  const ref = useRef();
  const [enableProfile, setEnableProfile] = useState(false);
  const [showpopup,setShowpopup] = useState(false)
  const [notifications,setNotifications] = useState([])
  const [dataAvail,setDataAvail] = useState(false)

  //console.log("Header :",context.logindata)

  const openMainNav = () => {
    if(mainNav){
      setmainNav(false)
    } else {
      setmainNav(true)
    }
}
useOutsideClick(ref, () => {
  if (mainNav) setmainNav(false);
});
const redirect = (e) => {
  try {
      //var domainpath = "att";
      e.preventDefault();
       if (e.target.id === 'admin') {
        history.push('/eicu/admin/hospital');
      }else if(e.target.id === 'logout'){
        localStorage.removeItem(USER_AUTH_DATA);
        localStorage.removeItem(USER_ID);
        history.push('/eicu/login');
      }
  }catch (err) {

    }
}

// useEffect(() => {
//   if(context.logindata!=="" && context.logindata._id!==undefined && !dataAvail){
//    AuthenticationService.getNotifications(context.logindata._id).then((resp) => {
//       console.log("Notifications response :"+JSON.stringify(resp));
//       if (resp != undefined && resp.data.suc) {
//             setNotifications(resp.data.res);
//             setDataAvail(true)
//             localStorage.setItem(USER_ID,context.logindata._id)
//         }
//       });
//       }else{
//         let userId=localStorage.getItem(USER_ID);
//         AuthenticationService.getNotifications(userId).then((resp) => {
//           console.log("Notifications response :"+JSON.stringify(resp));
//           if (resp != undefined && resp.data.suc) {
//                 setNotifications(resp.data.res);
//                 setDataAvail(true)
//             }
//           });
//       }
// }, [context.logindata!==undefined]);

// useEffect(() => {
//   if(dataAvail){
//   alert(dataAvail);
//    AuthenticationService.getNotifications(context.logindata._id).then((resp) => {
//       console.log("Notifications response :"+JSON.stringify(resp));
//       if (resp != undefined && resp.data.suc) {
//             setNotifications(resp.data.res);
//         }
//       });
//       }
// }, [dataAvail]);

const redirectToHome=(code)=>()=>{
  window.location = "/eicu/admin/location/"+code;
}

const loadProfile = () => {
  try {
    setEnableProfile(true);
  }catch (err) {

  }
}

const handleClose = () => {
  setEnableProfile(false);
}

const clearNotification=()=>()=>{
  let userId=localStorage.getItem(USER_ID);
  AuthenticationService.clearNotifications(userId).then((resp) => {
    console.log("Clear Notifications response :"+JSON.stringify(resp));
    if (resp != undefined && resp.data.suc) {
          setNotifications([]);
          setDataAvail(false)
      }
    });
}

const updateNotification=(obj)=>()=>{
   const payload={
    "_id":obj._id,
    "readStatus": true
   }
  AuthenticationService.updateNotifications(payload).then((resp) => {
    console.log("Clear Notifications response :"+JSON.stringify(resp));
    if (resp != undefined && resp.data.suc) {
         
      }
    });
}

/* set index for selected language display*/
let index_val=0;
if(lan==='ar'){
  index_val=1;
}else{
  index_val=0;
}

/**
 * @description:This event will when we change the language
 * @author: Rambabu
 * @param {*}
 * @function language event
 * @date : 09-09-2020
 */
  const languageEventt = (e) => {
    console.log('languageEventt-----' + e.value)
    if (e !== undefined && e !== '' && e !== null) {
      if (e.value === 'en') {
        localStorage.setItem('language', 'en');
        var { cnf,review,search } = require('../../../locale/en/index');
        props.languageData('en');
      } else {
        localStorage.setItem('language', 'ar');
        var { cnf,review,search } = require('../../../locale/ar/index');
        props.languageData('ar');
      }
    }
  }

  const onClickLink = (itm) => {
    window.location = itm;
  }
  
  const onClickNotification = (itm) => {
    setShowpopup(true)
  }

return (
  //console.log('header js language is---------',cnf.content),
    <header className={activeHeader==="dashbord" ? 'dashBordHeader':'app-header'}>
     <div className="container">
       <div className="logo float-left mt-3 ml-3">
       <Link className="app-header-logo mr-4">eMedSavers
            </Link>
       </div>
        <div className="app-header-wrapper align-items-center justify-content-between">
            <div className="hraderRight d-flex mt-4 pt-1">
            {/* <li>
                  <Link title="Alerts" onClick={() => onClickNotification()}>
                  <span>Alerts</span>
                  </Link>
            </li> */}
              <div className="wallatDetails mr-3">
                 <div className="user d-flex align-items-center"><Icon className="user mr-2" size={20} icon="user"/> <span>Hello! {context.logindata.firstName} {context.logindata.lastName}</span></div>
              </div>
              <div className="topMenu d-flex align-items-center" ref={ref} onClick={openMainNav}><Icon className="user" size={30} icon="menu"/>
                <nav className={"navigation d-flex align-items-center " + (mainNav ? 'active':'inactive')}>
                <ul className="d-flex flex-column">
                {/* <li><a id="loadProfile" href="#" onClick={loadProfile} className="navbar-brand"><Icon size={18} icon="profile" /> My Profile</a></li> */}
                 <li><a id="logout" href="#" onClick={redirect} className="navbar-brand"><Icon size={18} icon="logout" /> Log out</a></li>
                </ul>
                </nav>

              </div>
            </div>
            {enableProfile &&
             <Profile openPopup={enableProfile} handleClose={handleClose} 
              />}

        </div>
      </div>
      {showpopup &&
                                <Modal
                                    show={showpopup}
                                    onHide={() => setShowpopup(false)}
                                    backdrop="static"
                                    size="small"
                                    className="custom-dailog"
                                    keyboard={false}>
                                        <Modal.Header closeButton>
                                            <Modal.Title>Notifications</Modal.Title>
                                            <Link type="submit" color="#f44336"  size="lg" block  onClick={clearNotification()}>Clear</Link>    
                                        </Modal.Header>
                                        <Modal.Body>
                                            <div className="">
                                            {notifications.length!==0 && notifications.map((notification, idx) => (
                                              <React.Fragment key={idx}>
                                              <div>
                                                  <Form.Label>Patient Name : 
                                                    {notification.priority==="1" &&
                                                    <>
                                                   <Link type="submit" color="#f44336"  size="lg" block  onClick={redirectToHome(notification.notify_landing)}>{notification.message[0].description} Red</Link>
                                                   <Link type="submit" color="#f44336"  size="lg" block  onClick={updateNotification(notification)}><Icon className="edit" color="#ff7979" size={20} icon="delete" /></Link>
                                                 
                                                   </>
                                                    }
                                                    {notification.priority!=="1" &&
                                                     <>
                                                   <Link type="submit" color="#ffc107" size="lg" block  onClick={redirectToHome(notification.notify_ref)}>{notification.message[0].description} Yellow</Link>
                                                   <Link type="submit" color="#f44336"  size="lg" block  onClick={updateNotification(notification)}><Icon className="edit" color="#ff7979" size={20} icon="delete" /></Link>
                                                    </>
                                                    }
                                                   </Form.Label>
                                              </div>
                                              </React.Fragment>
                                            ))}
                                            </div>
                                         </Modal.Body>
                                </Modal>
                            }
    </header>

  );
};



export default Header;