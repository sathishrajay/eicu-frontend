import React, { useContext, useEffect, useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { LocationContext } from "../../../App";
//import image from "../../assets/images/home-img.jpg";
import LocationService from '../../services/administration/LoationService';
import TreeExample from '../adminstration/treeview/TreeExample';
import MainMenu from '../common/MainMenu';
import Footer from './Footer';
import Header from './Header';
var cities = require('../masters/Cities.json');


function EICUHomePage() {
  const [locationData, setLocationData] =useState();
  const [enableTree, setEnableTree] =useState(false);
  const [locationContext, setLocationContext] = useContext(LocationContext);
  const history = useHistory();
  
  useEffect(() => {
    loadLocationData();
  }, [locationContext.locations !== undefined && locationContext.locations !== ""]);


  const loadLocationData = async () => {
    let constData = LocationService.constructJsonDataForTree(locationContext.locations);
    setLocationContext({locations:constData});
    setLocationData(constData);
    setEnableTree(true);
  }


  const reDirectUrl =(data) =>{
    window.location = "/eicu/admin/location/"+data.hcode+"/"+data.bed_id;
  }

  return (
    <>
      <Header />
      <MainMenu />
      <div className="container">
        <div className="cardMainlayout pt-0">
          <Row>
            {enableTree === true && 
            <Col xs="3"><TreeExample locationData={locationData} reDirectUrl={reDirectUrl}/></Col>
            }
            <Col xs="9">
              <h1 className="pageTitle">Welcome to eMed Savers Health Services</h1>
              {/* <img className="w-100" src={image} /> */}
            </Col>
          </Row>
        </div>
      </div>
      <Footer />
    </>
  )
}
export default EICUHomePage