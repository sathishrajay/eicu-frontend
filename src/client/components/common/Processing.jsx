import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
/**
* @description:This component will work for processing popUp .
* @author: Pravas
@param {}
* @function Processing
* @date : 01-09-2020
*/
function Processing() {

return (
<>
<div className="layover">
<div className="popUpBox processing">
<div className="icons process"></div>
<strong>Please wait…</strong>
<p className="processInfo">Processing your request</p>
</div>
</div>

</>
)
}
export default Processing