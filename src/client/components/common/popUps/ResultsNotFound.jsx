import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

/**
 * @description:This function will work for Result Not Found pop Up.
 * @author: Pravas
 * @param {*}
 * @function ResultsNotFound
 * @date : 01-09-2020
 */
function ResultsNotFound() {

  return (
    <>
          <div className="layover layout">
            <div className="popUpBox resultNotFound">
              <div className="icons notFound"></div>
              <strong>Sorry No Results Found.</strong>
              <p className="m-0"><Link to="/">Modify Search</Link> or return to the <Link to="/">Home</Link> Page</p>

            </div>
          </div>

    </>
  )
}
export default ResultsNotFound