import React, { Component } from "react";
import { Link } from 'react-router-dom';
import Icon from '../icomoonIcon/Icon';
/**
 * @description:This function will add breadcrumb in respected page.
 * @author: Pravas
 * @param {*}
 * @function Breadcrumb
 * @date : 01-09-2020
 */

//=====================================
// Reducer function for userReact Hook
// which will update the state
//=====================================

function BreadcrumbFun(props){
    const { state } = props || {};
  return (
    <div className="container">
        <div className="breadcrumb hderSec align-items-center">
            <Icon className="arrowIcon ml-1 mr-1" color="#888888" size={10} icon="arrowDown" />
            {state.activePage}
            </div>
      </div>
  );
};

class Breadcrumb extends Component {
	constructor(props) {

        super(props);
        console.log('props', props)
		this.state = {
            activePage:props.activePage
		};

    }

    render() {
		return (
			<BreadcrumbFun
				state={this.state}

			/>
		);
	}

}

export default Breadcrumb;
