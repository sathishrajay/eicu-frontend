import React, { Component, useEffect, useState, useContext} from 'react';
import { Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { DropdownSubmenu, NavDropdownMenu } from "react-bootstrap-submenu";
import LocationService from '../../services/administration/LoationService';
import {Context, LocationContext } from "../../../App";
import {Link} from 'react-router-dom';
import { Col, Form, Table, Alert } from "react-bootstrap";
import Button from '../common/buttons/Button';
import Icon from '../common/icomoonIcon/Icon';
import { Typeahead } from 'react-bootstrap-typeahead'
var cities = require('../masters/Cities.json');



function MainMenuFun(props) {

  const [menuData, setMenuData] = useState();
  const [enableMenu, setEnableMenu] = useState(false);
  const [locationContext, setLocationContext] = useContext(LocationContext);
  const [notiMessage, setNotiMessage] = useState('');
  const [notiMessageShow, setNotiMessageShow] = useState(false);
  const [notiVarient, setNotiVarient] = useState();
  const [context, setContext] = useContext(Context);
  const [patientData,setPatientData] = useState([])

  const { state } = props || {}

  const [nameSearch, setNameSearch] = useState("");

    const handleNameSearch = (input, e) => {
      if(input !== undefined && input !== null && input.length >=3){
      let obj ={
        name : input
      }
      LocationService.nameSearch(obj).then((resp) => {
        if (resp != undefined && resp.data !== undefined && resp.data.suc) {
          if(resp.data.res.length > 0 && resp.data.res[0].hospital_code !== undefined && resp.data.res[0].hospital_code !== null &&
            resp.data.res[0].hospital_code !== ""){
              setPatientData(resp.data.res)
            }else{
              setNotiMessage('No data found');
              setNotiMessageShow(true);
              setNotiVarient('danger')
              hidemessage();
            }
        }
      });
    }
  }

  const hidemessage=() =>{
    setTimeout(function () {
        setNotiMessageShow(false);
        setNotiMessage('');
        setNotiVarient('');
    }, 5000);
  }


  useEffect(() => {
    loadMenuItems();
  }, [locationContext.locations === ""]);

  useEffect(() => {
    loadMenuItems();
  }, [locationContext.locations !== ""]);

  const fetchCityName = (cityCode) => {
    let cityArray = cities.filter(item => item.ccode === cityCode);
    if(cityArray.length>0){
    return cityArray[0].cname;
    }else{
      return '';
    }
  }

  const fetchStateName = (stateCode) => {
    if (stateCode === "TS") { return "Telangana" };
    if (stateCode === "AP") { return "Andhra Pradesh" };
    return stateCode;
  }

  const fetchDefaultBed = (hspData) => {
    let ocupiedBed = hspData.bed_details.filter((item) => item.status === 'occupy');
    if(ocupiedBed !== null && ocupiedBed.length > 0){
      for(let b in hspData.bed_details){
        if(hspData.bed_details[b].status !== null && hspData.bed_details[b].status=== 'occupy'){
          return "/"+hspData.bed_details[b].bed_id;
        }
      }
    }
    return "/1";
  }

  const loadMenuItems = () => {
      setMenuData(locationContext.locations);
      setEnableMenu(true);
  }


//=====================================
// This function will get the Source data
//=====================================
const handleOnPatient = (obj) => {
  if(obj!==undefined && obj.length>0){
  window.location = "/eicu/admin/location/"+obj[0].hospital_code+"/"+obj[0].bed_Num
  }
}

const onClickLink = (itm) => {
  window.location = itm;
}


  return (
/*
This Main Menu component expecing the following Props:
- active = "PageName"
Based on that link will be active
*/

<section className="menyLayout">
      <div className="container">
      <Alert className="notification" variant={notiVarient} show={notiMessageShow}
                onClose={() => setNotiMessageShow(false)}
                dismissible> {notiMessage}</Alert>
        <div className="MainMenu">
          <Navbar collapseOnSelect expand="lg"  variant="white">
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">

                  {context.logindata.permissions!==undefined && context.logindata.permissions.includes("VH") && <li>
                  <Link  className={state.active === "Hospital Creation" ? 'active':''} title="Master Services" onClick={() => onClickLink("/eicu/admin/hospital")}>
                  <span>Hospital Creation</span>
                  </Link>
                  </li>}
                  {context.logindata.permissions!==undefined && (context.logindata.permissions.includes("VU") || context.logindata.permissions.includes("VR")) &&
                   <li>
                  <Link className={state.active === "User Management" ? 'active':''} title="User Management" onClick={() => onClickLink("/eicu/admin/user")}>
                  <span>User Management</span>
                  </Link>
                  </li>
                   }
                {/* <Nav.Link href="/eicu/admin/hospital">Hospital Creation</Nav.Link>
                <Nav.Link href="/eicu/admin/user">User Management</Nav.Link> */}
                <NavDropdownMenu title="Locations" id="collasible-nav-dropdown">

                  {menuData!==undefined && menuData !== undefined && menuData !== "" && menuData.length > 0 && 
                    menuData.map((item, index) => {
                      return (
                        <DropdownSubmenu href="" title={fetchStateName(item.stateName)}>
                          {item.cities.map((city, index) => {
                            return (
                              <DropdownSubmenu href="" title={fetchCityName(city.cityName)}>
                                {city.hospitals.map((hosp, index) => {
                                  return (
                                    <NavDropdown.Item href={'/eicu/admin/location/'+hosp.code+ fetchDefaultBed(hosp)}>{hosp.name}</NavDropdown.Item>
                                  )
                                })}
                              </DropdownSubmenu>
                            )
                          })}
                        </DropdownSubmenu>
                      )
                    })
                  }
                </NavDropdownMenu>
                <Form.Row className="search-block">
                <Typeahead
                id="patienId"
                labelKey={patient => `${patient.name}`}
                onInputChange={handleNameSearch}
                options={patientData}
                onChange={patient=> handleOnPatient(patient)}
                placeholder="Patient Name"
                readOnly
                minLength={3}
                renderMenuItemChildren={(patient) => (
                  <><Icon className="search mr-2" size={10} />
                   <strong>{patient.name} </strong><br/>
                   <strong>Bed No : {patient.bed_Num}, </strong>
                   <span>{patient.hospital_name} </span>
                  </>
                )}
              />
                </Form.Row>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
      </div>
    </section>
  );
};



class MainMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: props.active

    };
  }

  render() {
    return <MainMenuFun state={this.state} />
  }
}

export default MainMenu;

