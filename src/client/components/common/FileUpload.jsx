import React,{ useState,useEffect } from 'react'
import axios from "axios";
import { Alert, Col, Form, Modal } from "react-bootstrap";
import Button from '../common/buttons/Button';
import Icon from '../common/icomoonIcon/Icon';
import api from "../../../api.json";
import InnerLoader from '../common/InnerLoader';
import Processing from '../common/Processing';
import { useFileUpload } from 'use-file-upload'

const FileUpload = (props) => {
  const [files, selectFiles] = useFileUpload()
  const [urls,setUrls] = useState({urlsList:[]})
  const [enableUrl,setEnableUrl] = useState(false)
  const [showPopup, setShowPopup] = useState(false);
  const [file, setFile] = useState();
  const [isLoadingMore, setIsLoadingMore,] = useState(false);
  const [notiMessage, setNotiMessage] = useState('');
  const [notiMessageShow, setNotiMessageShow] = useState(false);
  const [notiVarient, setNotiVarient] = useState();
  const [isLoading, setIsLoading,] = useState(false);
  const [pleasewait, setPleasewait,] = useState(false);

  useEffect(() => {
    if(props.reportUrls!==undefined){
      setUrls({urlsList:props.reportUrls});
      setEnableUrl(true);
    }
  }, [props.reportUrls]);

  const showFilePopup = (obj) =>()=> {
    setShowPopup(true);
    setFile(obj);
   }

  const deleteFile=()=>{
    setIsLoadingMore(true);
    setIsLoading(true);
    setPleasewait(true);
    const url = api.fileDelete;
    const req={"url":file.url}
    axios.post(url, req).then((response) => {
       if (response.data.suc){
        const finalUrls = urls.urlsList.filter((item) => item.name !== file.name);
        setUrls({urlsList:finalUrls});
        setShowPopup(false);
        setIsLoadingMore(false);
        setIsLoading(false);
        setPleasewait(false);
        props.sendResponse(file)
        setNotiMessageShow(true);
        setNotiMessage('Report deleted successfully..');
        setNotiVarient('success');
        window.scrollTo(0,0);
        hidemessage();
       }else{
        setShowPopup(false);
        setIsLoadingMore(false);
        setNotiMessageShow(true);
        setIsLoading(false);
        setPleasewait(false);
        setNotiMessage('Report deletion failed');
        setNotiVarient('danger');
        window.scrollTo(0,0);
        hidemessage();
       }
   });
  }

  const hidemessage=() =>{
    setTimeout(function () {
        setNotiMessageShow(false);
        setNotiMessage('');
        setNotiVarient('');
    }, 5000);
  }

  return (
    <div  className="upload-report">
      {showPopup &&
    <Modal
        show={showPopup}
        onHide={() => setShowPopup(false)}
        backdrop="static"
        size="small"
        className="custom-dailog"
        keyboard={false}>
            <Modal.Header>
                <Modal.Title>Delete Report</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="">
                    <div>
            <Form.Label className="custom-dailog-title">Are you sure you want to delete {file.name} Report ?</Form.Label>
                    </div>
                    <div className="text-center buttonGrop">
                        <Button
                        size="xs"
                        variant="outline-primary"
                        onClick={deleteFile}
                        // className={"progress-bar continue d-flex " + (isLoading ? ' progress-bar-striped progress-bar-animated' : '')}
                        disabled = {isLoading && pleasewait ? true : false } >
                          Yes
                        </Button>
                        {pleasewait ? <Processing/> : "" }
                        <Button
                            size="xs"
                            variant="outline-dark"
                            onClick={() => setShowPopup(false)}
                            type="submit"> No </Button>
                    </div>
                </div>
             </Modal.Body>
    </Modal>
}
      <button className="btn btn-outline-primary btn-small ml-2 mb-2"
        onClick={() => {
          setIsLoadingMore(true);
          // Single File Upload
          selectFiles({ multiple: true }, (files) => {
            // Note callback return an array
              files.map(({ source, name, size, file }) =>{
                const url = api.fileUploadUrl;
                const formData = new FormData();
                formData.append('image',file)
                const config = {
                    headers: {
                        'content-type': 'multipart/form-data'
                    }
                }
                axios.post(url, formData).then((response) => {
                   if (response.data.suc && response.data.res.length!==0){
                    const payload={
                      "name":name,
                      "url":response.data.res.url
                    }
                    urls.urlsList.push(payload);
                    setUrls({urlsList:urls.urlsList});
                    setEnableUrl(true);
                    setIsLoadingMore(false);
                    setNotiMessageShow(true);
                    setNotiMessage('Report uploaded successfully..');
                    setNotiVarient('success');
                    window.scrollTo(0,0);
                    hidemessage();
                   }
               });
            })
          })
        }
        
        }
      >
      {isLoadingMore ?  <InnerLoader/>:""}
     Upload Report
      </button>
      <div className="container px-0">
      <Alert className="notification" variant={notiVarient} show={notiMessageShow}
                onClose={() => setNotiMessageShow(false)}
                dismissible> {notiMessage}</Alert> 
      {urls.urlsList.length!==0  && enableUrl ? (
        urls.urlsList.map((file,idx) => (
          <React.Fragment key={idx}>
          <div>
            <span> <a href={file.url} target='_blank'>{file.name} </a>
                                    <Button
                                        size="xs"
                                        variant="link"
                                        className="btn px-0 py-0"
                                        onClick={showFilePopup(file)}
                                        type="submit"><Icon className="edit ml-1 mr-1" color="#475F7B" size={20} icon="delete" />
                                    </Button></span>
          </div>
          </React.Fragment>
        ))
      ) : (
        <span>No file selected</span>
      )}
      </div>
      
    </div>
  )
  
}

export default FileUpload