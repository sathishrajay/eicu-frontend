import React, { Component, useState } from 'react'; 
import { Container, Form, Row } from "react-bootstrap";
import { Link } from 'react-router-dom'; 
import { useHistory } from 'react-router-dom';


function OrganizationMenuFun(props) {   
  const { state} = props || {}
  const history = useHistory();
   
return (
    
        <div className="submenu">
            <ul>
            <li  className={state.active==="Hospital Creation" ? 'active':''}>
              <Link
                to="/eicu/admin/hospital" 
                title="Hospital Creation">                
                Hospital Creation
              </Link>
              </li>
            
              <li  className={state.active==="User Management" ? 'active':''}>
              <Link
                to="/eicu/admin/user" 
                title="User Management">                
                User
              </Link>
              </li>
            </ul>
          </div>
   
  );
};



class OrganizationMenu extends Component {
  constructor(props) {
  super(props);
  console.log(props)
  this.state = {
         active:props.active
          
      }; 
  }

  render() {
  return <OrganizationMenuFun state={this.state} />
}
}

export default OrganizationMenu;

