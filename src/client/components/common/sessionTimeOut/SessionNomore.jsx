import React from 'react';
import { confirmAlert } from "react-confirm-alert";
import Button from '../buttons/Button'
    //Confirmation dialog popup to delete the Branch
    export const confirmNomoreDlg = (cb) => {
        confirmAlert({
            customUI: ({ onClose }) => {
                return (
                    <>
                        <div className="custom-ui">
                          <span className="confirmD"></span>
                          <h3>Fare no more available. Please click on back to reslts</h3>
                        </div>
                        <div className="buttonGrop popUpBox text-right pb-0">
                            <Button
                                 size="md"
                                 className="refresh"
                                 variant="primary"
                                onClick={() => {
                                    cb();
                                    onClose();
                                }}>
                                Back to Results
                            </Button>
                        </div>
                    </>
                );
            }
        });
    };
