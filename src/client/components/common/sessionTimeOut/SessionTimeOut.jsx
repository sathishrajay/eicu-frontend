import React, { useState, useEffect } from 'react';
import Button from '../buttons/Button'
import { useHistory } from 'react-router-dom';
/**
 * @description:This function will work for Session time out .
 * @author: Pravas
 * @param {*}
 * @function SessionTimeOut
 * @date : 01-09-2020
 */

function SessionTimeOut(props) {
  const history = useHistory();
  const gotoResults=()=>{
    //return window.location="/att/bookings/flight/results";
  }
  const countinue=()=>{
    props.sendToReview("Review");
  }
  return (
    <>
          <div className="layover">
            <div className="popUpBox sessionTimeOut">
              <div className="icons sessionIcon"></div>
              <strong>Session has expired !</strong>
              <p>Price or availability may have changed. Refresh the screen to check it again.</p>
              <div className="d-flex seccionBtn">
              <Button onClick={() => {
                gotoResults();
              }}
                size="md"
                className="refresh"
                variant="primary"
              >REFRESH</Button>
              <br/>
              <Button onClick={() => {
                countinue();
              }}
                size="md"
                className="refresh"
                variant="primary"
              >CONTINUE BOOKING</Button>
</div>
            </div>
          </div>

    </>
  )
}
export default SessionTimeOut