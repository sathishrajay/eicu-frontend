
import React, { useState , useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Alert, Button, Col,Modal, Form} from 'react-bootstrap';
import AuthenticationService from '../../../services/authService/AuthenticationService';
import logo from "../../../assets/images/logo.svg";
import log from "loglevel";
import { Context } from "../../../../App";
import { Link } from 'react-router-dom';
import Processing from '../../common/Processing';
export const USER_AUTH_DATA = "userAuthData";

/**
 * Initial State Declaration
 */
const initialState = {
    _id:""
  
};
//=====================================
// Reducer function for userReact Hook
// which will update the state
//=====================================
const reducer = (state, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...state
            };
        case 'editHospital':
            return Object.assign(state, action.payload);
        default:
            return { ...state, [action.type]: action.payload };
    }
};


function ChangePassword(props) {
    const history = useHistory();
    const [context, setContext] = useContext(Context);
    const [state, setState] = useState({oldPwd:"",newPwd:"",cnfPwd:""})
    const [showMessage, setShowMessage] = useState("")
    const [showMessage1, setShowMessage1] = useState("")
    const [showMessage2, setShowMessage2] = useState("")
    const [commonMsg, setCommonMsg] = useState("")
    const [isLoading, setIsLoading,] = useState(false);
    const [pleasewait, setPleasewait,] = useState(false);
    const [userId,setUserId] = useState('')

    const handleChange = (e) => {
        const { id, value } = e.target
        setState(prevState => ({
            ...prevState,
            [id]: value
        }))
    }
    const handleUserLogin= (e) => {
        e.preventDefault();
        setShowMessage("");
        setShowMessage1("");
        setShowMessage2("");
        setCommonMsg("");
        if(validateChangePwd()){
            e.preventDefault();
            setIsLoading(true);
            setPleasewait(true);
                        const payload={
                        "oldPwd": state.oldPwd,
                        "newPwd": state.newPwd,
                        "cnfPwd": state.cnfPwd,
                        "usrId":context.logindata._id
                        }
                        AuthenticationService.changePassowrd(payload).then(response => {
                        if(response.data.suc){
                            setState({uname:"",pwd:""})
                            setCommonMsg("Change Password Updated Successfully...");
                            localStorage.removeItem(USER_AUTH_DATA);
                            return window.location="/eicu/login";
                        }else{
                            setIsLoading(false);
                            setPleasewait(false);
                            setCommonMsg("Change Password Updation Failed...");
                        }
                    }).catch(function (error) {
                        log.error(
                            "Exception occured in userLogin  function---" + error
                        );
                    });
        }
    }

    const validateChangePwd = (e) => {
        var paswd=/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/;
        if (state.oldPwd==="") {
            setShowMessage("Enter Old Password");
            return false;
        }else if(state.newPwd===""){
            setShowMessage1("Enter New Password");
            return false;
        }else if(state.cnfPwd===""){
            setShowMessage2("Enter Confirm Password");
            return false;
        }else if(state.cnfPwd!==state.cnfPwd){
            setCommonMsg("New Password and Confirm Password should be same");
            return false;
        }else if(!state.newPwd.match(paswd)){
            setCommonMsg("Enter valid New Password");
            return false;
        }else if(!state.cnfPwd.match(paswd)){
            setCommonMsg("Enter valid Confirm Password");
            return false;
        }else if(state.newPwd!=="" && state.newPwd.length<8){
            setCommonMsg("New passowrd minimum 8 characters");
            return false;
        }else if(state.cnfPwd!=="" && state.cnfPwd.length<8){
            setCommonMsg("Confirm passowrd minimum 8 characters");
            return false;
        }
        return true;
    }
    return (
        <div className="login-page change-pwd">
            <div className="card login-card">
                <form onSubmit={e => handleUserLogin(e)}>
                <div>
                <div class="ehealthlogo text-center">
                 <b>Change Password</b> <br/>
                 <Form.Row>
                    <Form.Text className="text-muted">
                    {commonMsg}
                    </Form.Text>
                    </Form.Row>
                </div>
                    <Form.Row>
                   <Form.Group as={Col} xs={12} controlId="oldPwd">
                       <Form.Control type="password" placeholder="Enter Password" autoComplete="off" value={state.oldPwd} onChange={handleChange} />
                        <Form.Label>Old Password<sup>*</sup></Form.Label>
                        <Form.Text className="text-muted">
                        {showMessage}
                        </Form.Text>
                        </Form.Group>
                    </Form.Row>
                <Form.Row>
                   <Form.Group as={Col} xs={12} controlId="newPwd">
                       <Form.Control type="password" placeholder="Enter Password" autoComplete="off" value={state.newPwd} onChange={handleChange} />
                        <Form.Label>New Password<sup>*</sup></Form.Label>
                        <Form.Text className="text-muted">
                        {showMessage1}
                        </Form.Text>
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                    <Form.Group as={Col} xs={12} controlId="cnfPwd">
                        <Form.Control type="password" placeholder="Enter Password" autoComplete="off" value={state.cnfPwd} onChange={handleChange} />
                        <Form.Label>Confirm Password<sup>*</sup></Form.Label>
                        <Form.Text className="text-muted">
                        {showMessage2}
                        </Form.Text>
                        </Form.Group>
                    </Form.Row>
                    <p className="mb-0"><b>Your new password must:</b> </p>
                    <div className="change-pwd--txt">
                    <p>Contain minimum 8 Characters </p>
                    <p>Contain atleast 1 Uppercase</p>
                    <p>Contain atleast 1 Numeric </p>
                    <p>Contain atleast 1 Speacial character </p>
                    </div>
                    <div className="d-flex justify-content-end">
                        <Button
                        onClick={handleUserLogin}
                        type="submit" variant="primary" size="lg" block
                        className={(isLoading ? ' progress-bar-striped progress-bar-animated' : '')}
                        disabled = {isLoading && pleasewait ? true : false } >
                          Confirm
                        </Button>
                        {pleasewait ? <Processing/> : "" }
                    </div>
                </div>
               </form></div></div>
    )
}

export default ChangePassword
