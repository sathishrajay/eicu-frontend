
import React, { useState , useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Alert, Button, Col,Modal, Form} from 'react-bootstrap';
import AuthenticationService from '../../../services/authService/AuthenticationService';
import logo from "../../../assets/images/logo.svg";
import log from "loglevel";
import { Context } from "../../../../App";
import { Link } from 'react-router-dom';
import Processing from '../../common/Processing';
import { LocationContext } from "../../../../App";
import LocationService from "../../../services/administration/LoationService"
export const USER_AUTH_DATA = "userAuthData";


/**
 * Initial State Declaration
 */
const initialState = {
    _id:""
  
};
//=====================================
// Reducer function for userReact Hook
// which will update the state
//=====================================
const reducer = (state, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...state
            };
        case 'editHospital':
            return Object.assign(state, action.payload);
        default:
            return { ...state, [action.type]: action.payload };
    }
};


function LoginForm(props) {
    const history = useHistory();
    const [context, setContext] = useContext(Context);
    const [state, setState] = useState({uname: "", pwd: "" , email:"",oldPwd:"",newPwd:"",cnfPwd:""})
    const [enableLogin, setEnableLogin] = useState(true)
    const [showMessage, setShowMessage] = useState("")
    const [showMessage1, setShowMessage1] = useState("")
    const [showMessage2, setShowMessage2] = useState("")
    const [commonMsg, setCommonMsg] = useState("")
    const [isLoading, setIsLoading,] = useState(false);
    const [pleasewait, setPleasewait,] = useState(false);
    const [showPopup, setShowPopup] = useState(false);
    const [userId,setUserId] = useState('')
    const [locationContext, setLocationContext] = useContext(LocationContext);

    const handleChange = (e) => {
        const { id, value } = e.target
        setState(prevState => ({
            ...prevState,
            [id]: value
        }))
    }

    const validateUser = (e) => {
        if (state.userName !== "" && state.password !== "") {
            return true;
        }
    }
    const handleForgotPw=()=>{
        setEnableLogin(false)
    }

    const cancelForPw=()=>{
        setEnableLogin(true)
    }

    const handleForgotEmail = (e) => {
        e.preventDefault();
        if (state.email!==""){
                    AuthenticationService.userForgotPw(state.email).then(response => {
                    if(response.data.suc){
                        window.location="/eicu/login";
                    }
                }).catch(function (error) {
                    log.error(
                        "Exception occured in userLogin  function---" + error
                    );
                });
            
           }else{
            setState({ showUserName: "Enter Email " })
           }
           }


    const handleUserLogin= async (e) => {
        e.preventDefault();
        setShowMessage("");
        setShowMessage1("");
        setShowMessage2("");
        setCommonMsg("");
        if (state.uname !=="" && state.pwd !== "") {
                if (validateUser) {
                    const payload={
                        "userName":state.uname,
                        "password":state.pwd
                    }
                    AuthenticationService.userLogin(payload).then(async response => {
                    if(response.data.suc){
                        setUserId(response.data.res._id);
                        if(response.data.res.isVerified){
                            setContext({logindata:response.data.res})
                            localStorage.setItem(USER_AUTH_DATA, JSON.stringify(response.data.res));
                             await LocationService.fetchMenus().then((resp) => {
                                if (resp != undefined && resp.data !== undefined && resp.data.res !== undefined && resp.data.res !== null) {
                                    setLocationContext({locations:resp.data.res});
                                }
                              });
                            history.push("/eicu/admin/home");
                        }else{
                            setContext({logindata:response.data.res})
                            localStorage.setItem(USER_AUTH_DATA, JSON.stringify(response.data.res));
                            history.push("/eicu/admin/changePw");
                        }
                    }else{
                        setShowMessage2("Incorrect User name and Password");
                    }
                }).catch(function (error) {
                    log.error(
                        "Exception occured in userLogin  function---" + error
                    );
                });
            }
        } else {
            if (state.uname === "") {
                setShowMessage1("Enter User Name");
            }
            if (state.pwd === "") {
                setShowMessage("Enter Password");
            }
        }
    }

    
    return (
        <div className="login-page">
            <div className="card login-card">
                <div class="ehealthlogo text-center">
                {enableLogin &&
                <b>eMedSavers Login</b>
                }
                </div>
                <form onSubmit={e => handleUserLogin(e)}>
                    <div className="error">
                    </div>
                    <div>
                    {enableLogin &&
                    <div>
                    <Form.Row>
                        <Form.Group as={Col} xs={12} controlId="uname">
                            <Form.Control type="text" placeholder="Enter User name" autoComplete="off" value={state.uname} onChange={handleChange} />
                            <Form.Label>User name</Form.Label>

                            <Form.Text className="text-muted">
                                {showMessage1}
                            </Form.Text>
                        </Form.Group>
                        <Form.Group as={Col} xs={12} controlId="pwd">
                            <Form.Control type="password" placeholder="Enter Password" autoComplete="off" value={state.pwd} onChange={handleChange} />
                            <Form.Label>Password</Form.Label>
                            <Form.Text className="text-muted">
                              {showMessage}
                            </Form.Text>
                        </Form.Group>
                        <Form.Text className="text-muted">
                              {showMessage2}
                            </Form.Text>
                    </Form.Row>
                    <div className="text-right">
                        <Button type="submit" variant="primary" size="lg" block onClick={handleUserLogin}>
                            Login
                        </Button>
                    </div>
                    <div className="text-right mt-2">
                    <Link type="submit" variant="primary" size="lg" block 
                     onClick={handleForgotPw}>
                     Forgot Password
                    </Link>
                    </div>
                    </div>
                      }
                    {!enableLogin &&
                    <div>
                    <Form.Row>
                        <Form.Group as={Col} xs={12} controlId="email">
                            <Form.Control type="text" placeholder="Enter Email id" autoComplete="off" value={state.email} onChange={handleChange} />
                            <Form.Label>Email <sup>*</sup></Form.Label>
                            <Form.Text className="text-muted">
                                {state.showUserName}
                            </Form.Text>
                        </Form.Group>
                    </Form.Row>
                    <div className="text-right">
                        <Button type="submit" variant="primary" size="xs" className="mr-2"  onClick={handleForgotEmail}>
                            Submit
                        </Button>
                        <Button type="submit"  size="xs"
                            variant="outline-dark" onClick={cancelForPw}>
                            Cancel
                        </Button>
                    </div>
                    </div>
                    }
            </div> </form>
 
    </div>
    </div>
    )
}

export default LoginForm
