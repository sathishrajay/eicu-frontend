import React, { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
import "react-datepicker/dist/react-datepicker.css";
import Button from '../../../common/buttons/Button';
import moment from 'moment';

function VitalGraph(props) {
    const [heartRateAry, setHeartRateAry]= useState([0,0,0,0,0,0,0]);
    const [spo2Ary, setSpo2Ary]= useState([0,0,0,0,0,0,0]);
    const [respirotaryAry, setRespirotaryAry]= useState([0,0,0,0,0,0,0]);
    const [bpSystolicAry, setBpSystolicAry]= useState([0,0,0,0,0,0,0]);
    const [bpDiastolicAry, setBpDiastolicAry]= useState([0,0,0,0,0,0,0]);
    const [mapAry, setMapAry]= useState([0,0,0,0,0,0,0]);
    const [lblAry, setLblAry]= useState([0,0,0,0,0,0,0]);
    const [data, setData] = useState();

    useEffect(() => {
        if(props.patientData!==undefined && props.patientData.patientId!==undefined){
         setData(props.patientData);
        }
      window.scrollTo(0, 150);
    }, [props.patientData]);

    const MINUTE_MS = 2000;

      useEffect(() => {
        const interval = setInterval(() => {
            refreshData();
        }, MINUTE_MS);
        return () => clearInterval(interval); // This represents the unmount function, in which you need to clear your interval to prevent memory leaks.
    }, []);

    const randomGen = () => {
        var min = 1;
        var max = 200;
        var rand = min + (Math.random() * (max - min));
        return rand.toFixed(0);
    }

    const randomGenBp = () => {
        var min = 120
        var max = 130;
        var rand = min + (Math.random() * (max - min));
        return rand.toFixed(0);
    }

    let state = {
        labels: lblAry,
        datasets: [
            {
                label: 'Heart Rate',
                fill: false,
                backgroundColor: 'rgba(75,192,192,1)',
                borderColor: 'rgb(124,252,0)',
                borderWidth: 2,
                lineTension: 0,
                data: heartRateAry
            },
            {
                label: 'Spo2',
                fill: false,
                backgroundColor: 'rgba(75,192,192,1)',
                borderColor: 'rgb(0,0,255)',
                borderWidth: 2,
                lineTension: 0,
                data: spo2Ary
            } ,
            {
                label: 'Respiratory Rate',
                fill: false,
                backgroundColor: 'rgba(75,192,192,1)',
                borderColor: 'rgb(255,255,0)',
                borderWidth: 2,
                lineTension: 0,
                data: respirotaryAry
            },
            {
                label: 'BP Systolic',
                fill: false,
                backgroundColor: 'rgba(75,192,192,1)',
                borderColor: 'rgb(255,0,0)',
                borderWidth: 2,
                lineTension: 0,
                data: bpSystolicAry
            },
            {
                label: 'BP Diastolic',
                fill: false,
                backgroundColor: 'rgba(75,192,192,1)',
                borderColor: 'rgb(255,0,0)',
                borderWidth: 2,
                lineTension: 0,
                data: bpDiastolicAry
            },
            {
                label: 'MAP',
                fill: false,
                backgroundColor: 'rgba(75,192,192,1)',
                borderColor: 'rgb(165,42,42)',
                borderWidth: 2,
                lineTension: 0,
                data: mapAry
            },
            {
                label: '',
                fill: false,
                backgroundColor: 'rgb(255,255,255)',
                borderColor: 'rgb(255,255,255)',
                borderWidth: 0,
                lineTension: 0,
                data: lblAry
            },
        ]
    }

    const refreshData = () => {
        let temp=[1];
        let heartRt=[];
        let spo2=[];
        let respirotary=[];
        let bpSystolic=[];
        let bpDiastolic=[];
        let mapData=[];
        let lbl=[];
        for(let t of temp){
        
            let hrtRt = heartRateAry;
            hrtRt=heartRateAry.slice(1);
            hrtRt.push(randomGen());
            heartRt = hrtRt;

            let lb = lblAry;
            lb=lblAry.slice(1);
            lb.push( moment(new Date()).format('hh:mm:ss'));
            lbl = lb;

            let sp2 = spo2Ary;
            sp2 =spo2Ary.slice(1);
            sp2.push(randomGen());
            spo2 = sp2;

            let resp = respirotaryAry;
            resp =respirotaryAry.slice(1);
            resp.push(randomGen());
            respirotary = resp;

            let bpS = bpSystolicAry;
            bpS =bpSystolicAry.slice(1);
            let randSys =randomGenBp();
            bpS.push(randSys);
            bpSystolic = bpS;

            let bpD = bpDiastolicAry;
            bpD =bpDiastolicAry.slice(1);
            let randDia =randomGenBp();
            bpD.push(randDia);
            bpDiastolic = bpD;

            let mp = mapAry;
            mp =mapAry.slice(1);
            mp.push(randomGen());
            mapData = mp;
        }
        setHeartRateAry(heartRt);
        setSpo2Ary(spo2);
        setRespirotaryAry(respirotary);
        setBpSystolicAry(bpSystolic);
        setBpDiastolicAry(bpDiastolic);
        setMapAry(mapData);
        setLblAry(lbl);
    }

    return (
        <>

        {data!==undefined &&
                    <div className="row">
                        <div className="col-sm-6">Patient Name : <b>{data.name}</b></div>
                        <div className="col-sm-6 text-right">MR Number : <b>{data.mrNum}</b></div>
                    </div>
                }
                <hr/>
            <div className="cardMainlayout pt-0 min-height">
                <Button
                    size="xs"
                    variant="outline-dark"
                    type="submit"
                    onClick={() => refreshData()}> Refresh</Button>
                <Line
                    data={state}
                    options={{
                        title: {
                            display: true,
                            text: 'Vital Data',
                            fontSize: 20
                        },
                        legend: {
                            display: true,
                            position: 'right'
                        }
                    }}
                />
            </div>

        </>)
}

export default VitalGraph

