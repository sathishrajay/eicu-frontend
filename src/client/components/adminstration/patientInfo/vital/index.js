import React, { useContext, useEffect, useReducer, useState } from 'react';
import { useHistory } from 'react-router-dom';
import VitalGraph from './VitalGraph';
const initialState = {
    info: ""
};

const reducer = (state, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...state, info: ""
            };
        default:
            return { ...state, [action.type]: action.payload };
    }
};

function VitalData(props) {
    const [state, dispatch] = useReducer(reducer, initialState);
    const history = useHistory();

    return (
        <>
            <div className="container">
                <h1> Vital </h1>
                <VitalGraph openPopup={true}/>
            </div>
        </>
    )
}

export default VitalData