import React from 'react';
import CriticalCareService from '../../../../services/administration/CriticalCareService'
/**
 * @description: CRUD operations for Criticalcare
 * @author: Praveen Varma
 * @param {*}
 * @function Branch
 * @date : 10-10-2020
 */



// ========================================================================
//  Branch Create Operation
//  This will create Branch and save the details 
// ========================================================================
export const creation = async (obj) => {
    let finalRes=[];
    await CriticalCareService.saveCriticalCareInfo(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}



// ========================================================================
//  Branch Update Operation
//  This function will call the service to delete the selected branch
// ========================================================================
export const update = async (obj) => {
    let finalRes = {};
    await CriticalCareService.updateCriticalCareInfo(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

// ========================================================================
//  Branch Update Operation
//  This function will call the service to delete the selected branch
// ========================================================================
export const retrieveCriticalCare = async (obj) => {
    let finalRes = {};
    await CriticalCareService.retriveCriticalCareInfo(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

export const fetchHistoryBasedId = async (obj) => {
    let finalRes = {};
    await CriticalCareService.fetchHistoryBasedId(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}
