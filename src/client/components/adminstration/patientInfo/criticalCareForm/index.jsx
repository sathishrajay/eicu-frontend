import React, { useContext, useEffect, useReducer, useState } from 'react';
import { Alert, Col, Form, Table } from "react-bootstrap";
import "react-datepicker/dist/react-datepicker.css";
import { useHistory } from 'react-router-dom';
import { Context } from "../../../../../App";
import Button from '../../../common/buttons/Button';
import DateUtils from '../../../commonUtilities/DateUtils';
import CriticalcareDialog from './CriticalcareDialog';
import { creation, fetchHistoryBasedId, retrieveCriticalCare } from './operations';
import InnerLoader from '../../../common/InnerLoader';
export const USER_AUTH_DATA = "userAuthData";

/**
 * @description:This function will creaet Hospitales and this will provide the ADD,EDIT and Remove functionalities
 * @author: Praveen
 * @param {*}
 * @function CriticalCare
 * @date : 10-10-2020
 */

/**
 * Initial State Declaration
 */
const initialState = {
    id: undefined, fk_id: "", diagnosis: "", todaysIssues: "", pendingIssues: "", clinicalTrends: "", gcs: "", pupils: "", oxygenTheraphy: "",
    invasive: false, niv: false, textAreaMd: "", temp: "", hr: "", bp: "", vasopressors: "", ph: "", pco2: "", po2: "", be: "", hco3: "",
    lac: "", inputOutput: "", inputOutputTwo: "", rrt: false, rrtText: "", balance: "", cumBalance: "", motionPassed: false, perabdomen: "",
    f: false, fText:"", a: false, aText:"", s: false, sText:"", t: false, tText:"", h: false, hText:"", u: false, uText:"", g: false, gText:"",  textArea1: "",
    painScore: "", cultureAndAensitivity: "", antibioticsPerday: "", invasiveLines: "", planForTheDay: "",
    responseNotes: []
};
//=====================================
// Reducer function for userReact Hook
// which will update the state
//=====================================
const reducer = (state, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...state, diagnosis: "", todaysIssues: "", pendingIssues: "", clinicalTrends: "", gcs: "", pupils: "", oxygenTheraphy: "",
                invasive: false, niv: false, textAreaMd: "", temp: "", hr: "", bp: "", vasopressors: "", ph: "", pco2: "", po2: "", be: "", hco3: "",
                lac: "", inputOutput: "", inputOutputTwo: "", rrt: false, rrtText: "", balance: "", cumBalance: "", motionPassed: false, perabdomen: "",
                f: false, fText:"", a: false, aText:"", s: false, sText:"", t: false, tText:"", h: false, hText:"", u: false, uText:"", g: false, gText:"", textAreaPlan: "",
                painScore: "", cultureAndAensitivity: "", antibioticsPerday: "", invasiveLines: "", planForTheDay: "",
            };
        case 'editCriticalCare':
            return Object.assign(state, action.payload);
        default:
            return { ...state, [action.type]: action.payload };
    }
};



function CriticalCare(props) {
    const [state, dispatch] = useReducer(reducer, initialState);
    const history = useHistory();
    const [notiMessage, setNotiMessage] = useState('');
    const [notiMessageShow, setNotiMessageShow] = useState(false);
    const [notiVarient, setNotiVarient] = useState();
    const [rrtEnable, setrrtEnable] = useState(false);
    const [currentDate, setCurrentData] = useState(new Date().toLocaleString());
    const [context, setContext] = useContext(Context);
    const [data, setData] = useState()
    const [userId, setUserId] = useState()
    const [showPopup, setShowPopup] = useState(false);
    const [ccPK, setCcPK] = useState(null);
    const [viewHistory, setViewHistory] = useState();
    const [isLoadingMore, setIsLoadingMore,] = useState(false);
    const [cumBalCal, setCumBalCal] = useState(0);
    const [enableBalance, setEnableBalance] = useState(true);

    useEffect(() => {
        dispatch({ type: "st", payload: true })
        window.scrollTo(0, 0);
    }, []);

    useEffect(() => {
    }, [cumBalCal]);

    useEffect(() => {
    }, [enableBalance]);

    useEffect(() => {
        if(props.showMsg){
          setNotiMessageShow(true);
          setNotiMessage('Drug Updated Successfully..');
          setNotiVarient('success');
          hidemessage();
        }
      }, [props.showMsg]);

    useEffect(() => {
        if (context.logindata._id !== undefined) {
            setUserId(context.logindata._id);
        }
    }, [context.logindata._id]);


    useEffect(() => {
        if (props.patientData !== undefined) {
            setData(props.patientData);
            const payload = { "patientId": props.patientData.patientId }
            setIsLoadingMore(true);
            retrieveCriticalCare(payload).then((response) => {
                if (response !== null && response.suc) {
                    if (response.res.cc_results !== undefined && response.res.cc_results !== null) {
                        setValuesToCare(response.res);                        
                        setCcPK(response.res.cc_results._id);
                        dispatch({ type: "responseNotes", payload: response.res.cc_history });
                        setIsLoadingMore(false);
                    }
                }else{
                    setIsLoadingMore(false);
                }
            });
        }
    }, [props.patientData]);


    const setValuesToCare = (careDataInfo) => {
        if (careDataInfo !== undefined && careDataInfo !== null) {
            let careData = careDataInfo.cc_results;
            dispatch({ type: 'diagnosis', payload: careData.diagnosis });
            dispatch({ type: 'todaysIssues', payload: careData.todaysIssues });
            dispatch({ type: 'pendingIssues', payload: careData.pendingIssues });
            dispatch({ type: 'clinicalTrends', payload: careData.clinicalTrends });
            dispatch({ type: 'gcs', payload: careData.gcs });
            dispatch({ type: 'pupils', payload: careData.pupils });
            dispatch({ type: 'oxygenTheraphy', payload: careData.oxygenTheraphy });
            dispatch({ type: 'invasive', payload: careData.invasive });
            dispatch({ type: 'niv', payload: careData.niv });
            dispatch({ type: 'textAreaMd', payload: careData.textAreaMd });
            dispatch({ type: 'temp', payload: careData.temp });
            dispatch({ type: 'hr', payload: careData.hr });
            dispatch({ type: 'bp', payload: careData.bp });
            dispatch({ type: 'vasopressors', payload: careData.vasopressors });
            dispatch({ type: 'ph', payload: careData.ph });
            dispatch({ type: 'pco2', payload: careData.pco2 });
            dispatch({ type: 'po2', payload: careData.po2 });
            dispatch({ type: 'be', payload: careData.be });
            dispatch({ type: 'hco3', payload: careData.hco3 });
            dispatch({ type: 'lac', payload: careData.lac });
            dispatch({ type: 'inputOutput', payload: careData.inputOutput });
            dispatch({ type: 'inputOutputTwo', payload: careData.inputOutputTwo });
            dispatch({ type: 'rrt', payload: careData.rrt });
            dispatch({ type: 'rrtText', payload: careData.rrt === true ? careData.rrtText : "" });
            dispatch({ type: 'balance', payload: careData.balance });
            dispatch({ type: 'cumBalance', payload: careData.cumBalance });
            if(careData.cumBalance !== null && careData.cumBalance !== "" && careData.cumBalance !== ''){
                setCumBalCal(Number(careData.cumBalance));
            }else{
                setCumBalCal(0);
            }
            dispatch({ type: 'motionPassed', payload: careData.motionPassed });
            dispatch({ type: 'perabdomen', payload: careData.perabdomen });
            dispatch({ type: 'f', payload: careData.f });
            dispatch({ type: 'a', payload: careData.a });
            dispatch({ type: 's', payload: careData.s });
            dispatch({ type: 't', payload: careData.t });
            dispatch({ type: 'h', payload: careData.h });
            dispatch({ type: 'u', payload: careData.u });
            dispatch({ type: 'g', payload: careData.g });
            dispatch({ type: 'fText', payload: careData.fText !== undefined ? careData.fText :"" });
            dispatch({ type: 'aText', payload: careData.aText !== undefined ? careData.aText :"" });
            dispatch({ type: 'sText', payload: careData.sText !== undefined ? careData.sText :"" });
            dispatch({ type: 'tText', payload: careData.tText !== undefined ? careData.tText :"" });
            dispatch({ type: 'hText', payload: careData.hText !== undefined ? careData.hText :"" });
            dispatch({ type: 'uText', payload: careData.uText !== undefined ? careData.uText :"" });
            dispatch({ type: 'gText', payload: careData.gText !== undefined ? careData.gText :"" });
            dispatch({ type: 'textArea1', payload: careData.textArea1 });
            dispatch({ type: 'painScore', payload: careData.painScore });
            dispatch({ type: 'cultureAndAensitivity', payload: careData.cultureAndAensitivity });
            dispatch({ type: 'antibioticsPerday', payload: careData.antibioticsPerday });
            dispatch({ type: 'invasiveLines', payload: careData.invasiveLines });
            dispatch({ type: 'planForTheDay', payload: careData.planForTheDay });
            if (careData.rrt) {
                setrrtEnable(true);
            } else {
                setrrtEnable(false);
            }
            if(careData.cumBalDate === undefined || (careData.cumBalDate !== undefined && careData.cumBalDate === null)){
                setEnableBalance(true);
                dispatch({ type: 'inputOutput', payload: "" });
                dispatch({ type: 'inputOutputTwo', payload: "" });
                dispatch({ type: 'balance', payload: "" });
            }else{
                setEnableBalance(false)
            }
            window.scrollTo(0, 0);
        }
    }
    // OnChange event we will call this and update the state
    const handleChange = (e) => {
        setNotiMessageShow(false);
        setNotiMessage('');
        setNotiVarient('');
        const { id, value } = e.target;
        if (id === "rrt") {
            if (state.rrt) {
                setrrtEnable(false);
                dispatch({ type: 'rrt', payload: false })
            } else {
                setrrtEnable(true);
                dispatch({ type: 'rrt', payload: true })
            }
        } else if (id === "f") {
            if (state.f) {
                dispatch({ type: 'f', payload: false })
            } else {
                dispatch({ type: 'f', payload: true })
                dispatch({ type: 'fText', payload: "" })
            }
        }
        else if (id === "a") {
            if (state.a) {
                dispatch({ type: 'a', payload: false })
            } else {
                dispatch({ type: 'a', payload: true })
                dispatch({ type: 'aText', payload: "" })
            }
        } else if (id === "s") {
            if (state.s) {
                dispatch({ type: 's', payload: false })
            } else {
                dispatch({ type: 's', payload: true })
                dispatch({ type: 'sText', payload: "" })
            }
        } else if (id === "t") {
            if (state.t) {
                dispatch({ type: 't', payload: false })
            } else {
                dispatch({ type: 't', payload: true })
                dispatch({ type: 'tText', payload: "" })
            }
        } else if (id === "h") {
            if (state.h) {
                dispatch({ type: 'h', payload: false })
            } else {
                dispatch({ type: 'h', payload: true })
                dispatch({ type: 'hText', payload: "" })
            }
        } else if (id === "u") {
            if (state.u) {
                dispatch({ type: 'u', payload: false })
            } else {
                dispatch({ type: 'u', payload: true })
                dispatch({ type: 'uText', payload: "" })
            }
        } else if (id === "g") {
            if (state.g) {
                dispatch({ type: 'g', payload: false })
            } else {
                dispatch({ type: 'g', payload: true })
                dispatch({ type: 'gText', payload: "" })
            }
        } else if (id === "niv") {
            if (state.niv) {
                dispatch({ type: 'niv', payload: false })
            } else {
                dispatch({ type: 'niv', payload: true })
            }
        } else if (id === "invasive") {
            if (state.invasive) {
                dispatch({ type: 'invasive', payload: false })
            } else {
                dispatch({ type: 'invasive', payload: true })
            }
        } else if (id === "motionPassed") {
            if (state.motionPassed) {
                dispatch({ type: 'motionPassed', payload: false })
            } else {
                dispatch({ type: 'motionPassed', payload: true })
            }
        } else if(id === "inputOutput" || id==="inputOutputTwo"){
            if(id === "inputOutput" && value !== null && value !=="" && value!==''){
                var pattern = new RegExp(/^[0-9\b]+$/);
                if (!pattern.test(value)) {
                    //dispatch({ type: id, payload: "" });
                    return;
                }
            }
            if(id === "inputOutputTwo" && value !== null && value !=="" && value!==''){
                var pattern = new RegExp(/^[0-9\b]+$/);
                if (!pattern.test(value)) {
                   // dispatch({ type: id, payload: "" });
                    return;
                }
            }
            dispatch({ type: id, payload: value });
            let balCount =0;
            if(id === "inputOutput"){
                if(value !== null && value !== "" && value !== ''){
                  balCount = Number(value);  
                }
                if(state.inputOutputTwo !== null && state.inputOutputTwo !== "" && state.inputOutputTwo !== ''){
                    balCount = balCount -Number(state.inputOutputTwo);
                } 
            }else if(id === "inputOutputTwo"){
                if(state.inputOutput !== null && state.inputOutput !== "" && state.inputOutput !== ''){
                    balCount = Number(state.inputOutput);
                }
                if(value !== null && value !== "" && value !== ''){
                    balCount = balCount - Number(value);  
                }
            }    
            dispatch({ type: 'balance', payload: balCount });
            if(cumBalCal !== null && cumBalCal !=="" && cumBalCal !== ''){
                let cmB = Number(cumBalCal) + balCount;
                dispatch({ type: 'cumBalance', payload: cmB });
            }else{
                dispatch({ type: 'cumBalance', payload: balCount });
            }
        } else {
            dispatch({ type: id, payload: value });
        }
    }

    //Save Critical care info

    const saveCriticalCare = () => {
        setIsLoadingMore(true);
        let valid=true;
        let msg ="";
        if(!state.f && (state.fText=== undefined || (state.fText === null || state.fText === "" || state.fText === ''))){
            valid = false;
            msg= "F";
        }
        if(!state.a && (state.aText === undefined || (state.aText === null || state.aText === "" || state.aText === ''))){
            valid = false;
            msg = msg === "" ? "A" :(msg+", A");
        }
        if(!state.s && (state.sText === undefined || (state.sText === null || state.sText === "" || state.sText === ''))){
            valid = false;
            msg = msg === "" ? "S" :(msg+", S");
        }
        if(!state.t && (state.tText === undefined || (state.tText === null || state.tText === "" || state.tText === ''))){
            valid = false;
            msg = msg === "" ? "T" :(msg+", T");
        }
        if(!state.h && (state.hText === undefined || (state.hText === null || state.hText === "" || state.hText === ''))){
            valid = false;
            msg = msg === "" ? "H" :(msg+", H");
        }
        if(!state.u && (state.uText === undefined || (state.uText === null || state.uText === "" || state.uText === ''))){
            valid = false;
            msg = msg === "" ? "U" :(msg+", U");
        }
        if(!state.g && (state.gText === undefined || (state.gText === null || state.gText === "" || state.gText === ''))){
            valid = false;
            msg = msg === "" ? "G" :(msg+", G");
        }
        if(valid){
            saveCriticalCareData(); 
        }else{
            setNotiMessage(msg+' Text must be provided')
            setNotiMessageShow(true);
            setNotiVarient('danger')
            window.scrollTo(0,0);
        }
        setIsLoadingMore(false);
    }
    const saveCriticalCareData = async () => {
        setIsLoadingMore(true);
        var userData = localStorage.getItem(USER_AUTH_DATA);
        let loggedInData = JSON.parse(userData); 
        let logedInUser  = loggedInData.firstName +" "+ loggedInData.lastName

        let emptyFields = false;
        if((state.inputOutput === null || state.inputOutput === "" || state.inputOutput === '')
        && (state.inputOutputTwo === null || state.inputOutputTwo === "" || state.inputOutputTwo === '')){
            emptyFields = true;
        }
        const payload = {
            ccPid: ccPK !== null ? ccPK : "",
            patientName : data !== undefined ? data.name : "",
            age : data !== undefined ? data.age : "",
            sex : data !== undefined ? data.sex : "",
            mrNum : data !== undefined ? data.mrNum : "",
            email:data !== undefined ? data.email : "",
            fk_patient_id: data.patientId,
            date: new Date(),
            diagnosis: state.diagnosis,
            todaysIssues: state.todaysIssues,
            pendingIssues: state.pendingIssues,
            clinicalTrends: state.clinicalTrends,
            gcs: state.gcs,
            pupils: state.pupils,
            oxygenTheraphy: state.oxygenTheraphy,
            invasive: state.invasive,
            niv: state.niv,
            textAreaMd: state.textAreaMd,
            temp: state.temp,
            hr: state.hr,
            bp: state.bp,
            vasopressors: state.vasopressors,
            ph: state.ph,
            pco2: state.pco2,
            po2: state.po2,
            be: state.be,
            hco3: state.hco3,
            lac: state.lac,
            inputOutput: state.inputOutput,
            inputOutputTwo : state.inputOutputTwo,
            rrt: rrtEnable,
            rrtText: state.rrtText,
            balance: state.balance,
            cumBalance: state.cumBalance,
            motionPassed: state.motionPassed,
            perabdomen: state.perabdomen,
            f: state.f, a: state.a, s: state.s, t: state.t, h: state.h, u: state.u, g: state.g,
            fText:state.fText,
            aText:state.aText,
            sText:state.sText,
            tText:state.tText,
            hText:state.hText,
            uText:state.uText,
            gText:state.gText,
            textArea1: state.textArea1,
            painScore: state.painScore,
            cultureAndAensitivity: state.cultureAndAensitivity,
            antibioticsPerday: state.antibioticsPerday,
            invasiveLines: state.invasiveLines,
            planForTheDay: state.planForTheDay,
            createdBy: userId,
            modifiedBy: userId,
            logedInUser : logedInUser,
            cumBalDate : (enableBalance && !emptyFields) ? new Date() : null
        }
        await creation(payload).then((resp) => {
            if (resp.suc) {
                dispatch({ type: 'showMsg', payload: true })
                setNotiMessageShow(true);
                setNotiMessage('Critical Care Notes saved successfully');
                setNotiVarient('success')
                window.scrollTo(0,0);
                hidemessage();
                const payload = { "patientId": data.patientId }
                retrieveCriticalCare(payload).then((response) => {
                    if (response !== null && response.suc) {
                        if (response.res.cc_results !== undefined && response.res.cc_results !== null) {
                            setValuesToCare(response.res);
                            setCcPK(response.res.cc_results._id);
                            dispatch({ type: "responseNotes", payload: response.res.cc_history })
                            window.scrollTo(0,0);
                        }
                    }
                });
            } else {
                setNotiMessageShow(true);
                setNotiMessage('Critical care persistance Failed');
                setNotiVarient('danger')
                window.scrollTo(0,0);
                hidemessage();
            }
        });
        setIsLoadingMore(false);
    }

    const hidemessage=() =>{
        setTimeout(function () {
            setNotiMessageShow(false);
            setNotiMessage('');
            setNotiVarient('');
        }, 5000);
      }


    const viewDoc = async (notes) => {
        setIsLoadingMore(true);
        fetchHistoryBasedId(notes).then((response) => {
            if (response !== null && response.suc) {
                if (response.res !== undefined && response.res !== null) {
                    setViewHistory(response.res);
                    setShowPopup(true);
                    setIsLoadingMore(false);
                }
            }
        });
    }

    const handleClose = () => {
        setShowPopup(false);
    }



    //===================
    // Template Part
    //===================

    return (
        <>
         {data!==undefined &&
                    <div className="row">
                        <div className="col-sm-6">Patient Name : <b>{data.name}</b></div>
                        <div className="col-sm-6 text-right">MR Number : <b>{data.mrNum}</b></div>
                       
                    </div>
                }
                <hr/>
            <div className="container">
            {isLoadingMore ?  <InnerLoader/>:""}
                 <Alert className="notification" variant={notiVarient} show={notiMessageShow}
                onClose={() => setNotiMessageShow(false)}
                dismissible> {notiMessage}</Alert>

                <div className="cardMainlayout pt-0">
                    <div className="contentArea pt-0">
                        <Form.Row>
                            <Form.Group as={Col} xs={12}>
                                <div><span><b>Date : {currentDate}</b></span></div>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={12} controlId="diagnosis">
                                <Form.Control as="textarea" rows="2" placeholder="Diagnosis" value={state.diagnosis} onChange={handleChange} />
                                <Form.Label>Diagnosis </Form.Label>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row>
                            <Form.Group as={Col} xs={12} controlId="todaysIssues">
                                <Form.Control as="textarea" rows="2" placeholder="Today's Issues" value={state.todaysIssues} onChange={handleChange} />
                                <Form.Label>Today's Issues</Form.Label>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row>
                            <Form.Group as={Col} xs={12} controlId="pendingIssues">
                                <Form.Control as="textarea" rows="2" placeholder="Pending Issues" value={state.pendingIssues} onChange={handleChange} />
                                <Form.Label>Pending Issues</Form.Label>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row>
                            <Form.Group as={Col} xs={12} controlId="clinicalTrends">
                                <Form.Control as="textarea" rows="2" placeholder="Pending Issues" value={state.clinicalTrends} onChange={handleChange} />
                                <Form.Label>Clinical Trends</Form.Label>
                            </Form.Group>
                        </Form.Row>


                        <div><span><b>CNS</b></span></div>
                        <Form.Row className="ccGrid" xs={12}>
                            <Form.Group as={Col} xs={6} controlId="gcs">
                                <Form.Control type="text" placeholder="GCS" value={state.gcs} onChange={handleChange} />
                                <Form.Label>GCS</Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={6} controlId="pupils">
                                <Form.Control type="text" placeholder="Pupils" value={state.pupils} onChange={handleChange} />
                                <Form.Label>Pupils</Form.Label>
                            </Form.Group>
                        </Form.Row>

                        <div><span><b>RS</b></span></div>
                        <Form.Row className="ccGrid">
                            <Form.Group as={Col} xs={6} controlId="oxygenTheraphy">
                                <Form.Control type="text" placeholder="Oxygen Theraphy" value={state.oxygenTheraphy} onChange={handleChange} />
                                <Form.Label>Oxygen Theraphy</Form.Label>
                            </Form.Group>
                        </Form.Row>


                        <div><span><b>Mode</b></span></div>
                        <Form.Row className="ccGrid">
                            <Form.Group as={Col} xs={3} controlId="st">
                                <Form.Check
                                    type="checkbox"
                                    id="invasive"
                                    label="Invasive"
                                    checked={state.invasive}
                                    value={state.invasive} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="st">
                                <Form.Check
                                    type="checkbox"
                                    id="niv"
                                    label="NIV"
                                    checked={state.niv}
                                    value={state.niv} onChange={handleChange}
                                />
                            </Form.Group>

                            <Form.Group as={Col} xs={6} controlId="textAreaMd">
                                <Form.Control as="textarea" rows="2" placeholder="Text Area" value={state.textAreaMd} onChange={handleChange} />
                                <Form.Label>Text Area</Form.Label>
                            </Form.Group>
                        </Form.Row>


                        <div><span><b>CVS</b></span></div>
                        <Form.Row className="ccGrid">
                            <Form.Group as={Col} xs={3} controlId="temp">
                                <Form.Control type="text" placeholder="Temp F" value={state.temp} onChange={handleChange} />
                                <Form.Label>Temp F</Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={3} controlId="hr">
                                <Form.Control type="text" placeholder="HR/min" value={state.hr} onChange={handleChange} />
                                <Form.Label>HR/min</Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={3} controlId="bp">
                                <Form.Control type="text" placeholder="BP mmHg" value={state.bp} onChange={handleChange} />
                                <Form.Label>BP mmHg</Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={3} controlId="vasopressors">
                                <Form.Control type="text" placeholder="Vasopressors" value={state.vasopressors} onChange={handleChange} />
                                <Form.Label>Vasopressors</Form.Label>
                            </Form.Group>
                        </Form.Row>

                        <div><span><b>ABG</b></span></div>
                        <Form.Row className="ccGrid">
                            <Form.Group as={Col} xs={3} controlId="ph">
                                <Form.Control type="text" placeholder="PH" value={state.ph} onChange={handleChange} />
                                <Form.Label>PH</Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={3} controlId="pco2">
                                <Form.Control type="text" placeholder="Pco2 mmHg" value={state.pco2} onChange={handleChange} />
                                <Form.Label>Pco2 mmHg </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="po2">
                                <Form.Control type="text" placeholder="Po2 mmHg" value={state.po2} onChange={handleChange} />
                                <Form.Label>Po2 mmHg</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="be">
                                <Form.Control type="text" placeholder="BE" value={state.be} onChange={handleChange} />
                                <Form.Label>BE </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="hco3" className="mt-3">
                                <Form.Control type="text" placeholder="Hco3" value={state.hco3} onChange={handleChange} />
                                <Form.Label>Hco3</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="lac" className="mt-3">
                                <Form.Control type="text" placeholder="Lac" value={state.lac} onChange={handleChange} />
                                <Form.Label>Lac </Form.Label>
                            </Form.Group>
                        </Form.Row>

                        <div><span><b>Renal</b></span></div>
                        <Form.Row className="ccGrid">
                            <Form.Group as={Col} xs={3} controlId="inputOutput">
                                <Form.Control type="text" disabled={!enableBalance} placeholder="Input" value={state.inputOutput} onChange={handleChange} />
                                <Form.Label>Input</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="inputOutputTwo">
                                <Form.Control type="text" disabled={!enableBalance} placeholder="Output" value={state.inputOutputTwo} onChange={handleChange} />
                                <Form.Label>Output</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="balance">
                                <Form.Control type="text" placeholder="Balance" disabled value={state.balance} onChange={handleChange} />
                                <Form.Label>Balance</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="cumBalance">
                                <Form.Control type="text" placeholder="Cum Balance" disabled value={state.cumBalance} onChange={handleChange} />
                                <Form.Label>Cum Balance</Form.Label>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row className="ccGrid">
                            <Form.Group as={Col} xs={3} controlId="st">
                                <Form.Check
                                    type="checkbox"
                                    id="rrt"
                                    label="RRT"
                                    checked={state.rrt}
                                    value={state.rrt} onChange={handleChange}
                                />
                            </Form.Group>
                            {rrtEnable &&
                                <Form.Group as={Col} xs={9} controlId="rrtText">
                                    <Form.Control as="textarea" rows="2" placeholder="RRT Text" value={state.rrtText} onChange={handleChange} />
                                    <Form.Label>RRT Text </Form.Label>
                                </Form.Group>
                            }
                        </Form.Row>

                        <Form.Row className="ccGrid">         
                            <Form.Group as={Col} xs={3} controlId="st">
                                <Form.Check
                                    type="checkbox"
                                    id="motionPassed"
                                    label="Motion Passed"
                                    checked={state.motionPassed}
                                    value={state.motionPassed} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={9} controlId="perabdomen">
                                <Form.Control type="text" placeholder="Perabdomen" value={state.perabdomen} onChange={handleChange} />
                                <Form.Label>Perabdomen</Form.Label>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row className="ccGrid">
                            <Form.Group as={Col} xs={1} controlId="st">
                                <Form.Check
                                    type="checkbox"
                                    id="f"
                                    label="F"
                                    checked={state.f}
                                    value={state.f} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="fText">
                                <Form.Control type="text" placeholder="Text (F)" disabled={state.f} value={state.fText} onChange={handleChange} />
                                <Form.Label>Text (F)</Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={1} controlId="st">
                                <Form.Check
                                    type="checkbox"
                                    id="a"
                                    label="A"
                                    checked={state.a}
                                    value={state.a} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="aText">
                                <Form.Control type="text" placeholder="Text (A)" disabled={state.a} value={state.aText} onChange={handleChange} />
                                <Form.Label>Text (A)</Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={1} controlId="st">
                                <Form.Check
                                    type="checkbox"
                                    id="s"
                                    label="S"
                                    checked={state.s}
                                    value={state.s} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="sText">
                                <Form.Control type="text" placeholder="Text (S)" disabled={state.s} value={state.sText} onChange={handleChange} />
                                <Form.Label>Text (S)</Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={1} controlId="st" className="mt-4">
                                <Form.Check
                                    type="checkbox"
                                    id="t"
                                    label="T"
                                    checked={state.t}
                                    value={state.t} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="tText" className="mt-4">
                                <Form.Control type="text" placeholder="Text (T)" disabled={state.t} value={state.tText} onChange={handleChange} />
                                <Form.Label>Text (T)</Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={1} controlId="st" className="mt-4">
                                <Form.Check
                                    type="checkbox"
                                    id="h"
                                    label="H"
                                    checked={state.h}
                                    value={state.h} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="hText" className="mt-4">
                                <Form.Control type="text" placeholder="Text (H)" disabled={state.h} value={state.hText} onChange={handleChange} />
                                <Form.Label>Text (H)</Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={1} controlId="st" className="mt-4">
                                <Form.Check
                                    type="checkbox"
                                    id="u"
                                    label="U"
                                    checked={state.u}
                                    value={state.u} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="uText" className="mt-4">
                                <Form.Control type="text" placeholder="Text (U)" disabled={state.u} value={state.uText} onChange={handleChange} />
                                <Form.Label>Text (U)</Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={1} controlId="st" className="mt-4">
                                <Form.Check
                                    type="checkbox"
                                    id="g"
                                    label="G"
                                    checked={state.g}
                                    value={state.g} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="gText" className="mt-4">
                                <Form.Control type="text" placeholder="Text (G)" disabled={state.g} value={state.gText} onChange={handleChange} />
                                <Form.Label>Text (G)</Form.Label>
                            </Form.Group>

                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={6} controlId="textArea1">
                                <Form.Control type="text" placeholder="Text Area" value={state.textArea1} onChange={handleChange} />
                                <Form.Label>Text Area</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={6} controlId="painScore">
                                <Form.Control type="text" placeholder="Pain Score" value={state.painScore} onChange={handleChange} />
                                <Form.Label>Pain Score</Form.Label>
                            </Form.Group>
                        </Form.Row>

                        <div><span><b>Infection</b></span></div>
                        <Form.Row className="ccGrid">
                            <Form.Group as={Col} xs={4} controlId="cultureAndAensitivity">
                                <Form.Control type="text" placeholder="Culture And Aensitivity" value={state.cultureAndAensitivity} onChange={handleChange} />
                                <Form.Label>Culture And Sensitivity</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={4} controlId="antibioticsPerday">
                                <Form.Control type="text" placeholder="Anti biotics per day" value={state.antibioticsPerday} onChange={handleChange} />
                                <Form.Label>Antibiotics/day</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={4} controlId="invasiveLines">
                                <Form.Control type="text" placeholder="Invasive Lines" value={state.invasiveLines} onChange={handleChange} />
                                <Form.Label>Invasive Lines</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={12} controlId="planForTheDay">
                                <Form.Control as="textarea" rows="6" placeholder="Plan for the day" value={state.planForTheDay} onChange={handleChange} />
                                <Form.Label>Plan for the day</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        
                        <div className="text-right buttonGrop pb-0">
                            <Button
                                size="xs"
                                variant="outline-dark"
                                type="submit"
                                loading={state.loadOfice}
                            > Reset</Button>
                             {!state.enabelUpdate && context.logindata.permissions!==undefined && context.logindata.permissions.includes("ACCN") && (
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    loading={state.loadOfice}
                                    onClick={saveCriticalCare}
                                    type="submit"
                                > Send</Button>
                            )}
                            {state.enabelUpdate && (
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    loading={state.loadOfice}
                                    type="submit"
                                > Update</Button>
                            )}

                        </div>
                    </div>
                </div>

                <div className="cardMainlayout p-0 cc-history">
                    <Table hover>
                        <thead>
                            <tr>
                                <th>Critical Care History</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>
                            {state.responseNotes.length > 0 && state.responseNotes.map((notes, index) => (
                                <React.Fragment key={index}>
                                    <tr>
                                        <td>
                                            <span>{"Critical Cate Notes ("+DateUtils.convertToDateFormate(notes.created_date)+")"}</span>
                                        </td>
                                        <td>
                                            <span><Button
                                                size="xs"
                                                variant="outline-primary"
                                                onClick={() => viewDoc(notes)}
                                                type="submit"> View </Button></span>
                                        </td>
                                    </tr>
                                    {showPopup &&
                                        <CriticalcareDialog openPopup={showPopup} handleClose={handleClose} 
                                        viewHistory = {viewHistory} patientMinimalInfo= {data}/>
                                    }
                                </React.Fragment>
                            ))}
                            {state.responseNotes.length === 0 && (
                                <div>
                                    No Records found
                                </div>
                            )
                            }
                        </tbody>
                    </Table>
                </div>
            </div>
        </>
    )
}
export default CriticalCare