import React, { useEffect, useReducer, useState } from 'react';
import { Col, Form, Modal, Table } from "react-bootstrap";
import "react-datepicker/dist/react-datepicker.css";
import { useHistory } from 'react-router-dom';
import Button from '../../../common/buttons/Button';
import DatePicker from 'react-datepicker';
import Icon from '../../../common/icomoonIcon/Icon';
import DateUtils from '../../../commonUtilities/DateUtils';

const initialState = {
    id: undefined, fk_id: "", createdDate: "", diagnosis: "", todaysIssues: "", pendingIssues: "", clinicalTrends: "", gcs: "", pupils: "", oxygenTheraphy: "",
    invasive: false, niv: false, textAreaMd: "", temp: "", hr: "", bp: "", vasopressors: "", ph: "", pco2: "", po2: "", be: "", hco3: "",
    lac: "", inputOutput: "", inputOutputTwo: "", rrt: false, rrtText: "", balance: "", cumBalance: "", motionPassed: false, perabdomen: "",
    f: false, fText:"", a: false, aText:"", s: false, sText:"", t: false, tText:"", h: false, hText:"", u: false, uText:"", g: false, gText:"", textArea1: "",
    painScore: "", cultureAndAensitivity: "", antibioticsPerday: "", invasiveLines: "", planForTheDay: "",
    responseNotes: []
};

const reducer = (state, action) => {
    switch (action.type) {
        default:
            return { ...state, [action.type]: action.payload };
    }
};

function CriticalcareDialog(props) {
    const [show, setShow] = useState(props.openPopup);
    const [state, dispatch] = useReducer(reducer, initialState);
    const history = useHistory();
    const [drugChat, setDrugChat] = useState([]);
    const [patientMinimalInfo, setPatientMinimalInfo] = useState(props.patientMinimalInfo);
    const [rrtEnable, setrrtEnable] = useState(false);

    useEffect(() => {
        setValuesToCare(props.viewHistory);
        setDrugChatData(props.viewHistory);
    }, [])

    const handleClose = () => {
        props.handleClose(false);
    }

    const handleChange = (e) => {
        const { id, value } = e.target;
        dispatch({ type: id, payload: value });
    }

    const setDrugChatData = (careDataInfo) => {
        if (careDataInfo !== undefined && careDataInfo !== null) {
            if (careDataInfo.drug_chart !== undefined && careDataInfo.drug_chart !== null && careDataInfo.drug_chart.length > 0) {
                
                setDrugChat(careDataInfo.drug_chart);
            }
        }
    }

    const setValuesToCare = (careDataInfo) => {
        if (careDataInfo !== undefined && careDataInfo !== null) {
            if (careDataInfo.critical_care !== undefined && careDataInfo.critical_care !== null && careDataInfo.critical_care.length > 0) {
                let careData = careDataInfo.critical_care[0];
                dispatch({ type: 'createdDate', payload: careData.createdDate });
                dispatch({ type: 'diagnosis', payload: careData.diagnosis });
                dispatch({ type: 'todaysIssues', payload: careData.todaysIssues });
                dispatch({ type: 'pendingIssues', payload: careData.pendingIssues });
                dispatch({ type: 'clinicalTrends', payload: careData.clinicalTrends });
                dispatch({ type: 'gcs', payload: careData.gcs });
                dispatch({ type: 'pupils', payload: careData.pupils });
                dispatch({ type: 'oxygenTheraphy', payload: careData.oxygenTheraphy });
                dispatch({ type: 'invasive', payload: careData.invasive });
                dispatch({ type: 'niv', payload: careData.niv });
                dispatch({ type: 'textAreaMd', payload: careData.textAreaMd });
                dispatch({ type: 'temp', payload: careData.temp });
                dispatch({ type: 'hr', payload: careData.hr });
                dispatch({ type: 'bp', payload: careData.bp });
                dispatch({ type: 'vasopressors', payload: careData.vasopressors });
                dispatch({ type: 'ph', payload: careData.ph });
                dispatch({ type: 'pco2', payload: careData.pco2 });
                dispatch({ type: 'po2', payload: careData.po2 });
                dispatch({ type: 'be', payload: careData.be });
                dispatch({ type: 'hco3', payload: careData.hco3 });
                dispatch({ type: 'lac', payload: careData.lac });
                dispatch({ type: 'inputOutput', payload: careData.inputOutput });
                dispatch({ type: 'inputOutputTwo', payload: careData.inputOutputTwo });
                dispatch({ type: 'rrt', payload: careData.rrt });
                dispatch({ type: 'rrtText', payload: careData.rrt === true ? careData.rrtText : "" });
                dispatch({ type: 'balance', payload: careData.balance });
                dispatch({ type: 'cumBalance', payload: careData.cumBalance });
                dispatch({ type: 'motionPassed', payload: careData.motionPassed });
                dispatch({ type: 'perabdomen', payload: careData.perabdomen });
                dispatch({ type: 'f', payload: careData.f });
                dispatch({ type: 'a', payload: careData.a });
                dispatch({ type: 's', payload: careData.s });
                dispatch({ type: 't', payload: careData.t });
                dispatch({ type: 'h', payload: careData.h });
                dispatch({ type: 'u', payload: careData.u });
                dispatch({ type: 'g', payload: careData.g });
                dispatch({ type: 'fText', payload: careData.fText });
                dispatch({ type: 'aText', payload: careData.aText });
                dispatch({ type: 'sText', payload: careData.sText });
                dispatch({ type: 'tText', payload: careData.tText });
                dispatch({ type: 'hText', payload: careData.hText });
                dispatch({ type: 'uText', payload: careData.uText });
                dispatch({ type: 'gText', payload: careData.gText });
                dispatch({ type: 'textArea1', payload: careData.textArea1 });
                dispatch({ type: 'painScore', payload: careData.painScore });
                dispatch({ type: 'cultureAndAensitivity', payload: careData.cultureAndAensitivity });
                dispatch({ type: 'antibioticsPerday', payload: careData.antibioticsPerday });
                dispatch({ type: 'invasiveLines', payload: careData.invasiveLines });
                dispatch({ type: 'planForTheDay', payload: careData.planForTheDay });
                if (careData.rrt) {
                    setrrtEnable(true);
                } else {
                    setrrtEnable(false);
                }
            }
        }
    }

    return (
        <>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                size="lg"
                keyboard={false}>
                <Modal.Header>
                    <Modal.Title>Critical Care Notes</Modal.Title>
                    <Button variant="secondary" onClick={handleClose}>Close</Button>
                </Modal.Header>
                <Modal.Body>
                    <div className="cardMainlayout pt-0 min-height">
                        <Table hover>
                            <thead>
                                <tr>
                                    <th>Patient Name</th>
                                    <th>Age</th>
                                    <th>Sex</th>
                                    <th>MR Number</th>
                                </tr>
                            </thead>
                            <tbody>
                                {patientMinimalInfo !== undefined && patientMinimalInfo !== null &&
                                    <React.Fragment>
                                        <tr>
                                            <td>
                                                <span>{patientMinimalInfo.name}</span>
                                            </td>
                                            <td>
                                                <span>{patientMinimalInfo.age}</span>
                                            </td>
                                            <td>
                                                <span>{patientMinimalInfo.sex}</span>
                                            </td>
                                            <td>
                                                <span>{patientMinimalInfo.mrNum}</span>
                                            </td>
                                        </tr>
                                    </React.Fragment>
                                }
                            </tbody>
                        </Table>
                    </div>


                    <div className="cardMainlayout pt-0 min-height">
                        <h4 className="mb-4">Drug Chart</h4>
                        <div className="container drugchart mt-3">
                        {drugChat !== null && drugChat.length > 0 && drugChat.map((drug, idx) => (
                            <Form.Row className="divider">
                                <React.Fragment key={idx}>
                                    <Form.Group as={Col} xs={4} controlId="drugName">
                                        <Form.Control type="text" placeholder="Enter Name" disabled  value={drug.drugName} />
                                        <Form.Label>Drug Name</Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} xs={4} controlId="prescBy">
                                        <Form.Control type="text" placeholder="Enter Email" disabled value={drug.prescBy}  />
                                        <Form.Label>Prescribed By</Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} xs={3} controlId="prescBy">
                                        <Form.Control type="text" placeholder="Enter Email" disabled value={DateUtils.convertToDateFormate(drug.dateTime)} />
                                        <Form.Label>Start Date & Time</Form.Label>
                                    </Form.Group>

                                    <Form.Group as={Col} xs={2} controlId="dose">
                                        <Form.Control type="text" placeholder="Dose" disabled value={drug.dose} />
                                        <Form.Label>Dose</Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} xs={2} controlId="route">
                                        <Form.Control type="text" placeholder="Route" disabled value={drug.route} />
                                        <Form.Label>Route</Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} xs={2} controlId="freq">
                                        <Form.Control type="text" placeholder="Frequency" disabled value={drug.freq} />
                                        <Form.Label>Frequency</Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} xs={3} controlId="prescBy">
                                        <Form.Control type="text" placeholder="Enter Email" disabled value={DateUtils.convertToDateFormate(drug.stopDate)} />
                                        <Form.Label>Stop Date & Time</Form.Label>
                                    </Form.Group>
                                </React.Fragment>
                            </Form.Row>
                        ))}
                    </div>
                    </div>

                    <div className="cardMainlayout pt-0">
                        <h4>Notes</h4>
                        <div className="contentArea">
                            <Form.Row>
                                <Form.Group as={Col} xs={12}>
                                    <div><span><b>Date : {DateUtils.convertToDateFormate(state.createdDate)}</b></span></div>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={12} controlId="diagnosis">
                                    <Form.Control as="textarea" rows="2" disabled placeholder="Diagnosis" value={state.diagnosis} onChange={handleChange} />
                                    <Form.Label>Diagnosis </Form.Label>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row>
                                <Form.Group as={Col} xs={12} controlId="todaysIssues">
                                    <Form.Control as="textarea" rows="2" disabled placeholder="Today's Issues" value={state.todaysIssues} onChange={handleChange} />
                                    <Form.Label>Today's Issues</Form.Label>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row>
                                <Form.Group as={Col} xs={12} controlId="pendingIssues">
                                    <Form.Control as="textarea" rows="2" disabled placeholder="Pending Issues" value={state.pendingIssues} onChange={handleChange} />
                                    <Form.Label>Pending Issues</Form.Label>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row>
                                <Form.Group as={Col} xs={12} controlId="clinicalTrends">
                                    <Form.Control as="textarea" rows="2" disabled placeholder="Pending Issues" value={state.clinicalTrends} onChange={handleChange} />
                                    <Form.Label>Clinical Trends</Form.Label>
                                </Form.Group>
                            </Form.Row>


                            <div><span><b>CNS</b></span></div>

                            <Form.Row className="ccGrid">
                                <Form.Group as={Col} xs={6} controlId="gcs">
                                    <Form.Control type="text" disabled placeholder="GCS" value={state.gcs} onChange={handleChange} />
                                    <Form.Label>GCS</Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={6} controlId="pupils">
                                    <Form.Control type="text" disabled placeholder="Pupils" value={state.pupils} onChange={handleChange} />
                                    <Form.Label>Pupils</Form.Label>
                                </Form.Group>
                            </Form.Row>

                            <div><span><b>RS</b></span></div>
                            <Form.Row className="ccGrid">
                                <Form.Group as={Col} xs={6} controlId="oxygenTheraphy">
                                    <Form.Control type="text" disabled placeholder="Oxygen Theraphy" value={state.oxygenTheraphy} onChange={handleChange} />
                                    <Form.Label>Oxygen Theraphy</Form.Label>
                                </Form.Group>
                            </Form.Row>


                            <div><span><b>Mode</b></span></div>
                            <Form.Row className="ccGrid">
                                <Form.Group as={Col} xs={3} controlId="st">
                                    <Form.Check
                                        type="checkbox"
                                        disabled
                                        id="invasive"
                                        label="Invasive"
                                        checked={state.invasive}
                                        value={state.invasive} onChange={handleChange}
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="st">
                                    <Form.Check
                                        type="checkbox"
                                        disabled
                                        id="niv"
                                        label="NIV"
                                        checked={state.niv}
                                        value={state.niv} onChange={handleChange}
                                    />
                                </Form.Group>

                                <Form.Group as={Col} xs={6} controlId="textAreaMd">
                                    <Form.Control as="textarea" rows="2" disabled placeholder="Text Area" value={state.textAreaMd} onChange={handleChange} />
                                    <Form.Label>Text Area</Form.Label>
                                </Form.Group>
                            </Form.Row>


                            <div><span><b>CVS</b></span></div>
                            <Form.Row className="ccGrid">
                                <Form.Group as={Col} xs={3} controlId="temp">
                                    <Form.Control type="text" disabled placeholder="Temp F" value={state.temp} onChange={handleChange} />
                                    <Form.Label>Temp F</Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={3} controlId="hr">
                                    <Form.Control type="text" disabled placeholder="HR/min" value={state.hr} onChange={handleChange} />
                                    <Form.Label>HR/min</Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={3} controlId="bp">
                                    <Form.Control type="text" disabled placeholder="BP mmHg" value={state.bp} onChange={handleChange} />
                                    <Form.Label>BP mmHg</Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={3} controlId="vasopressors">
                                    <Form.Control type="text" disabled placeholder="Vasopressors" value={state.vasopressors} onChange={handleChange} />
                                    <Form.Label>Vasopressors</Form.Label>
                                </Form.Group>
                            </Form.Row>

                            <div><span><b>ABG</b></span></div>
                            <Form.Row className="ccGrid">
                                <Form.Group as={Col} xs={3} controlId="ph">
                                    <Form.Control type="text" disabled placeholder="PH" value={state.ph} onChange={handleChange} />
                                    <Form.Label>PH</Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={3} controlId="pco2">
                                    <Form.Control type="text" disabled placeholder="Pco2 mmHg" value={state.pco2} onChange={handleChange} />
                                    <Form.Label>Pco2 mmHg</Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="po2">
                                    <Form.Control type="text" disabled placeholder="Po2 mmHg" value={state.po2} onChange={handleChange} />
                                    <Form.Label>Po2 mmHg</Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="be">
                                    <Form.Control type="text" disabled placeholder="BE" value={state.be} onChange={handleChange} />
                                    <Form.Label>BE </Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="hco3" className="mt-3">
                                    <Form.Control type="text" disabled placeholder="Hco3" value={state.hco3} onChange={handleChange} />
                                    <Form.Label>Hco3</Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={3} controlId="lac" className="mt-3">
                                    <Form.Control type="text" disabled placeholder="Lac" value={state.lac} onChange={handleChange} />
                                    <Form.Label>Lac </Form.Label>
                                </Form.Group>
                            </Form.Row>

                            <div><span><b>Renal</b></span></div>
                            <Form.Row className="ccGrid">
                                <Form.Group as={Col} xs={3} controlId="inputOutput">
                                    <Form.Control type="text" disabled placeholder="Input" disabled value={state.inputOutput} onChange={handleChange} />
                                    <Form.Label>Input</Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="inputOutputTwo">
                                    <Form.Control type="text" disabled placeholder="Output" value={state.inputOutputTwo} onChange={handleChange} />
                                    <Form.Label>Output</Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="balance">
                                    <Form.Control type="text" disabled placeholder="Balance" value={state.balance} onChange={handleChange} />
                                    <Form.Label>Balance</Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="cumBalance">
                                    <Form.Control type="text" disabled placeholder="Cum Balance" value={state.cumBalance} onChange={handleChange} />
                                    <Form.Label>Cum Balance</Form.Label>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row className="ccGrid">
                                <Form.Group as={Col} xs={3} controlId="st">
                                    <Form.Check
                                        type="checkbox"
                                        id="rrt"
                                        label="RRT"
                                        disabled
                                        checked={state.rrt}
                                        value={state.rrt} onChange={handleChange}
                                    />
                                </Form.Group>
                                {rrtEnable &&
                                    <Form.Group as={Col} xs={9} controlId="rrtText">
                                        <Form.Control as="textarea" rows="2" disabled placeholder="RRT Text" value={state.rrtText} onChange={handleChange} />
                                        <Form.Label>RRT Text </Form.Label>
                                    </Form.Group>
                                }
                            </Form.Row>
                            <Form.Row className="ccGrid">
                                <Form.Group as={Col} xs={3} controlId="st">
                                    <Form.Check
                                        type="checkbox"
                                        id="motionPassed"
                                        label="Motion Passed"
                                        checked={state.motionPassed}
                                        disabled
                                        value={state.motionPassed} onChange={handleChange}
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={9} controlId="perabdomen">
                                    <Form.Control type="text" disabled placeholder="Perabdomen" value={state.perabdomen} onChange={handleChange} />
                                    <Form.Label>Perabdomen</Form.Label>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row className="ccGrid">
                                <Form.Group as={Col} xs={1} controlId="st">
                                    <Form.Check
                                        type="checkbox"
                                        disabled
                                        id="f"
                                        label="F"
                                        checked={state.f}
                                        value={state.f} onChange={handleChange}
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="fText">
                                    <Form.Control type="text" placeholder="Text (F)" disabled value={state.fText} onChange={handleChange} />
                                    <Form.Label>Text (F)</Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={1} controlId="st">
                                    <Form.Check
                                        type="checkbox"
                                        disabled
                                        id="a"
                                        label="A"
                                        checked={state.a}
                                        value={state.a} onChange={handleChange}
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="aText">
                                    <Form.Control type="text" placeholder="Text (A)" disabled value={state.aText} onChange={handleChange} />
                                    <Form.Label>Text (A)</Form.Label>
                                 </Form.Group>

                                <Form.Group as={Col} xs={1} controlId="st">
                                    <Form.Check
                                        type="checkbox"
                                        disabled
                                        id="s"
                                        label="S"
                                        checked={state.s}
                                        value={state.s} onChange={handleChange}
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="sText">
                                    <Form.Control type="text" placeholder="Text (S)" disabled value={state.sText} onChange={handleChange} />
                                    <Form.Label>Text (S)</Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={1} controlId="st" className="mt-4">
                                    <Form.Check
                                        type="checkbox"
                                        id="t"
                                        disabled
                                        label="T"
                                        checked={state.t}
                                        value={state.t} onChange={handleChange}
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="tText" className="mt-4">
                                    <Form.Control type="text" placeholder="Text (T)" disabled value={state.tText} onChange={handleChange} />
                                    <Form.Label>Text (T)</Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={1} controlId="st" className="mt-4">
                                    <Form.Check
                                        type="checkbox"
                                        disabled
                                        id="h"
                                        label="H"
                                        checked={state.h}
                                        value={state.h} onChange={handleChange}
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="hText" className="mt-4">
                                    <Form.Control type="text" placeholder="Text (H)" disabled value={state.hText} onChange={handleChange} />
                                    <Form.Label>Text (H)</Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={1} controlId="st" className="mt-4">
                                    <Form.Check
                                        type="checkbox"
                                        id="u"
                                        disabled
                                        label="U"
                                        checked={state.u}
                                        value={state.u} onChange={handleChange}
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="uText" className="mt-4">
                                    <Form.Control type="text" placeholder="Text (U)" disabled value={state.uText} onChange={handleChange} />
                                    <Form.Label>Text (U)</Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={1} controlId="st" className="mt-4">
                                    <Form.Check
                                        type="checkbox"
                                        id="g"
                                        label="G"
                                        disabled
                                        checked={state.g}
                                        value={state.g} onChange={handleChange}
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="gText" className="mt-4">
                                    <Form.Control type="text" placeholder="Text (G)" value={state.gText} onChange={handleChange} />
                                    <Form.Label>Text (G)</Form.Label>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row>
                                <Form.Group as={Col} xs={6} controlId="textArea1">
                                    <Form.Control type="text" disabled placeholder="Text Area" value={state.textArea1} onChange={handleChange} />
                                    <Form.Label>Text Area</Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={6} controlId="painScore">
                                    <Form.Control type="text" disabled placeholder="Pain Score" value={state.painScore} onChange={handleChange} />
                                    <Form.Label>Pain Score</Form.Label>
                                </Form.Group>
                            </Form.Row>

                            <div><span><b>Infection</b></span></div>
                            <Form.Row className="ccGrid">
                                <Form.Group as={Col} xs={4} controlId="cultureAndAensitivity">
                                    <Form.Control type="text" disabled placeholder="Culture And Aensitivity" value={state.cultureAndAensitivity} onChange={handleChange} />
                                    <Form.Label>Culture And Sensitivity</Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={4} controlId="antibioticsPerday">
                                    <Form.Control type="text" disabled placeholder="Anti biotics per day" value={state.antibioticsPerday} onChange={handleChange} />
                                    <Form.Label>Antibiotics/day</Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={4} controlId="invasiveLines">
                                    <Form.Control type="text" disabled placeholder="Invasive Lines" value={state.invasiveLines} onChange={handleChange} />
                                    <Form.Label>Invasive Lines</Form.Label>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={12} controlId="planForTheDay">
                                    <Form.Control as="textarea" rows="6" disabled placeholder="Plan for the day" value={state.planForTheDay} onChange={handleChange} />
                                    <Form.Label>Plan for the day</Form.Label>
                                </Form.Group>

                            </Form.Row>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>Close</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
export default CriticalcareDialog
