import React from 'react';
import PatientInfoService from '../../../../services/administration/PatientInfoService';

/**
 * @description: CRUD operations for Patient
 * @author: Lakshmi
 * @param {*}
 * @function Lab
 * @date : 13-08-2020
 */



// ========================================================================
//  Branch Create Operation
//  This will create Branch and save the details 
// ========================================================================
export const saveLab = async (obj) => {
    let finalRes=[];
    await PatientInfoService.saveLabInfo(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

export const retrieveLab = async (obj) => {
    let finalRes=[];
    await PatientInfoService.retriveLabInfo(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}


export const fetchLabTrendingData = async (obj) => {
    let finalRes=[];
    await PatientInfoService.fetchLabTrendingData(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

