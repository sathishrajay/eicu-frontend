import React, { useEffect, useState } from 'react';
import { Modal } from "react-bootstrap";
import { Line } from 'react-chartjs-2';
import "react-datepicker/dist/react-datepicker.css";
import DateUtils from '../../../commonUtilities/DateUtils';
import Button from '../../../common/buttons/Button';
import { fetchLabTrendingData } from './operations';
import LocationService from '../../../../services/administration/LoationService'

function LabTrendingDialog(props) {
    const [show, setShow] = useState(props.openPopup);
    const [trendLabDataReq, setTrendLabDataReq] = useState(props.trendLabDataReq);
    const [graphDataDates, setGraphDataDates] = useState([]);
    const [graphDataValues, setGraphDataValues] = useState([]);
    const [graphData, setGraphData] = useState(null);

    useEffect(() => {}, [graphDataDates, setGraphDataValues])
    useEffect(() => {}, [trendLabDataReq])
    useEffect(() => {}, [graphData])

    useEffect(() => {
        setTrendLabDataReq(props.trendLabDataReq)
        constructLabTrendingData();
    }, [props.trendLabDataReq])

    const constructLabTrendingData = async () =>{
        let payload =props.trendLabDataReq;
        console.log("Lab trending data request :"+JSON.stringify(payload));
        await fetchLabTrendingData(payload).then((response) => {
            console.log("Lab trending data response :"+JSON.stringify(response));
             if (response !==null && response.suc && response.res !== null && response.res.length > 0){
                response.res.map((itm, index) => {
                    graphDataDates.push(DateUtils.convertToDate(itm.date));
                    graphDataValues.push(itm.value);
                });
                graphDataFn();
             }
         });
    }

    const graphDataFn = () => {
        let graphData = {
            labels: graphDataDates,
            datasets: [
              {
                label: trendLabDataReq !== undefined && trendLabDataReq != null ? getLabel(trendLabDataReq.type) : "",
                fill: false,
                lineTension: 0.5,
                backgroundColor: 'rgba(75,192,192,1)',
                borderColor: 'rgba(0,0,0,1)',
                borderWidth: 2,
                data: graphDataValues
              }
            ]
          }
          setGraphData(graphData);
    }
    
    const handleClose = () => {
        props.handleClose(false);
    }

    const getLabel = (lbl) => {
        let data = LocationService.trendingDataMaster().filter(itm => itm.value === lbl);
        if(data !== null && data.length > 0){
            return data[0].label;
        }else{
            return lbl;
        }
    }
    
    return (
        <>
        <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                size="lg"
                keyboard={false}>
                <Modal.Header   >
                    <Modal.Title>Trending Data</Modal.Title>
                    <Button variant="secondary" onClick={handleClose}>Close</Button>
                </Modal.Header>
                <Modal.Body>
                    <div className="cardMainlayout pt-0 min-height">
                    <Line
                        data={graphData}
                        options={{
                            title:{
                            display:true,
                            text: trendLabDataReq !== undefined && trendLabDataReq != null ? getLabel(trendLabDataReq.type) : "",
                            fontSize:20
                            },
                            legend:{
                            display:true,
                            position:'right'
                            }
                        }}
                        />
                    </div>
                </Modal.Body>
            </Modal>
        </>    )
}

export default LabTrendingDialog
