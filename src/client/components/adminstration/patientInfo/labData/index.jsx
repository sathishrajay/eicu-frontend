import React, { useContext, useEffect, useReducer, useState } from 'react';
import { Alert, Col, Form } from "react-bootstrap";
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { useHistory } from 'react-router-dom';
import { Context } from "../../../../../App";
import Button from '../../../common/buttons/Button';
import FileUpload from '../../../common/FileUpload';
import Icon from '../../../common/icomoonIcon/Icon';
import DateUtils from '../../../commonUtilities/DateUtils';
import LabTrendingDialog from './LabTrendingDialog';
import { retrieveLab, saveLab } from './operations';
import LoationService from '../../../../services/administration/LoationService';



/**
 * @description:This function will creaet Hospitales and this will provide the ADD,EDIT and Remove functionalities
 * @author: Lakshmi
 * @param {*}
 * @function Hospital
 * @date : 13-08-2020
 */

/**
 * Initial State Declaration
 */
const initialState = {
    id:0
};
//=====================================
// Reducer function for userReact Hook
// which will update the state
//=====================================
const reducer = (state, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...state
            };
        case 'editHospital':
            return Object.assign(state, action.payload);
        default:
            return { ...state, [action.type]: action.payload };
    }
};

function LabData(props) {
    const [state, dispatch] = useReducer(reducer, initialState);
    const [context, setContext] = useContext(Context);
    const history = useHistory();
    const [notiMessage, setNotiMessage] = useState('');
    const [notiMessageShow, setNotiMessageShow] = useState(false);
    const [notiVarient, setNotiVarient] = useState();
    const [foodEnable, setFoodEnable] = useState(false);
    const [drugEnable, setDrugEnable] = useState(false);
    const [fromDate, setFromDate] = useState(new Date());
    const [lab,setLabs] =useState({labdata:[]})
    const [data,setData] =useState()
    const [patientId, setPatientId] =useState();
    const [userId , setUserId] = useState()
    const [enableBtn, setEnableBtn] =useState(false);
    const [disableBtn,setDisableBtn] = useState(false)
    const [showTrndingPopup, setShowTrndingPopup] = useState(false);
    const [trendLabDataReq, setTrendLabDataReq] =useState(null);
    
    useEffect(() => {
        window.scrollTo(0, 0);
        if(context.logindata._id!==undefined){
          setUserId(context.logindata._id);
        }
      }, [context.logindata._id]);

      useEffect(() => {
        window.scrollTo(0, 0);
        if(props.showMsg){
          setNotiMessageShow(true);
          setNotiMessage('Patient Details Updated Successfully..');
          setNotiVarient('success');
          hidemessage();
        }
      }, [props.showMsg]);

      const hidemessage=() =>{
        setTimeout(function () {
            setNotiMessageShow(false);
            setNotiMessage('');
            setNotiVarient('');
        }, 5000);
      }

    useEffect(() => {
    }, [trendLabDataReq])


    useEffect(() => {
        if(props.patientData!==undefined){
            setData(props.patientData);
            setPatientId(props.patientData.patientId);
            if(props.patientData.patientId!==undefined && props.patientData.patientId!==null){
                const payload={"patientId":props.patientData.patientId}
                retrieveLab(payload).then((response) => {
                    if (response.suc && response.res.length!==0){
                        setEnableBtn(true);
                        setDisableBtn(true);
                       setValuesToCare(response.res);
                    }
                });
            }
        }
      }, [props.patientData!==undefined]);
      const setValuesToCare =(labData) => {
        let dataArr=[];
        labData.map((itm, index) => {

            let data = {
                _id:itm._id,
                date: DateUtils.convertToDate(itm.date),
                bloodGroup: itm.bloodGroup,
                haematology: itm.bloodGroup,
                totalLeucocytes: itm.totalLeucocytes,
                haemoglobin: itm.haemoglobin,
                platelets: itm.platelets,
                neutrophils: itm.neutrophils,
                lymphocytes: itm.lymphocytes,
                biochemistry: itm.biochemistry,
                bloodUrea: itm.bloodUrea,
                serCreatinine: itm.serCreatinine,
                totalBilirubin: itm.totalBilirubin,
                directBilirubin: itm.directBilirubin,
                indirectBilirubin: itm.indirectBilirubin,
                totalProtiens: itm.totalProtiens,
                serAlbumin: itm.serAlbumin,
                serGlobulin: itm.serGlobulin,
                sgptAlt: itm.sgptAlt,
                sgptAst: itm.sgptAst,
                alkPhosphataseAlp: itm.alkPhosphataseAlp,
                troponinT: itm.troponinT,
                troponinI: itm.troponinI,
                serumElectrolytes: itm.serumElectrolytes,
                sodium: itm.sodium,
                potassium: itm.potassium,
                bicarbonate: itm.bicarbonate,
                chlorides: itm.chlorides,
                coagulation: itm.coagulation,
                bt: itm.bt,
                ct: itm.ct,
                pt: itm.pt,
                inr: itm.inr,
                aptt: itm.aptt,
                summary: itm.summary,
                reportURL:itm.reportURL
            }
            dataArr.push(data);
          })
          setLabs({labdata:dataArr});
      }

    useEffect(() => {
        setLabs({ labdata:
             [
              {
                "_id":"",
                "date": new Date(),
                "bloodGroup": "",
                "haematology": "",
                "totalLeucocytes": "",
                "haemoglobin": "",
                "platelets": "",
                "neutrophils": "",
                "lymphocytes": "",
                "biochemistry": "",
                "bloodUrea": "",
                "serCreatinine": "",
                "totalBilirubin": "",
                "directBilirubin": "",
                "indirectBilirubin": "",
                "totalProtiens": "",
                "serAlbumin": "",
                "serGlobulin": "",
                "sgptAlt": "",
                "sgptAst": "",
                "alkPhosphataseAlp": "",
                "troponinT": "",
                "troponinI": "",
                "serumElectrolytes": "",
                "sodium": "",
                "potassium": "",
                "bicarbonate": "",
                "chlorides": "",
                "coagulation": "",
                "bt": "",
                "ct": "",
                "pt": "",
                "inr": "",
                "aptt": "",
                "summary": "",
                "createdBy": userId,
                "patientId":(props.patientData!==undefined && props.patientData.patientId!==undefined?props.patientData.patientId:""),
                "reportURL":[]
              }
            ]
          });

            let trendingDropDwConst = LoationService.trendingDataMaster();
            let trendList = trendingDropDwConst.map((trnd) =>
                    <option value={trnd.value}>{trnd.label}</option>
                );
            dispatch({ type: "trendLabDrpData", payload: trendList })
            dispatch({ type: "trendFromDt", payload: null })  
            dispatch({ type: "trendToDt", payload: null })  
            window.scrollTo(0, 0);
      }, []);

    const handleChangeTrend = (e) => {
        dispatch({type: "selLabTrendVal", payload : e.target.value})
    }

    const trendLabDataFn = () => {
        if(state.trendFromDt === null || state.trendFromDt === "" || state.trendFromDt ===''){
            setNotiMessageShow(true);
            setNotiMessage('From date must be provided');
            setNotiVarient('danger');
            hidemessage();
        }else if(state.trendToDt === null || state.trendToDt === "" || state.trendToDt ===''){
            setNotiMessageShow(true);
            setNotiMessage('To date must be provided');
            setNotiVarient('danger');
            hidemessage();
        }else if(state.selLabTrendVal === undefined || state.selLabTrendVal === null || state.selLabTrendVal === "" || state.selLabTrendVal ===''){
            setNotiMessageShow(true);
            setNotiMessage('Type must be provided');
            setNotiVarient('danger');
            hidemessage();
        }else{
            let req ={
                patientId : props.patientData.patientId,
                type      : state.selLabTrendVal,
                fromdate  : state.trendFromDt,
                todate    : state.trendToDt
            } 
            setTrendLabDataReq(req);
            setShowTrndingPopup(true);
        }
    }

    const handleClose = () => {
        setShowTrndingPopup(false);
    }

    const handleChange = idx => evt => {
        try {
          let newData = lab.labdata.map((obj, sidx) => {
            if (idx !== sidx) return obj;
            else {
              return { ...obj, [evt.target.id]: evt.target.value };
            }
          });
          setLabs({ labdata: newData })
        } catch (err) {
         // log.error('Exception occured in CompanyComponent function---' + err);
        }
      }
      const handleDobChange = idx => date => {
        const convertDate = DateUtils.convertToDateHifen(date);
        let newData = lab.labdata.map((obj, sidx) => {
          if (idx !== sidx) return obj;
          return {
            ...obj, date: convertDate
          };
        });
        setLabs({ labdata: newData })
      }

      const handleRemoveLab = idx => () => {
        setLabs({
            labdata: lab.labdata.filter((s, sidx) => idx !== sidx)
        });
      };
     
    //Add Hospital
    const saveLabData = () => {
        
        const convertDate = DateUtils.convertStringToDate(fromDate);
        state.date=convertDate;
        for(let val of lab.labdata){
             val.patientId=data.patientId;
             
        }
        saveLab(lab).then((resp) => {
            if (resp.suc) {
                setDisableBtn(true);
                const payload={"patientId":props.patientData.patientId}
                retrieveLab(payload).then((response) => {
                     if (response.suc && response.res.length!==0){
                         setEnableBtn(true);
                        setValuesToCare(response.res);
                     }
                 });
                props.sendLab("Lab data");
            }else{
                setNotiMessageShow(true);
                setNotiMessage('Lab Creattion Failed');
                setNotiVarient('danger')
                hidemessage();
            }
        });
        window.scrollTo(0, 0);
    }
    
    const handleAddLab = () => {
        setLabs({
            labdata: lab.labdata.concat([{
                "_id":"",
                "date": new Date(),
                "bloodGroup": "",
                "haematology": "",
                "totalLeucocytes": "",
                "haemoglobin": "",
                "platelets": "",
                "neutrophils": "",
                "lymphocytes": "",
                "biochemistry": "",
                "bloodUrea": "",
                "serCreatinine": "",
                "totalBilirubin": "",
                "directBilirubin": "",
                "indirectBilirubin": "",
                "totalProtiens": "",
                "serAlbumin": "",
                "serGlobulin": "",
                "sgptAlt": "",
                "sgptAst": "",
                "alkPhosphataseAlp": "",
                "troponinT": "",
                "troponinI": "",
                "serumElectrolytes": "",
                "sodium": "",
                "potassium": "",
                "bicarbonate": "",
                "chlorides": "",
                "coagulation": "",
                "bt": "",
                "ct": "",
                "pt": "",
                "inr": "",
                "aptt": "",
                "summary": "",
                "reportURL":[]
              }])
        });
        setDisableBtn(false);
        window.scrollTo(0, 0);
      };

      const fileUploadRes=(idx) => (resp) =>{
        try {
            if(lab.labdata[idx].reportURL){
            const finalUrls = lab.labdata[idx].reportURL.filter((item) => item.name !== resp.name);
            lab.labdata[idx].reportURL=finalUrls;
            }
        } catch (err) {
         // log.error('Exception occured in CompanyComponent function---' + err);
        }
    }

    const handleFromDateChange = date => {
        const convertDate = DateUtils.convertStringToDate(date);
        dispatch({ type: 'trendFromDt', payload: convertDate })
    }

    const handleToDateChange = date => {
        const convertDate = DateUtils.convertStringToDate(date);
        dispatch({ type: 'trendToDt', payload: convertDate })
    }
   
    return (
        <>
            
            <Alert className="notification" variant={notiVarient} show={notiMessageShow}
                onClose={() => setNotiMessageShow(false)}
                dismissible> {notiMessage}</Alert>
                
               {data!==undefined &&
                <div className="row">
                    <div className="col-sm-6">Patient Name : <b>{data.name}</b></div>
                    <div className="col-sm-6 text-right"> MR Number : <b>{data.mrNum}</b></div>
               
                 
                </div>
                
                }
                <hr/>
                {true &&
                <div className="col-12 mt-4 mb-5">
                <Form.Row className="ccGrid">
                    <span className="col-12 mb-3"><b className="text-primary">Lab Trending Data</b></span>
                    <Form.Group as={Col} xs={3} className="selectplugin datePicker" controlId="trendFromDt">
                        <DatePicker
                            id="trendFromDt"
                            name="trendFromDt"
                            value={state.trendFromDt}
                            onChange={handleFromDateChange}/>
                        <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                        <Form.Label>From Date</Form.Label>
                    </Form.Group>
                    <Form.Group as={Col} xs={3} className="selectplugin datePicker" controlId="trendToDt">
                    <DatePicker
                            id="trendToDt"
                            name="trendToDt"
                            value={state.trendToDt}
                            onChange={handleToDateChange}/>
                        <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                        <Form.Label>To Date</Form.Label>
                    </Form.Group>
                    <Form.Group as={Col} xs={3} controlId="cty">
                        <Form.Control as="select" value={state.selLabTrendVal} onChange={handleChangeTrend}>
                            <option value="">Select</option>
                            {state.trendLabDrpData}
                        </Form.Control>
                        <Form.Label>Type <sup>*</sup></Form.Label>
                    </Form.Group>
                    <Button
                        size="xs"
                        variant="link"
                        className="add btn-outline-primary btn-small"
                        onClick={trendLabDataFn}
                        type="submit">Submit
                    </Button>
                    {showTrndingPopup &&
                        <LabTrendingDialog openPopup={showTrndingPopup} trendLabDataReq = {trendLabDataReq} handleClose={handleClose} 
                        />
                    }
                </Form.Row>
                </div>
                }
                <div className="labdata-tbl">
                        
                            {lab.labdata.map((lab, idx) => (
                            <React.Fragment key={idx}>           
                            <div className="labdata">
                            <Form.Group as={Col} xs={12} className="selectplugin datePicker" controlId="dob">
                            <DatePicker
                            selected={fromDate}
                            onChange={handleDobChange(idx)}
                            value={lab.date}
                            fromDate={fromDate}
                            minDate={new Date()}
                            disabled={true}
							/>
                            <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                            <Form.Label>Date <sup>*</sup></Form.Label>
                            </Form.Group>
                            {(idx > 0 && lab._id==="" && !disableBtn) ? (
                                    <Col className="delete">
                                    <Button
                                        size="xs"
                                        variant="link"
                                        className="dlt iconBtn mt-1 ml-3 mr-3"
                                        onClick={handleRemoveLab(idx)}
                                        type="submit"><Icon className="edit ml-1 mr-1" color="#475F7B" size={20} icon="delete" />
                                    </Button></Col>
                                    ) : ''}
                                       <Form.Row>
                                        <Form.Group as={Col} xs={6} controlId="bloodGroup">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.bloodGroup} onChange={handleChange(idx)} />
                                            <Form.Label>Blood Group</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                       <Form.Row>
                                       <span className="d-block mb-3"><b>Haematology : </b></span>
                                      
                                        <Form.Group as={Col} xs={6} controlId="totalLeucocytes">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.totalLeucocytes} onChange={handleChange(idx)}/>
                                            <Form.Label>Total Leucocytes</Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Col} xs={6} controlId="haemoglobin">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.haemoglobin} onChange={handleChange(idx)}/>
                                            <Form.Label>Haemoglobin</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                        <Form.Row>
                                        <Form.Group as={Col} xs={6} controlId="platelets">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.platelets} onChange={handleChange(idx)}/>
                                            <Form.Label>Platelets</Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Col} xs={6} controlId="neutrophils">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.neutrophils} onChange={handleChange(idx)}/>
                                            <Form.Label>Neutrophils</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                        <Form.Row>
                                        <Form.Group as={Col} xs={6} controlId="lymphocytes">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.lymphocytes} onChange={handleChange(idx)}/>
                                            <Form.Label>Lymphocytes</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                        <span className="d-block mb-3"><b>Biochemistry :</b></span>
                                         
                                        <Form.Row>
                                        <Form.Group as={Col} xs={6} controlId="bloodUrea">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.bloodUrea} onChange={handleChange(idx)}/>
                                            <Form.Label>Blood Urea</Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Col} xs={6} controlId="serCreatinine">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.serCreatinine} onChange={handleChange(idx)}/>
                                            <Form.Label>Ser. Creatinine</Form.Label>
                                        </Form.Group>
                                        </Form.Row>

                                        <Form.Row>
                                        <Form.Group as={Col} xs={6} controlId="totalBilirubin">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.totalBilirubin} onChange={handleChange(idx)}/>
                                            <Form.Label>Total Bilirubin</Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Col} xs={6} controlId="directBilirubin">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.directBilirubin} onChange={handleChange(idx)}/>
                                            <Form.Label>Direct Bilirubin (Conj)</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                       
                                        <Form.Row>
                                        <Form.Group as={Col} xs={6} controlId="indirectBilirubin">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.indirectBilirubin} onChange={handleChange(idx)} />
                                            <Form.Label>Indirect Bilirubin (Unconj)</Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Col} xs={6} controlId="totalProtiens">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.totalProtiens} onChange={handleChange(idx)}/>
                                            <Form.Label>Total Protiens</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                        
                                        <Form.Row>
                                        <Form.Group as={Col} xs={6} controlId="serAlbumin">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.serAlbumin} onChange={handleChange(idx)}/>
                                            <Form.Label>Ser. Albumin</Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Col} xs={6} controlId="serGlobulin">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.serGlobulin} onChange={handleChange(idx)}/>
                                            <Form.Label>Ser. Globulin</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                       
                                        <Form.Row>
                                        <Form.Group as={Col} xs={6} controlId="sgptAlt">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.sgptAlt} onChange={handleChange(idx)}/>
                                            <Form.Label>SGPT/ALT</Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Col} xs={6} controlId="sgptAst">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.sgptAst} onChange={handleChange(idx)}/>
                                            <Form.Label>SGOT/AST</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                      
                                        <Form.Row>
                                        <Form.Group as={Col} xs={6} controlId="alkPhosphataseAlp">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.alkPhosphataseAlp} onChange={handleChange(idx)}/>
                                            <Form.Label>Alk. Phosphatase/ALP</Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Col} xs={6} controlId="troponinT">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.troponinT} onChange={handleChange(idx)}/>
                                            <Form.Label>Troponin T</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                        
                                        <Form.Row>
                                        <Form.Group as={Col} xs={6} controlId="troponinI">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.troponinI} onChange={handleChange(idx)}/>
                                            <Form.Label>Troponin I</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                       
                                        <Form.Row>
                                        <span className="d-block mb-3"><b>Serum Electrolytes :</b></span>
                                          
                                        <Form.Group as={Col} xs={6} controlId="sodium">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.sodium} onChange={handleChange(idx)}/>
                                            <Form.Label>Sodium</Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Col} xs={6} controlId="potassium">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.potassium} onChange={handleChange(idx)}/>
                                            <Form.Label>Potassium</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                        
                                        <Form.Row>
                                        <Form.Group as={Col} xs={6} controlId="bicarbonate">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.bicarbonate} onChange={handleChange(idx)}/>
                                            <Form.Label>Bicarbonate</Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Col} xs={6} controlId="chlorides">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.chlorides} onChange={handleChange(idx)}/>
                                            <Form.Label>Chlorides</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                       
                                        <Form.Row>
                                        <span className="d-block mb-3"><b>Coagulation :</b></span>
                                          
                                        <Form.Group as={Col} xs={6} controlId="bt">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.bt} onChange={handleChange(idx)}/>
                                            <Form.Label>BT</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                      
                                        <Form.Row>
                                        <Form.Group as={Col} xs={6} controlId="ct">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.ct} onChange={handleChange(idx)} />
                                            <Form.Label>CT</Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Col} xs={6} controlId="pt">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.tt} onChange={handleChange(idx)} />
                                            <Form.Label>PT</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                      
                                        <Form.Row>
                                        <Form.Group as={Col} xs={6} controlId="inr">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.inr} onChange={handleChange(idx)}/>
                                            <Form.Label>INR</Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Col} xs={6} controlId="aptt">
                                            <Form.Control type="text" placeholder="Enter Name" value={lab.aptt} onChange={handleChange(idx)} />
                                            <Form.Label>aPTT</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                        
                                        <Form.Row>
                                        <Form.Group as={Col} xs={12} controlId="summary">
                                            <Form.Control as="textarea" rows="2" placeholder="Summary" value={lab.summary} onChange={handleChange(idx)} />
                                            <Form.Label>Summary</Form.Label>
                                        </Form.Group>
                                        </Form.Row>
                                        <Form.Row>
                                        <FileUpload sendResponse={fileUploadRes(idx)} reportUrls={lab.reportURL} />
                                        </Form.Row>
                                    </div>
                                    </React.Fragment>
                                    ))}
                                    <div className="addbtn"><Button
                                    size="xs"
                                    variant="link"
                                    className="add btn-outline-primary btn-small"
                                    onClick={handleAddLab}
                                    type="submit"
                                    >Add</Button></div>
                    </div>
                    {context.logindata.permissions!==undefined && (context.logindata.permissions.includes("AL") || context.logindata.permissions.includes("EL")) &&
                    <div className="text-right buttonGrop pb-0">
                            <Button
                                size="xs"
                                variant="outline-primary"
                                onClick={saveLabData}
                                type="submit"
                                loading={state.loadOfice}
                            > Update</Button>
                           </div>
                       }
        </>
    )
}
export default LabData