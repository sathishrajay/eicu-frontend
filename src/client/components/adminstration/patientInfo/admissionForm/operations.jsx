import React from 'react';
import PatientInfoService from '../../../../services/administration/PatientInfoService';

/**
 * @description: CRUD operations for Patient
 * @author: Lakshmi
 * @param {*}
 * @function Branch
 * @date : 13-08-2020
 */



// ========================================================================
//  Branch Create Operation
//  This will create Branch and save the details 
// ========================================================================
export const savePatient = async (obj) => {
    let finalRes=[];
    await PatientInfoService.savePatientInfo(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

export const retrievePatient = async (obj) => {
    let finalRes=[];
    await PatientInfoService.retrivePatientInfo(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

export const dischargePatient = async (obj) => {
    let finalRes=[];
    await PatientInfoService.dischargePatient(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

export const sendEprescription = async (obj) => {
    let finalRes=[];
    await PatientInfoService.senPrescription(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

export const fetchOpdHistoryBasedId = async (obj) => {
    let finalRes=[];
    await PatientInfoService.fetchOpdHistoryBasedId(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}


