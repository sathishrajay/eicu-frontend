import React, { useContext, useEffect, useReducer, useState } from 'react';
import { Alert, Col, Form, Modal, Table } from "react-bootstrap";
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { useHistory } from 'react-router-dom';
import { Context, LocationContext } from "../../../../../App";
import Button from '../../../common/buttons/Button';
import Icon from '../../../common/icomoonIcon/Icon';
import DateUtils from '../../../commonUtilities/DateUtils';
import { retrievePatient, savePatient, dischargePatient, sendEprescription, fetchOpdHistoryBasedId} from './operations';
import {retrieveDrug , saveDrugData} from '../dischargeSummary/operations';
import moment from 'moment';
import InnerLoader from '../../../common/InnerLoader';
import OpdHistory from './opdHistory';
var cities = require('../../../masters/Cities.json');


/**
 * @description:This function will creaet Hospitales and this will provide the ADD,EDIT and Remove functionalities
 * @author: Lakshmi
 * @param {*}
 * @function Hospital
 * @date : 13-08-2020
 */

/**
 * Initial State Declaration
 */
const initialState = {
   "_id":"",  "usrid": "","bedNum": 0,"hosp_id": "","name": "","age": "","sex": "","address": "",
  "phoneNum": "","mrNum": "","dtOfAdmission": null,"sourOfHistory": "","prestComplaints": "","histyOfPrestIllness": "","medical": "",
  "dm": false,"ihd": false,"cva": false,"crf": false,"htn": false,"asthma": false,"epilepsy": false,"significantHistory": "",
  "smoking": false,"alcohol": false,"foodReactions": false,"foodDesc":"","drugDesc":"", "drugReactions": false,"pulse": "","bp": "","resp": "",
  "temp": "","spo": "","wt": "","respiratory": "","abdominal": "","neurology": "","musculoskeletal": "",
  "otherSystems": "","provisionalDiagnosis": "","planOfCare": "",discSummary:"",remarks:"",diagnosis:"", historyOpd:[]
 
};
//=====================================
// Reducer function for userReact Hook
// which will update the state
//=====================================
const reducer = (state, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...state
            };
        case 'editHospital':
            return Object.assign(state, action.payload);
        default:
            return { ...state, [action.type]: action.payload };
    }
};

function PatientForm(props) {
    const [context, setContext] = useContext(Context);
    const [state, dispatch] = useReducer(reducer, initialState);
    const history = useHistory();
    const [notiMessage, setNotiMessage] = useState('');
    const [notiMessageShow, setNotiMessageShow] = useState(false);
    const [notiVarient, setNotiVarient] = useState();
    const [foodEnable, setFoodEnable] = useState(false);
    const [drugEnable, setDrugEnable] = useState(false);
    const [fromDate, setFromDate] = useState(new Date());
    const [medication, setMedication] = useState({medications: []});
    const [response, setResponse] = useState();
    const [patientInfo, setpatientInfo] = useState(props.patientInfo);
    const [enableBtn, setEnableBtn] =useState(false);
    const [locationContext, setLocationContext] = useContext(LocationContext);
    const [showPopup, setShowPopup] = useState(false);
    const [errorMsges, setErrorMsges] = useState([]);
    const [isLoadingMore, setIsLoadingMore,] = useState(false);
    const [patientData,setPatientData] = useState();
    const [drugs , setDrugData]= useState({drugdata:[]})
    const [patientId,setPatientId] = useState();
    const [drugdate, setDate] = useState(new Date());
    const [showDischarge, setShowDischarge] = useState(false);
    const [showOpdPopup, setShowOpdPopup] = useState(false);
    const [viewHistory, setViewHistory] = useState();
    const [data, setData] = useState();
    const [showHistory, setShowHistory] = useState(false);
    
    useEffect(() => {
        let nowDate =  moment(new Date()).format('YYYY-MM-DD HH:mm');
        setDrugData({drugdata: [{drugName:"",prescBy:context.logindata.firstName+" "+context.logindata.lastName,dateTime:nowDate, isStop:false,dose:"",route :"",freq:"",stoppedBy:"", stopDate:nowDate,stat:false}]});

        setMedication({ medications: [{ serialNum: 0, name: "", dose: "", route: "", frq: "", isrecncl: false }] })
        let spm = localStorage.getItem("spm");
        //alert("SPM :"+spm);
        if(spm !== undefined && spm !== null && spm === "yes"){
            setNotiMessage('Patient Details Added Successfully..');
            setNotiMessageShow(true);
            setNotiVarient('success');
            hidemessage();
            localStorage.removeItem("spm");
        }
    }, []);



    
      useEffect(() => {
          if(context.logindata._id!==undefined){
            dispatch({ type: 'usrid', payload: context.logindata._id })
          }
        }, [context.logindata._id]);

      useEffect(() => {
        if(props.sendHospInfo!==undefined && props.patientInfo===undefined){
            let r = Math.random().toString(36).substr(2, 5);
            var strFirstThree = props.sendHospInfo.hospName.substring(0,3);
            dispatch({ type: 'mrNum', payload: strFirstThree.toUpperCase()+r.toUpperCase()})
            dispatch({ type: 'bedNum', payload: props.sendHospInfo.bed_id })
            dispatch({ type: 'hosp_id', payload: props.sendHospInfo.hospital_id })
        }
        }, [props.sendHospInfo]);

        useEffect(() => {         
            if(props.patientInfo!==undefined && props.patientInfo!==null){
                Object.assign(state,props.patientInfo);
                setPatientData(props.patientInfo);
                const payload={"patientId":props.patientInfo._id}
                setPatientId(props.patientInfo._id)
                retrieveDrug(payload).then((response) => {
                    if (response.suc){
                        setValuesToDrug(response.res);
                    }  
                });
            
                dispatch({ type: 'dtOfAdmission', payload: DateUtils.convertToDate(props.patientInfo.dtOfAdmission)});
                if(props.patientInfo.foodReactions){
                    setFoodEnable(true);
                }
                if(props.patientInfo.drugReactions){
                    setDrugEnable(true);
                }
                setEnableBtn(true);
                if(props.patientInfo.medicalHistory !== undefined && props.patientInfo.medicalHistory !== null 
                    && props.patientInfo.medicalHistory.length >0){
                        setMedication({medications:props.patientInfo.medicalHistory})
                }
                if(props.patientInfo.opdHistory && props.patientInfo.opdHistory.length >0){
                    state.historyOpd=props.patientInfo.opdHistory;
                }else{
                    state.historyOpd=[];
                }
            }
        }, [props.patientInfo]);

    // OnChange event we will call this and update the state
    const handleChange = (e) => {
        setErrorMsges([]);
        const { id, value } = e.target;
            if (id === "food") {
                if (state.foodReactions) {
                    setFoodEnable(false);
                    dispatch({ type: 'foodReactions', payload: false })
                } else {
                    setFoodEnable(true);
                    dispatch({ type: 'foodReactions', payload: true })
                }
              }else if (id === "drug") {
                if (state.drugReactions) {
                    setDrugEnable(false)
                    dispatch({ type: 'drugReactions', payload: false })
                } else {
                    setDrugEnable(true)
                    dispatch({ type: 'drugReactions', payload: true })
                }
              }else if (id === "dm") {
                if (state.dm) {
                    dispatch({ type: 'dm', payload: false })
                } else {
                    dispatch({ type: 'dm', payload: true })
                }
                }else if (id === "ihd") {
                    if (state.ihd) {
                        dispatch({ type: 'ihd', payload: false })
                    } else {
                        dispatch({ type: 'ihd', payload: true })
                }
                }else if (id === "cva") {
                    if (state.cva) {
                        dispatch({ type: 'cva', payload: false })
                    } else {
                        dispatch({ type: 'cva', payload: true })
                    }
                }else if (id === "crf") {
                    if (state.crf) {
                        dispatch({ type: 'crf', payload: false })
                    } else {
                        dispatch({ type: 'crf', payload: true })
                    }
                }else if (id === "htn") {
                    if (state.htn) {
                        dispatch({ type: 'htn', payload: false })
                    } else {
                        dispatch({ type: 'htn', payload: true })
                    }
                   }else if (id === "asthma") {
                    if (state.asthma) {
                        dispatch({ type: 'asthma', payload: false })
                    } else {
                        dispatch({ type: 'asthma', payload: true })
                    }
                }else if (id === "epilepsy") {
                    if (state.epilepsy) {
                        dispatch({ type: 'epilepsy', payload: false })
                    } else {
                        dispatch({ type: 'epilepsy', payload: true })
                    }
                }else if (id === "smoking") {
                    if (state.smoking) {
                        dispatch({ type: 'smoking', payload: false })
                    } else {
                        dispatch({ type: 'smoking', payload: true })
                    }
                }else if (id === "alcohol") {
                    if (state.alcohol) {
                        dispatch({ type: 'alcohol', payload: false })
                    } else {
                        dispatch({ type: 'alcohol', payload: true })
                    }
                }
                else{
                    dispatch({ type: id, payload: value });
                }
                
    }
    const hidemessage=() =>{
        setTimeout(function () {
            setNotiMessageShow(false);
            setNotiMessage('');
            setNotiVarient('');
        }, 5000);
      }

    const savePatientData = () => {
        if(validatePatient()){
        const convertAdmin = DateUtils.convertStringToDate(fromDate);
        state.medicalHistory=medication.medications;
        state.dtOfAdmission=convertAdmin;
        var patienId="";
        savePatient(state).then((resp) => {
            if (resp.suc) {
                patienId=resp.res._id;
                const payload={
                    "hid":props.sendHospInfo.hospital_id,
                    "bed_no":props.sendHospInfo.bed_id
                }
                if(enableBtn){
                    patienId=state._id;
                }else{
                    setNotiMessage('Patient Details Added Successfully..');
                    setNotiMessageShow(true);
                    setNotiVarient('success');
                    hidemessage();
                    localStorage.setItem("spm", "yes");
                }
                retrievePatient(payload).then((response) => {
                        setResponse(response);
                        if (patienId!==""){
                            var drugDesc="";
                            if(state.drugReactions){
                                drugDesc=state.drugDesc;
                            }
                            let drugsList=[];
                            if(medication.medications && medication.medications.length!==0){
                                for(let val of medication.medications){
                                  if(val.isrecncl){
                                    drugsList.push(val);
                                  }
                                }
                            }
                            const pload={
                                "name":state.name,
                                "mrNum":state.mrNum,
                                "patientId":patienId,
                                "drugReactions":drugDesc,
                                "drugsList":drugsList,
                                "sex":state.sex,
                                "age":state.age,
                                "email":(props.sendHospInfo && props.sendHospInfo.email) ? props.sendHospInfo.email:""
                           }
                           props.sendPatientInfo(pload);
                    }
                });
                if(!enableBtn){
                    window.location = "/eicu/admin/location/"+props.sendHospInfo.hcode+"/"+props.sendHospInfo.bed_id;
                }
            }else{
                setNotiMessageShow(true);
                setNotiMessage("Patient Creattion Failed");
                setNotiVarient('danger')
                hidemessage();
            }
        });
        window.scrollTo(0, 0);
    }
    }

    const dischargePatientPopup = ()=>{
        setIsLoadingMore(true);
        const payload={"patientId":props.patientInfo._id}
                setPatientId(props.patientInfo._id)
                retrieveDrug(payload).then((response) => {
                    if (response.suc){
                        setValuesToDrug(response.res);
                        setIsLoadingMore(false);
                    }else{
                        setShowDischarge(true);
                        setIsLoadingMore(false);
                    }  
                });
                setShowDischarge(true);
    }

    const validatePatient=()=>{
      let errorMsg =[];
      if(state.name===undefined || state.name === null || state.name === ""){
        errorMsg.push("Name must be provided")
      }else if(state.age===undefined || state.age === null || state.age === ""){
        errorMsg.push("Age must be provided")
      }else if(state.sex===undefined || state.sex === null || state.sex === ""){
        errorMsg.push("Sex must be provided")
      }else if(state.phoneNum===undefined || state.phoneNum === null || state.phoneNum === ""){
        errorMsg.push("Contact Number must be provided")
      }else if(state.address===undefined || state.address === null || state.address === ""){
        errorMsg.push("Address must be provided")
      }
      if(errorMsg.length > 0){
          setNotiMessageShow(true);
          setErrorMsges(errorMsg);
          setNotiVarient('danger')
          window.scrollTo(0,0);
          return false;
      }else{
        return true;
      }
    }
    
    const handleMedicationChange = idx => evt => {
        try {
          let newmedictions = medication.medications.map((obj, sidx) => {
            if (idx !== sidx) return obj;
            else if (evt.target.id === "isrecncl") {
                if (obj.isrecncl) {
                  return { ...obj, isrecncl: false };
                } else {
                  return { ...obj, isrecncl: true };
                }
              }
            else {
              return { ...obj, [evt.target.id]: evt.target.value };
            }
          });
          setMedication({ medications: newmedictions })
        } catch (err) {
         // log.error('Exception occured in CompanyComponent function---' + err);
        }
      }

      const handleRemoveMedication = idx => () => {
        setMedication({
            medications: medication.medications.filter((s, sidx) => idx !== sidx)
        });
      };
    
      const handleAddMedication = () => {
        setMedication({
            medications: medication.medications.concat([{serialNum: 0, name: "", dose: "", route: "", frq: "", isrecncl: false}])
        });
      };

      const setValuesToDrug =(drugData) => {
        let dataArr=[];
        drugData.map((itm, index) => {
            let data = {
                _id:itm._id,
                drugName: itm.drugName,
                dateTime:itm.dateTime,
                prescBy: itm.prescBy,
                isStop: itm.isStop,
                dose:  itm.dose,
                route: itm.route,
                freq: itm.freq,
                stopDate: itm.stopDate,
                stoppedBy:itm.stoppedBy,
                stat:itm.stat,
                infusion:itm.infusion
            }
            dataArr.push(data);
          })
          setDrugData({drugdata:dataArr});
      }

    const handleChangeDrug = idx => evt => {
        try {
          let newDrugs = drugs.drugdata.map((obj, sidx) => {
            if (idx !== sidx) return obj;
            else {
            return { ...obj, [evt.target.id]: evt.target.value };
            }
          });
          setDrugData({ drugdata: newDrugs })
        } catch (err) {
         // log.error('Exception occured in CompanyComponent function---' + err);
        }
      }
  

    const submitDischarge = () => {
        setShowPopup(true);
    }

    const closeBtnDischarge = () => {
        setShowDischarge(false);
    }

    const openOpdPopup=()=>{
        setShowOpdPopup(true)
    }

    const dischargePatientFn = () => {
        const payload={
           "_id":"",
           "usrid":context.logindata._id,
           "patientId":patientId,
           "drugdata":drugs.drugdata
        }
        console.log("Drugs request :"+JSON.stringify(payload));
        saveDrugData(payload).then((resp) => {
            if (resp.suc && props.sendHospInfo!==undefined){
                let req = {
                    hspCode : props.sendHospInfo.hcode,
                    bed_Num  : props.sendHospInfo.bed_id,
                    hid : props.sendHospInfo.hospital_id,
                    patient_id: patientId,
                    dischargedesc:state.discSummary,
                    diagnosis:state.diagnosis,
                    remarks:state.remarks,
                    loggedin_user:context.logindata._id
                }
                console.log("Discharge request :"+JSON.stringify(req))
                dischargePatient(req).then((resp) => {
                    if (resp.suc){
                        setNotiMessageShow(true);
                        setNotiMessage("Patient discharged successfully");
                        setNotiVarient('success')
                        hidemessage();
                        return window.location = "/eicu/admin/location/"+props.sendHospInfo.hcode+"/"+props.sendHospInfo.bed_id;
                    }else{
                        setNotiMessageShow(true);
                        setNotiMessage("Unable to discharge the patient");
                        setNotiVarient('danger')
                    }
                });
            }
        });
    }

    const sendEPrescription = () => {
                let req = {
                    hspCode : props.sendHospInfo.hcode,
                    bed_Num  : props.sendHospInfo.bed_id,
                    hid : props.sendHospInfo.hospital_id,
                    patient_id: patientId,
                    dischargedesc:"",
                    diagnosis:"",
                    remarks:"",
                    loggedin_user:context.logindata._id
                }
                console.log("EPRESCRIPT REQUEST :"+JSON.stringify(req))
                sendEprescription(req).then((resp) => {
                    if (resp.suc){
                        if(state.historyOpd && state.historyOpd.length >0){
                            let req = {
                                _id : resp.historyId,
                                createdDate : resp.historyCreationDt
                            }
                            let hist = [];
                            hist.push(req);
                            hist = [...hist, ...state.historyOpd];
                            state.historyOpd = hist;
                        }else{
                            let req = {
                                _id : resp.historyId,
                                createdDate : resp.historyCreationDt
                            }
                            state.historyOpd.push(req);
                        }
                        setNotiMessageShow(true);
                        setNotiMessage("E-Prescription sent successfully");
                        setNotiVarient('success')
                        hidemessage();
                        setShowOpdPopup(false)
                        window.scrollTo(0, 0);
                    }else{
                        setNotiMessageShow(true);
                        setNotiMessage("Unable to send the E-Prescription");
                        setNotiVarient('danger')
                    }
                });
    }
        

      // OnChange event we will call this and update the state
  const handleDateChange = idx => date => {
    const convertDate = DateUtils.convertStringToDate(date);
    let newData = drugs.drugdata.map((obj, sidx) => {
      if (idx !== sidx) return obj;
      return {
        ...obj, dateTime: convertDate
      };
    });
    setDrugData({ drugdata:newData})
  }

  const handleClose = () => {
    setShowHistory(false);
  }

  const viewOpd = async (notes) => {
   fetchOpdHistoryBasedId(notes).then((response) => {
        if (response !== null && response.suc) {
            if (response.res !== undefined && response.res !== null) {
                setViewHistory(response.res);
                setShowHistory(true);
                setIsLoadingMore(false);
            }
        }
    });
}

    return (
        <>

            <div className="container">
            <Alert className="notification" variant={notiVarient} show={notiMessageShow}
                onClose={() => setNotiMessageShow(false)}
                dismissible> {notiMessage}</Alert> 
                {errorMsges !== null && errorMsges.length > 0 && 
                    <Alert className="notification" variant={state.notiVarient} show={state.notiMessageShow}
                        onClose={() => dispatch({ type: "notiMessageShow", payload: false })}
                        dismissible> {errorMsges.map(itm => {
                            return(<li>{itm}</li>)
                        })}
                    </Alert>
                 }
                 {isLoadingMore ?  <InnerLoader/>:""}
                    <div className="cardMainlayout pt-0 mt-0">
                    <div className="contentArea">
                        <Form.Row>
                            <Form.Group as={Col} xs={3} controlId="name">
                                <Form.Control type="text" placeholder="Enter Name" autoComplete="off" value={state.name} onChange={handleChange} />
                                <Form.Label>Name <sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="age">
                                <Form.Control type="text" placeholder="Enter Age" autoComplete="off" value={state.age} onChange={handleChange} />
                                <Form.Label>Age <sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="sex">
                                <Form.Control as="select" value={state.sex} onChange={handleChange}>
                                    <option value="">Select</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </Form.Control>
                                <Form.Label>Sex <sup>*</sup></Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={3} controlId="mrNum">
                                <Form.Control type="text" placeholder="Enter MR" disabled="true" value={state.mrNum} onChange={handleChange} />
                                <Form.Label>MR Number <sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} className="selectplugin datePicker" controlId="dtOfAdmission">
                            <DatePicker
                            selected={fromDate}
                            value={state.dtOfAdmission}
                            onChange={date => setFromDate(date)}
                            fromDate={fromDate}
                            minDate={new Date()}
							/>
                            <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                            <Form.Label>Date of Admission</Form.Label>
                            </Form.Group>
                           
                            <Form.Group as={Col} xs={3} controlId="phoneNum">
                                <Form.Control type="text" placeholder="Contact Number" value={state.phoneNum} onChange={handleChange} />
                                <Form.Label>Contact Number <sup>*</sup></Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={9} controlId="address">
                                <Form.Control as="textarea" rows="2" placeholder="Address" value={state.address} onChange={handleChange} />
                                <Form.Label>Address<sup>*</sup></Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={9} controlId="sourOfHistory">
                                <Form.Control as="textarea" rows="2" placeholder="Sourece of History" value={state.sourOfHistory} onChange={handleChange} />
                                <Form.Label>Source of History</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={9} controlId="prestComplaints">
                                <Form.Control as="textarea" rows="2" placeholder="Preseinting Complaints" value={state.prestComplaints} onChange={handleChange} />
                                <Form.Label>Presenting Complaints</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={9} controlId="histyOfPrestIllness">
                                <Form.Control as="textarea" rows="2" placeholder="Hoistory of Present Illness" value={state.histyOfPrestIllness} onChange={handleChange} />
                                <Form.Label>History of Present Illness</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={9} controlId="medical">
                                <Form.Control as="textarea" rows="2" placeholder="Medical" value={state.medical} onChange={handleChange} />
                                <Form.Label>Medical</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                        <Form.Group as={Col}  xs={1} controlId="dm">
                                <Form.Check
                                    type="checkbox"
                                    id="dm"
                                    label="DM"
                                    checked={state.dm}
                                    value={state.dm} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={1} controlId="ihd">
                                <Form.Check
                                    type="checkbox"
                                    id="ihd"
                                    label="IHD"
                                    checked={state.ihd}
                                    value={state.ihd} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={1} controlId="cva">
                                <Form.Check
                                    type="checkbox"
                                    id="cva"
                                    label="CVA"
                                    checked={state.cva}
                                    value={state.cva} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={1} controlId="crf">
                                <Form.Check
                                    type="checkbox"
                                    id="crf"
                                    label="CRF"
                                    checked={state.crf}
                                    value={state.crf} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={1} controlId="htn">
                                <Form.Check
                                    type="checkbox"
                                    id="htn"
                                    label="HTN"
                                    checked={state.htn}
                                    value={state.htn} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={2} controlId="asthma">
                                <Form.Check
                                    type="checkbox"
                                    id="asthma"
                                    label="Asthma/COPD"
                                    checked={state.asthma}
                                    value={state.asthma} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={2} controlId="epilepsy">
                                <Form.Check
                                    type="checkbox"
                                    id="epilepsy"
                                    label="Epilepsy"
                                    checked={state.epilepsy}
                                    value={state.epilepsy} onChange={handleChange}
                                />
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                        <Form.Group as={Col} xs={9} controlId="significantHistory">
                                <Form.Control as="textarea" rows="2" placeholder="Significant History" value={state.significantHistory} onChange={handleChange} />
                                <Form.Label>Significant History</Form.Label>
                            </Form.Group>
                            </Form.Row>
                            <Form.Row>
                        <Form.Group as={Col}  xs={2} controlId="smoking" className="mb-0">
                                <Form.Check
                                    type="checkbox"
                                    id="smoking"
                                    label="Smoking"
                                    checked={state.smoking}
                                    value={state.smoking} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={2} controlId="alcohol" className="mb-0">
                                <Form.Check
                                    type="checkbox"
                                    id="alcohol"
                                    label="Alcohol Use"
                                    checked={state.alcohol}
                                    value={state.alcohol} onChange={handleChange}
                                />
                            </Form.Group>
                            </Form.Row>
                            <hr />
                            <span>Medication History</span>
        <div className="contact d-flex row">
          <div className="col-md-12">
          
            
              {medication.medications.map((contact, idx) => (
              <Form.Row className="medicationh">
                <React.Fragment key={idx}>
                  {/* <Form.Group as={Col} xs={1} controlId="name">
                  <Form.Control type="text" placeholder="Enter Name" disabled="true" value={idx+1}/>
                  </Form.Group> */}
                  <Form.Group as={Col} xs={3} controlId="name">
                    <Form.Control type="text" placeholder="Enter Name" value={contact.name} onChange={handleMedicationChange(idx)} />
                    <Form.Label>Name </Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={2} controlId="dose">
                    <Form.Control type="text" placeholder="Enter Email" value={contact.dose} onChange={handleMedicationChange(idx)} />
                    <Form.Label>Dose</Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={2} controlId="route">
                    <Form.Control type="text" placeholder="Route" value={contact.route} onChange={handleMedicationChange(idx)} />
                    <Form.Label>Route</Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={2} controlId="frq">
                    <Form.Control type="text" placeholder="Frequency" value={contact.frq} onChange={handleMedicationChange(idx)} />
                    <Form.Label>Frequency</Form.Label>
                  </Form.Group>
                  <Form.Group as={Col}  xs={2} controlId="isrecncl">
                   <Form.Check
                    type="checkbox"
                    id="isrecncl"
                    label="Reconciliation"
                    checked={contact.isrecncl}
                    value={contact.isrecncl}
                    onChange={handleMedicationChange(idx)}
                    />
                  </Form.Group>
                  {(idx > 0) ? (
                    <Col className="d-flex justify-content-center">
                  <Button
                    size="xs"
                    variant="link"
                    className="dlt iconBtn mt-1 ml-3 mr-3"
                    onClick={handleRemoveMedication(idx)}
                    type="submit"><Icon className="edit ml-1 mr-1" color="#475F7B" size={20} icon="delete" />
                  </Button></Col>
                  ) : ''}
                </React.Fragment>
              </Form.Row>
            ))}
            
            </div>
            <div className="col-sm-12 text-right"><Button
              size="xs"
              variant="link"
              className="iconBtn1 add btn-outline-primary btn-small px-4"
              onClick={handleAddMedication}
              type="submit"
            >Add</Button></div>
           </div>
           <hr />
                            <Form.Row>
                        <Form.Group as={Col}  xs={2} controlId="st">
                                <Form.Check
                                    type="checkbox"
                                    id="food"
                                    label="Food Reaction"
                                    checked={state.foodReactions}
                                    value={state.foodReactions} onChange={handleChange}
                                />
                            </Form.Group>
                            {foodEnable &&
                            <Form.Group as={Col} xs={9} controlId="foodDesc">
                                <Form.Control as="textarea" rows="2" placeholder="Food" value={state.foodDesc} onChange={handleChange} />
                                <Form.Label>Food</Form.Label>
                            </Form.Group>
                            }
                            </Form.Row>
                            <Form.Row>
                            <Form.Group as={Col}  xs={2} controlId="st">
                                <Form.Check
                                    type="checkbox"
                                    id="drug"
                                    label="Drug Reaction"
                                    checked={state.drugReactions}
                                    value={state.drugReactions} onChange={handleChange}
                                />
                            </Form.Group>
                            {drugEnable &&
                            <Form.Group as={Col} xs={9} controlId="drugDesc">
                                <Form.Control as="textarea" rows="2" placeholder="Drug" value={state.drugDesc} onChange={handleChange} />
                                <Form.Label>Drug</Form.Label>
                            </Form.Group>
                             }
                        </Form.Row>
                        
                        <div><span>Vital Data</span></div>
                        <Form.Row className="medicationh">
                          <Form.Group as={Col} xs={2} controlId="pulse">
                                <Form.Control type="text" placeholder="Enter Address" value={state.pulse} onChange={handleChange} />
                                <Form.Label>Pulse /min </Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={2} controlId="bp">
                                <Form.Control type="text" placeholder="Enter Address" value={state.bp} onChange={handleChange} />
                                <Form.Label>BP mmHg </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="resp">
                                <Form.Control type="text" placeholder="Enter Address" value={state.resp} onChange={handleChange} />
                                <Form.Label>Resp /min </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="temp">
                                <Form.Control type="text" placeholder="Enter Address" value={state.temp} onChange={handleChange} />
                                <Form.Label>Temp F</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="spo">
                                <Form.Control type="text" placeholder="Enter Address" value={state.spo} onChange={handleChange} />
                                <Form.Label>SPO2 </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="wt">
                                <Form.Control type="text" placeholder="Enter Address" value={state.wt} onChange={handleChange} />
                                <Form.Label>WT Kgs </Form.Label>
                            </Form.Group>
                         </Form.Row>
                         <Form.Row className="mt-4">
                          <Form.Group as={Col} xs={4} controlId="respiratory">
                                <Form.Control type="text" placeholder="Enter Address" value={state.respiratory} onChange={handleChange} />
                                <Form.Label>Respiratory </Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={4} controlId="abdominal">
                                <Form.Control type="text" placeholder="Enter Address" value={state.abdominal} onChange={handleChange} />
                                <Form.Label>Abdominal </Form.Label>
                            </Form.Group>
                            </Form.Row>
                          <Form.Row>
                            <Form.Group as={Col} xs={4} controlId="neurology">
                                <Form.Control type="text" placeholder="Enter Address" value={state.neurology} onChange={handleChange} />
                                <Form.Label>Neurology </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={4} controlId="musculoskeletal">
                                <Form.Control type="text" placeholder="Enter Address" value={state.musculoskeletal} onChange={handleChange} />
                                <Form.Label>Musculoskeletal </Form.Label>
                            </Form.Group>
                            </Form.Row>
                             <Form.Row>
                             <Form.Group as={Col} xs={9} controlId="otherSystems">
                                <Form.Control as="textarea" rows="1" placeholder="Other Systems" value={state.otherSystems} onChange={handleChange} />
                                <Form.Label>Other Systems</Form.Label>
                            </Form.Group>
                             </Form.Row>
                             <Form.Row>
                             <Form.Group as={Col} xs={9} controlId="provisionalDiagnosis">
                                <Form.Control as="textarea" rows="4" placeholder="Provisional Diagnosis" value={state.provisionalDiagnosis} onChange={handleChange} />
                                <Form.Label>Provisional Diagnosis</Form.Label>
                            </Form.Group>
                             </Form.Row>
                             <Form.Row>
                             <Form.Group as={Col} xs={9} controlId="planOfCare">
                                <Form.Control as="textarea" rows="6" placeholder="Plan of care" value={state.planOfCare} onChange={handleChange} />
                                <Form.Label>Plan of care</Form.Label>
                            </Form.Group>
                             </Form.Row>
                             <div class="row">
                             <div className="text-left buttonGrop pb-0 pl-1 col-sm-6">
                                {enableBtn &&
                                        <Button
                                            size="xs"
                                            variant="outline-primary"
                                            onClick={dischargePatientPopup}
                                            loading={state.loadOfice}
                                            type="submit"
                                        > Discharge </Button>
                                    }
                                    {enableBtn &&
                                        <Button
                                            size="xs"
                                            variant="outline-primary"
                                            onClick={openOpdPopup}
                                            loading={state.loadOfice}
                                            type="submit"
                                        > OPD </Button>
                                    }
                             </div>
                            
                             <div className="text-right buttonGrop pb-0 col-sm-6 pr-3">
                          {enableBtn && context.logindata.permissions!==undefined && context.logindata.permissions.includes("EP") &&
                            <Button
                                size="xs"
                                variant="outline-primary"
                                onClick={savePatientData}
                                type="submit"
                                loading={state.loadOfice}
                            > Update</Button>
                            }
                            {!enableBtn && context.logindata.permissions!==undefined && context.logindata.permissions.includes("AP") &&
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    onClick={savePatientData}
                                    loading={state.loadOfice}
                                    type="submit"
                                > Save </Button>
                            }
                        </div>
                      </div>
                    </div>
                </div>
            </div>

            {enableBtn &&
            <div className="cardMainlayout pt-0 min-height">
                    <Table hover>
                        <thead>
                            <tr>
                                <th>OPD History</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>
                            {state.historyOpd.length > 0 && state.historyOpd.map((notes, index) => (
                                <React.Fragment key={index}>
                                    <tr>
                                        <td>
                                            <span>{"OPD ("+notes.createdDate+")"}</span>
                                        </td>
                                        <td>
                                            <span><Button
                                                size="xs"
                                                variant="outline-primary"
                                                onClick={() => viewOpd(notes)}
                                                type="submit"> View </Button></span>
                                        </td>
                                    </tr>
                                    {showHistory &&
                                        <OpdHistory openPopup={showHistory} handleClose={handleClose} 
                                        viewHistory = {viewHistory} patientMinimalInfo= {patientData}/>
                                    }
                                </React.Fragment>
                            ))}
                            {state.historyOpd.length === 0 && (
                                <div>
                                    No Records found
                                </div>
                            )
                            }
                        </tbody>
                    </Table>
                </div>}

            {showDischarge &&
            <Modal
                show={showDischarge}
                onHide={() => setShowDischarge(false)}
                backdrop="static"
                size="lg"
                className="importPNRMd1"
                keyboard={false}>
                    <Modal.Header>
                    <Modal.Title>Discharge Summary :  <b>{patientData!==undefined && patientData.name} ({patientData!==undefined && patientData.mrNum})</b></Modal.Title>
                    <Button variant="secondary" onClick={closeBtnDischarge}>Close</Button>
                    </Modal.Header>
                    <Modal.Body>
                    <div className="container">
                   {patientData!==undefined &&
                    <div className="cardMainlayout pt-0 mt-0">
                    <div className="contentArea">
                        <Form.Row>
                            <Form.Group as={Col} xs={3} controlId="name">
                                <Form.Control type="text" disabled="true" placeholder="Enter Name" autoComplete="off" value={patientData.name} onChange={handleChange} />
                                <Form.Label>Name <sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="age">
                                <Form.Control type="text" disabled="true" placeholder="Enter Age" autoComplete="off" value={patientData.age} onChange={handleChange} />
                                <Form.Label>Age <sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="sex">
                                <Form.Control as="select" disabled="true" value={patientData.sex} onChange={handleChange}>
                                    <option value="">Select</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </Form.Control>
                                <Form.Label>Sex <sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="phoneNum">
                                <Form.Control type="text" disabled="true" placeholder="Contact Number" value={patientData.phoneNum} onChange={handleChange} />
                                <Form.Label>Contact Number <sup>*</sup></Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={3} controlId="mrNum">
                                <Form.Control type="text" disabled="true" placeholder="Enter MR" disabled="true" value={patientData.mrNum} onChange={handleChange} />
                                <Form.Label>MR Number <sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} className="selectplugin datePicker" controlId="dtOfAdmission">
                            <DatePicker
                            selected={fromDate}
                            value={new Date(patientData.dtOfAdmission)}
                            //onChange={date => setFromDate(date)}
                            fromDate={fromDate}
                            disabled="true"
                            minDate={new Date()}
							/>
                            <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                            <Form.Label>Date of Admission</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} className="selectplugin datePicker" controlId="dtOfAdmission">
                            <DatePicker
                            disabled="true"
                            selected={new Date()}
                            value={new Date()}
                            //onChange={date => setFromDate(date)}
                            fromDate={new Date()}
                            minDate={new Date()}
							/>
                            <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                            <Form.Label>Date of Discharge</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={10} controlId="address">
                                <Form.Control as="textarea" disabled="true" rows="2" placeholder="Address" value={patientData.address} onChange={handleChange} />
                                <Form.Label>Address<sup>*</sup></Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={10} controlId="sourOfHistory">
                                <Form.Control as="textarea"  disabled="true" rows="2" placeholder="Sourece of History" value={patientData.sourOfHistory} onChange={handleChange} />
                                <Form.Label>Source of History</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={10} controlId="prestComplaints">
                                <Form.Control as="textarea" disabled="true" rows="2" placeholder="Preseinting Complaints" value={patientData.prestComplaints} onChange={handleChange} />
                                <Form.Label>Presenting Complaints</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={10} controlId="histyOfPrestIllness">
                                <Form.Control as="textarea" disabled="true" rows="2" placeholder="Hoistory of Present Illness" value={patientData.histyOfPrestIllness} onChange={handleChange} />
                                <Form.Label>History of Present Illness</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={10} controlId="medical">
                                <Form.Control as="textarea" disabled="true" rows="2" placeholder="Medical" value={patientData.medical} onChange={handleChange} />
                                <Form.Label>Medical</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                        <Form.Group as={Col}  xs={1} controlId="dm">
                                <Form.Check
                                    type="checkbox"
                                    id="dm"
                                    label="DM"
                                    checked={patientData.dm}
                                    disabled="true"
                                    value={patientData.dm} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={1} controlId="ihd">
                                <Form.Check
                                    type="checkbox"
                                    id="ihd"
                                    label="IHD"
                                    checked={patientData.ihd}
                                    disabled="true"
                                    value={patientData.ihd} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={1} controlId="cva">
                                <Form.Check
                                    type="checkbox"
                                    id="cva"
                                    label="CVA"
                                    checked={patientData.cva}
                                    disabled="true"
                                    value={patientData.cva} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={1} controlId="crf">
                                <Form.Check
                                    type="checkbox"
                                    id="crf"
                                    label="CRF"
                                    checked={patientData.crf}
                                    disabled="true"
                                    value={patientData.crf} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={1} controlId="htn">
                                <Form.Check
                                    type="checkbox"
                                    id="htn"
                                    label="HTN"
                                    checked={patientData.htn}
                                    disabled="true"
                                    value={patientData.htn} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={2} controlId="asthma">
                                <Form.Check
                                    type="checkbox"
                                    id="asthma"
                                    label="Asthma/COPD"
                                    checked={patientData.asthma}
                                    disabled="true"
                                    value={patientData.asthma} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={2} controlId="epilepsy">
                                <Form.Check
                                    type="checkbox"
                                    id="epilepsy"
                                    label="Epilepsy"
                                    disabled="true"
                                    checked={patientData.epilepsy}
                                    value={patientData.epilepsy} onChange={handleChange}
                                />
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                        <Form.Group as={Col} xs={10} controlId="significantHistory">
                                <Form.Control as="textarea" disabled="true" rows="2" placeholder="Significant History" value={patientData.significantHistory} onChange={handleChange} />
                                <Form.Label>Significant History</Form.Label>
                            </Form.Group>
                            </Form.Row>
                            <Form.Row>
                        <Form.Group as={Col}  xs={2} controlId="smoking" className="mb-0">
                                <Form.Check
                                    type="checkbox"
                                    id="smoking"
                                    label="Smoking"
                                    checked={patientData.smoking}
                                    disabled="true"
                                    value={patientData.smoking} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={2} controlId="alcohol" className="mb-0">
                                <Form.Check
                                    type="checkbox"
                                    id="alcohol"
                                    label="Alcohol Use"
                                    checked={patientData.alcohol}
                                    disabled="true"
                                    value={patientData.alcohol} onChange={handleChange}
                                />
                            </Form.Group>
                            </Form.Row>
                            <hr />
        {patientData.medicalHistory.length!==0 &&
        <>
        <span>Medication History</span>
        <div className="contact d-flex row">
          <div className="col-md-12">
              {patientData.medicalHistory.map((contact, idx) => (
              <Form.Row className="medicationh">
                <React.Fragment key={idx}>
                  <Form.Group as={Col} xs={3} controlId="name">
                    <Form.Control type="text" disabled="true" placeholder="Enter Name" value={contact.name} />
                    <Form.Label>Name </Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={2} controlId="dose">
                    <Form.Control type="text" disabled="true" placeholder="Enter Email" value={contact.dose}  />
                    <Form.Label>Dose</Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={2} controlId="route">
                    <Form.Control type="text" disabled="true" placeholder="Route" value={contact.route} />
                    <Form.Label>Route</Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={2} controlId="frq">
                    <Form.Control type="text" disabled="true" placeholder="Frequency" value={contact.frq}  />
                    <Form.Label>Frequency</Form.Label>
                  </Form.Group>
                  <Form.Group as={Col}  xs={2} controlId="isrecncl">
                   <Form.Check
                    type="checkbox"
                    id="isrecncl"
                    label="Reconciliation"
                    checked={contact.isrecncl}
                    disabled="true"
                    value={contact.isrecncl}
                    //onChange={handleMedicationChange(idx)}
                    />
                  </Form.Group>
                </React.Fragment>
              </Form.Row>
            ))}
            
            </div>
           </div>
           </>
                   }
           <hr />
                            <Form.Row>
                        <Form.Group as={Col}  xs={2} controlId="st">
                                <Form.Check
                                    type="checkbox"
                                    id="food"
                                    label="Food Reaction"
                                    checked={patientData.foodReactions}
                                    disabled="true"
                                    value={patientData.foodReactions} onChange={handleChange}
                                />
                            </Form.Group>
                            {patientData.foodReactions &&
                            <Form.Group as={Col} xs={10} controlId="foodDesc">
                                <Form.Control as="textarea" rows="2" disabled="true" placeholder="Food" value={patientData.foodDesc} onChange={handleChange} />
                                <Form.Label>Food</Form.Label>
                            </Form.Group>
                            }
                            </Form.Row>
                            <Form.Row>
                            <Form.Group as={Col}  xs={2} controlId="st">
                                <Form.Check
                                    type="checkbox"
                                    id="drug"
                                    disabled="true"
                                    label="Drug Reaction"
                                    checked={patientData.drugReactions}
                                    value={patientData.drugReactions} onChange={handleChange}
                                />
                            </Form.Group>
                            {patientData.drugReactions &&
                            <Form.Group as={Col} xs={10} controlId="drugDesc">
                                <Form.Control as="textarea" disabled="true" rows="2" placeholder="Drug" value={patientData.drugDesc} onChange={handleChange} />
                                <Form.Label>Drug</Form.Label>
                            </Form.Group>
                             }
                        </Form.Row>
                        
                        <div><span>Vital Data</span></div>
                        <Form.Row className="medicationh">
                          <Form.Group as={Col} xs={2} controlId="pulse">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.pulse} onChange={handleChange} />
                                <Form.Label>Pulse /min </Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={2} controlId="bp">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.bp} onChange={handleChange} />
                                <Form.Label>BP mmHg </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="resp">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.resp} onChange={handleChange} />
                                <Form.Label>Resp /min </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="temp">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.temp} onChange={handleChange} />
                                <Form.Label>Temp F</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="spo">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.spo} onChange={handleChange} />
                                <Form.Label>SPO2 </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="wt">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.wt} onChange={handleChange} />
                                <Form.Label>WT Kgs </Form.Label>
                            </Form.Group>
                         </Form.Row>
                         <Form.Row className="mt-4">
                          <Form.Group as={Col} xs={4} controlId="respiratory">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.respiratory} onChange={handleChange} />
                                <Form.Label>Respiratory </Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={4} controlId="abdominal">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.abdominal} onChange={handleChange} />
                                <Form.Label>Abdominal </Form.Label>
                            </Form.Group>
                            </Form.Row>
                          <Form.Row>
                            <Form.Group as={Col} xs={4} controlId="neurology">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.neurology} onChange={handleChange} />
                                <Form.Label>Neurology </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={4} controlId="musculoskeletal">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.musculoskeletal} onChange={handleChange} />
                                <Form.Label>Musculoskeletal </Form.Label>
                            </Form.Group>
                            </Form.Row>
                             <Form.Row>
                             <Form.Group as={Col} xs={10} controlId="otherSystems">
                                <Form.Control as="textarea" disabled="true" rows="1" placeholder="Other Systems" value={patientData.otherSystems} onChange={handleChange} />
                                <Form.Label>Other Systems</Form.Label>
                            </Form.Group>
                             </Form.Row>
                             <Form.Row>
                             <Form.Group as={Col} xs={10} controlId="provisionalDiagnosis">
                                <Form.Control as="textarea" disabled="true" rows="4" placeholder="Provisional Diagnosis" value={patientData.provisionalDiagnosis} onChange={handleChange} />
                                <Form.Label>Provisional Diagnosis</Form.Label>
                            </Form.Group>
                             </Form.Row>
                             <Form.Row>
                             <Form.Group as={Col} xs={10} controlId="diagnosis">
                                <Form.Control as="textarea" rows="4" placeholder="Diagnosis" value={state.diagnosis} onChange={handleChange} />
                                <Form.Label>Diagnosis</Form.Label>
                            </Form.Group>
                             </Form.Row>
                             <Form.Row>
                             <Form.Group as={Col} xs={10} controlId="discSummary">
                                <Form.Control as="textarea" rows="4" placeholder="Summary" value={state.discSummary} onChange={handleChange} />
                                <Form.Label>Summary</Form.Label>
                            </Form.Group>
                             </Form.Row>

            <span>Discharge Advise</span>
            <hr />
            <div className="cardMainlayout pt-0 min-height">
            <div className="container drugchart mt-3">
            {drugs.drugdata && drugs.drugdata.map((drug, idx) => (
            <>
            {!drug.isStop &&
             <>
              <Form.Row className="divider"
              className={"divider " + (drug.isStop ? ' in-active' : '')}>
                <div className="col-sm-12">
                {!drug.infusion && !drug.stat &&
                <h6 className="mb-3 text-primary">Drug</h6>
                }
                {drug.infusion &&
                <h6 className="mb-3 text-primary">Drug Infusions</h6>
                }
                {drug.stat &&
                <h6 className="mb-3 text-primary">STAT Drug </h6>
                }
               </div>
                <React.Fragment key={idx}>
                  <Form.Group as={Col} xs={3} controlId="drugName">
                    <Form.Control type="text" placeholder="Enter Name" disabled={drug.isStop} value={drug.drugName} onChange={handleChangeDrug(idx)} />
                    <Form.Label>Drug Name <sup>*</sup></Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={3} controlId="prescBy">
                    <Form.Control type="text" disabled="true" placeholder="Enter Email" value={drug.prescBy} onChange={handleChangeDrug(idx)} />
                    <Form.Label>Prescribed By<sup>*</sup></Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={3} className="selectplugin datePicker" controlId="dob">
                            <DatePicker
                            selected={drugdate}
                            onChange={handleDateChange(idx)}
                            value={drug.dateTime}
                            minDate={new Date()}
                            dateFormat="dd-MMM-YYY HH:mm"
							 />
                            <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                            {!drug.stat &&
                            <Form.Label>Start Date & Time <sup>*</sup></Form.Label>
                            }
                            {drug.stat &&
                            <Form.Label>Date & Time <sup>*</sup></Form.Label>
                            }
                            </Form.Group>  
                  <Form.Row>
                  <Form.Group as={Col} xs={2} controlId="dose">
                    <Form.Control type="text" placeholder="Dose" disabled={drug.isStop} value={drug.dose}  onChange={handleChangeDrug(idx)} />
                    <Form.Label>Dose</Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={2} controlId="route">
                    <Form.Control type="text" placeholder="Route"  disabled={drug.isStop} value={drug.route} onChange={handleChangeDrug(idx)} />
                    <Form.Label>Route</Form.Label>
                  </Form.Group>
                  {!drug.infusion && !drug.stat &&
                  <Form.Group as={Col} xs={2} controlId="freq">
                    <Form.Control type="text" placeholder="Frequency"  disabled={drug.isStop} value={drug.freq} onChange={handleChangeDrug(idx)} />
                    <Form.Label>Frequency</Form.Label>
                  </Form.Group>
                  } 
                  {drug.infusion && !drug.stat &&
                  <Form.Group as={Col} xs={2} controlId="freq">
                    <Form.Control type="text" placeholder="Frequency"  disabled={true} value={drug.freq} onChange={handleChangeDrug(idx)} />
                    <Form.Label>Frequency</Form.Label>
                  </Form.Group>
                  }
                  {drug.isStop && !drug.stat &&
                  <Form.Group as={Col} xs={3} controlId="stoppedBy">
                    <Form.Control type="text" disabled="true" placeholder="Enter Email" value={drug.stoppedBy} />
                    <Form.Label>Stopped By<sup>*</sup></Form.Label>
                  </Form.Group>
                  }
                   {!drug.stat &&
                  <Form.Group as={Col} xs={3} className="selectplugin datePicker" controlId="dob">
                            <DatePicker
                            //selected={drugdate}
                            value={drug.stopDate}
                            minDate={new Date()}
                            dateFormat="dd-MMM-YYY HH:mm"
                            disabled={true}
						 />
                            <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                            <Form.Label>Stop Date & Time</Form.Label>
                            </Form.Group>
                      }
                    </Form.Row>                 
                </React.Fragment>
              </Form.Row>
              </>
            }
            </>
            ))}
            </div></div>
                             <Form.Row>
                             <Form.Group as={Col} xs={12} controlId="remarks">
                                <Form.Control as="textarea" rows="4" placeholder="Plan of care" value={state.remarks} onChange={handleChange} />
                            </Form.Group>
                             </Form.Row>
                             <div class="row">
                             <div className="text-left buttonGrop pb-0 pl-1 col-sm-6">
                                        <Button
                                            size="xs"
                                            variant="outline-primary"
                                            onClick={submitDischarge}
                                            loading={patientData.loadOfice}
                                            type="submit"
                                        >Submit Discharge 
                                        </Button>
                             </div>
                             </div>

                        {showPopup &&
                                <Modal
                                    show={showPopup}
                                    onHide={() => setShowPopup(false)}
                                    backdrop="static"
                                    size="small"
                                    className="importPNRMd1"
                                    keyboard={false}>
                                        <Modal.Header>
                                            <Modal.Title>Discharge Patient</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                        <div className="icons warning"></div>
                                                <div>
                                                    <Form.Label className="custom-dailog-sub-title">Patient Name : <b>{patientData.name}</b>  <span class="ml-4"> MR Number : <b>{patientData.mrNum}</b></span></Form.Label>
                                                    <Form.Label className="custom-dailog-title">Are you sure you want to discharge this patient ?</Form.Label>
                                                </div>
                                                <div className="text-center buttonGrop">
                                                    <Button 
                                                        size="xs"
                                                        className="mr-3"
                                                        variant="outline-primary"
                                                        onClick={dischargePatientFn}
                                                        type="submit"> Yes </Button>
                                                    <Button
                                                        size="xs"
                                                        variant="outline-dark"
                                                        onClick={() => setShowPopup(false)}
                                                        type="submit"> No </Button>
                                                </div>
                                            
                                         </Modal.Body>
                                </Modal>
                            }

                    </div>
                </div>
                }
            </div>
                                            
                                         </Modal.Body>
                                </Modal>
                            }

{showOpdPopup &&
                                <Modal
                                    show={showOpdPopup}
                                    onHide={() => setShowOpdPopup(false)}
                                    backdrop="static"
                                    size="small"
                                    className="importPNRMd1"
                                    keyboard={false}>
                                        <Modal.Header closeButton>
                                            <Modal.Title>ePrescription OPD</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                        <div className="icons warning"></div>
                                                <div>
                                                    <Form.Label className="custom-dailog-title">Are you sure you want to send ePrescription?</Form.Label>
                                                </div>
                                                <div className="text-center buttonGrop">
                                                    <Button 
                                                        size="xs"
                                                        className="mr-3"
                                                        variant="outline-primary"
                                                        onClick={sendEPrescription}
                                                        type="submit"> Yes </Button>
                                                    <Button
                                                        size="xs"
                                                        variant="outline-dark"
                                                        onClick={() => setShowOpdPopup(false)}
                                                        type="submit"> No </Button>
                                                </div>
                                            
                                         </Modal.Body>
                                </Modal>
                            }
           
        </>
    )
}
export default PatientForm