import React, { useEffect, useReducer, useState } from 'react';
import { Modal, Table, Form, Col } from "react-bootstrap";
import "react-datepicker/dist/react-datepicker.css";
import { useHistory } from 'react-router-dom';
import Button from '../../../common/buttons/Button';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import Icon from '../../../common/icomoonIcon/Icon';
import DateUtils from '../../../commonUtilities/DateUtils';
const initialState = {
    "_id": "", "usrid": "", "bedNum": 0, "hosp_id": "", "name": "", "age": "", "sex": "", "address": "",
    "phoneNum": "", "mrNum": "", "dtOfAdmission": null, "sourOfHistory": "", "prestComplaints": "", "histyOfPrestIllness": "", "medical": "",
    "dm": false, "ihd": false, "cva": false, "crf": false, "htn": false, "asthma": false, "epilepsy": false, "significantHistory": "",
    "smoking": false, "alcohol": false, "foodReactions": false, "foodDesc": "", "drugDesc": "", "drugReactions": false, "pulse": "", "bp": "", "resp": "",
    "temp": "", "spo": "", "wt": "", "respiratory": "", "abdominal": "", "neurology": "", "musculoskeletal": "",
    "otherSystems": "", "provisionalDiagnosis": "", "planOfCare": "", discSummary: "", remarks: "", diagnosis: "", createdDate :""
};

const reducer = (state, action) => {
    switch (action.type) {
        default:
            return { ...state, [action.type]: action.payload };
    }
};

function OpdHistory(props) {
    const [show, setShow] = useState(props.openPopup);
    const [state, dispatch] = useReducer(reducer, initialState);
    const history = useHistory();
    const [drugChat, setDrugChat] = useState([]);
    const [patientMinimalInfo, setPatientMinimalInfo] = useState(props.patientMinimalInfo);
    const [drugEnable, setDrugEnable] = useState(false);
    const [foodEnable, setFoodEnable] = useState(false);
    const [fromDate, setFromDate] = useState(new Date());
    const [medication, setMedication] = useState({medications: []});

    useEffect(() => {
        setValuesToHistory(props.viewHistory);
    }, []);

    const handleClose = () => {
        props.handleClose(false);
    }

    const setValuesToHistory = (history) => {
        let historyData = history
        if(history.medicalHistory !== undefined && history.medicalHistory !== null 
            && history.medicalHistory.length >0){
                setMedication({medications:history.medicalHistory})
        }
        dispatch({ type: 'createdDate', payload: historyData.createdDate });
        dispatch({ type: 'name', payload: historyData.name });
        dispatch({ type: 'age', payload: historyData.age });
        dispatch({ type: 'sex', payload: historyData.sex });
        dispatch({ type: 'mrNum', payload: historyData.mrNum });
        dispatch({ type: 'phoneNum', payload: historyData.phoneNum });
        dispatch({ type: 'address', payload: historyData.address });
        dispatch({ type: 'sourOfHistory', payload: historyData.sourOfHistory });
        dispatch({ type: 'prestComplaints', payload: historyData.prestComplaints });
        dispatch({ type: 'histyOfPrestIllness', payload: historyData.histyOfPrestIllness });
        dispatch({ type: 'dm', payload: historyData.dm });
        dispatch({ type: 'ihd', payload: historyData.ihd });
        dispatch({ type: 'cva', payload: historyData.cva });
        dispatch({ type: 'crf', payload: historyData.crf });
        dispatch({ type: 'htn', payload: historyData.htn });
        dispatch({ type: 'epilepsy', payload: historyData.epilepsy });
        dispatch({ type: 'asthma', payload: historyData.asthma });
        dispatch({ type: 'significantHistory', payload: historyData.significantHistory });
        dispatch({ type: 'medical', payload: historyData.medical });
        dispatch({ type: 'smoking', payload: historyData.smoking });
        dispatch({ type: 'alcohol', payload: historyData.alcohol });
        dispatch({ type: 'foodReactions', payload: historyData.foodReactions });
        if(historyData.foodReactions && historyData.foodReactions === true){
            setFoodEnable(true);
        }
        dispatch({ type: 'foodDesc', payload: historyData.foodDesc });
        dispatch({ type: 'drugReactions', payload: historyData.drugReactions });
        if(historyData.drugReactions && historyData.drugReactions === true){
            setDrugEnable(true);
        }
        dispatch({ type: 'drugDesc', payload: historyData.drugDesc });
        dispatch({ type: 'pulse', payload: historyData.pulse });
        dispatch({ type: 'bp', payload: historyData.bp });
        dispatch({ type: 'resp', payload: historyData.resp });
        dispatch({ type: 'temp', payload: historyData.temp });
        dispatch({ type: 'spo', payload: historyData.spo });
        dispatch({ type: 'wt', payload: historyData.wt });
        dispatch({ type: 'respiratory', payload: historyData.respiratory });
        dispatch({ type: 'abdominal', payload: historyData.abdominal });
        dispatch({ type: 'neurology', payload: historyData.neurology });
        dispatch({ type: 'musculoskeletal', payload: historyData.musculoskeletal });
        dispatch({ type: 'otherSystems', payload: historyData.otherSystems });
        dispatch({ type: 'provisionalDiagnosis', payload: historyData.provisionalDiagnosis });
        dispatch({ type: 'planOfCare', payload: historyData.planOfCare });
        dispatch({ type: 'remarks', payload: historyData.remarks });
        dispatch({ type: 'diagnosis', payload: historyData.diagnosis });
        dispatch({ type: 'historyOpd', payload: historyData.historyOpd });
    }

    return (
        <>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                size="lg"
                keyboard={false}>
                <Modal.Header>
                    <Modal.Title>ePrescription OPD
                </Modal.Title>
                    <Button variant="secondary" onClick={handleClose}>Close</Button>
                </Modal.Header>

                <Modal.Body>
                    <div className="cardMainlayout pt-0 min-height">
                        <Table hover>
                            <thead>
                                <tr>
                                    <th>Patient Name</th>
                                    <th>Age</th>
                                    <th>Sex</th>
                                    <th>MR Number</th>
                                    <th>Contact Number</th>
                                </tr>
                            </thead>
                            <tbody>
                                {patientMinimalInfo !== undefined && patientMinimalInfo !== null &&
                                    <React.Fragment>
                                        <tr>
                                            <td>
                                                <span>{patientMinimalInfo.name}</span>
                                            </td>
                                            <td>
                                                <span>{patientMinimalInfo.age}</span>
                                            </td>
                                            <td>
                                                <span>{patientMinimalInfo.sex}</span>
                                            </td>
                                            <td>
                                                <span>{patientMinimalInfo.mrNum}</span>
                                            </td>
                                            <td>
                                                <span>{patientMinimalInfo.phoneNum}</span>
                                            </td>
                                        </tr>
                                    </React.Fragment>
                                }
                            </tbody>
                        </Table>
                    </div>

                    <div className="cardMainlayout pt-0 mt-0">
                        <h4>OPD Information (Date : {DateUtils.convertToDateFormate(state.createdDate)})</h4>
                        <div className="contentArea">
                            {/*<Form.Row>
                                 <Form.Group as={Col} xs={3} controlId="name">
                                    <Form.Control type="text" placeholder="Enter Name" autoComplete="off" value={state.name}  disabled/>
                                    <Form.Label>Name </Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="age">
                                    <Form.Control type="text" placeholder="Enter Age" autoComplete="off" value={state.age} disabled />
                                    <Form.Label>Age </Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={3} controlId="sex">
                                    <Form.Control as="select" value={state.sex} disabled>
                                        <option value="">Select</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </Form.Control>
                                    <Form.Label>Sex </Form.Label>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={3} controlId="mrNum">
                                    <Form.Control type="text" placeholder="Enter MR" disabled="true" value={state.mrNum}  disabled />
                                    <Form.Label>MR Number </Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={3} className="selectplugin datePicker" controlId="dtOfAdmission">
                                    <DatePicker
                                        selected={fromDate}
                                        value={state.createdDate}
                                        fromDate={fromDate}
                                        dateFormat="dd-MMM-YYY HH:mm"
                                        disabled
                                        minDate={new Date()}
                                    />
                                    <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                                    <Form.Label>Date</Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={3} controlId="phoneNum">
                                    <Form.Control type="text" placeholder="Contact Number" value={state.phoneNum}  disabled/>
                                    <Form.Label>Contact Number </Form.Label>
                                </Form.Group>
                            </Form.Row> */}
                            <Form.Row>
                                <Form.Group as={Col} xs={9} controlId="address">
                                    <Form.Control as="textarea" rows="2" placeholder="Address" value={state.address}  disabled/>
                                    <Form.Label>Address</Form.Label>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={9} controlId="sourOfHistory">
                                    <Form.Control as="textarea" rows="2" placeholder="Sourece of History" value={state.sourOfHistory} disabled />
                                    <Form.Label>Source of History</Form.Label>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={9} controlId="prestComplaints">
                                    <Form.Control as="textarea" rows="2" placeholder="Preseinting Complaints" value={state.prestComplaints}  disabled/>
                                    <Form.Label>Presenting Complaints</Form.Label>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={9} controlId="histyOfPrestIllness">
                                    <Form.Control as="textarea" rows="2" placeholder="Hoistory of Present Illness" value={state.histyOfPrestIllness}  disabled/>
                                    <Form.Label>History of Present Illness</Form.Label>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={9} controlId="medical">
                                    <Form.Control as="textarea" rows="2" placeholder="Medical" value={state.medical} disabled />
                                    <Form.Label>Medical</Form.Label>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={1} controlId="dm">
                                    <Form.Check
                                        type="checkbox"
                                        id="dm"
                                        label="DM"
                                        disabled
                                        checked={state.dm}
                                        value={state.dm} 
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={1} controlId="ihd">
                                    <Form.Check
                                        type="checkbox"
                                        id="ihd"
                                        disabled
                                        label="IHD"
                                        checked={state.ihd}
                                        value={state.ihd} 
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={1} controlId="cva">
                                    <Form.Check
                                        type="checkbox"
                                        id="cva"
                                        label="CVA"
                                        disabled
                                        checked={state.cva}
                                        value={state.cva} 
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={1} controlId="crf">
                                    <Form.Check
                                        type="checkbox"
                                        id="crf"
                                        label="CRF"
                                        disabled
                                        checked={state.crf}
                                        value={state.crf} 
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={1} controlId="htn">
                                    <Form.Check
                                        type="checkbox"
                                        id="htn"
                                        label="HTN"
                                        disabled
                                        checked={state.htn}
                                        value={state.htn} 
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={2} controlId="asthma">
                                    <Form.Check
                                        type="checkbox"
                                        id="asthma"
                                        disabled
                                        label="Asthma/COPD"
                                        checked={state.asthma}
                                        value={state.asthma} 
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={2} controlId="epilepsy">
                                    <Form.Check
                                        type="checkbox"
                                        id="epilepsy"
                                        disabled
                                        label="Epilepsy"
                                        checked={state.epilepsy}
                                        value={state.epilepsy} 
                                    />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={9} controlId="significantHistory">
                                    <Form.Control as="textarea" rows="2" placeholder="Significant History" value={state.significantHistory}  disabled/>
                                    <Form.Label>Significant History</Form.Label>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={2} controlId="smoking" className="mb-0">
                                    <Form.Check
                                        type="checkbox"
                                        disabled
                                        id="smoking"
                                        label="Smoking"
                                        checked={state.smoking}
                                        value={state.smoking} 
                                    />
                                </Form.Group>
                                <Form.Group as={Col} xs={2} controlId="alcohol" className="mb-0">
                                    <Form.Check
                                        type="checkbox"
                                        id="alcohol"
                                        disabled
                                        label="Alcohol Use"
                                        checked={state.alcohol}
                                        value={state.alcohol} 
                                    />
                                </Form.Group>
                            </Form.Row>
                            <hr />
                            <span>Medication History</span>
                            <div className="contact d-flex row">
                                <div className="col-md-12">
                                    {medication.medications.map((contact, idx) => (
                                        <Form.Row className="medicationh">
                                            <React.Fragment key={idx}>
                                                <Form.Group as={Col} xs={3} controlId="name">
                                                    <Form.Control type="text" placeholder="Enter Name" value={contact.name} disabled/>
                                                    <Form.Label>Name </Form.Label>
                                                </Form.Group>
                                                <Form.Group as={Col} xs={2} controlId="dose">
                                                    <Form.Control type="text" placeholder="Enter Email" value={contact.dose}  disabled/>
                                                    <Form.Label>Dose</Form.Label>
                                                </Form.Group>
                                                <Form.Group as={Col} xs={2} controlId="route">
                                                    <Form.Control type="text" placeholder="Route" value={contact.route}  disabled/>
                                                    <Form.Label>Route</Form.Label>
                                                </Form.Group>
                                                <Form.Group as={Col} xs={2} controlId="frq">
                                                    <Form.Control type="text" placeholder="Frequency" value={contact.frq}  disabled/>
                                                    <Form.Label>Frequency</Form.Label>
                                                </Form.Group>
                                                <Form.Group as={Col} xs={2} controlId="isrecncl">
                                                    <Form.Check
                                                        type="checkbox"
                                                        id="isrecncl"
                                                        disabled
                                                        label="Reconciliation"
                                                        checked={contact.isrecncl}
                                                        value={contact.isrecncl}
                                                        
                                                    />
                                                </Form.Group>
                                            </React.Fragment>
                                        </Form.Row>
                                    ))}
                                </div>
                            </div>
                            <hr />
                            <Form.Row>
                                <Form.Group as={Col} xs={2} controlId="st">
                                    <Form.Check
                                        type="checkbox"
                                        id="food"
                                        disabled
                                        label="Food Reaction"
                                        checked={state.foodReactions}
                                        value={state.foodReactions} 
                                    />
                                </Form.Group>
                                {foodEnable &&
                                    <Form.Group as={Col} xs={9} controlId="foodDesc">
                                        <Form.Control as="textarea" rows="2" placeholder="Food" value={state.foodDesc}  disabled/>
                                        <Form.Label>Food</Form.Label>
                                    </Form.Group>
                                }
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={2} controlId="st">
                                    <Form.Check
                                        type="checkbox"
                                        id="drug"
                                        label="Drug Reaction"
                                        disabled
                                        checked={state.drugReactions}
                                        value={state.drugReactions} 
                                    />
                                </Form.Group>
                                {drugEnable &&
                                    <Form.Group as={Col} xs={9} controlId="drugDesc">
                                        <Form.Control as="textarea" rows="2" placeholder="Drug" value={state.drugDesc}  disabled/>
                                        <Form.Label>Drug</Form.Label>
                                    </Form.Group>
                                }
                            </Form.Row>

                            <div><span>Vital Data</span></div>
                            <Form.Row className="medicationh">
                                <Form.Group as={Col} xs={2} controlId="pulse">
                                    <Form.Control type="text" placeholder="Enter Address" value={state.pulse}  disabled/>
                                    <Form.Label>Pulse /min </Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={2} controlId="bp">
                                    <Form.Control type="text" placeholder="Enter Address" value={state.bp} disabled />
                                    <Form.Label>BP mmHg </Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={2} controlId="resp">
                                    <Form.Control type="text" placeholder="Enter Address" value={state.resp}  disabled/>
                                    <Form.Label>Resp /min </Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={2} controlId="temp">
                                    <Form.Control type="text" placeholder="Enter Address" value={state.temp}  disabled/>
                                    <Form.Label>Temp F</Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={2} controlId="spo">
                                    <Form.Control type="text" placeholder="Enter Address" value={state.spo}  disabled/>
                                    <Form.Label>SPO2 </Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={2} controlId="wt">
                                    <Form.Control type="text" placeholder="Enter Address" value={state.wt}  disabled/>
                                    <Form.Label>WT Kgs </Form.Label>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row className="mt-4">
                                <Form.Group as={Col} xs={4} controlId="respiratory">
                                    <Form.Control type="text" placeholder="Enter Address" value={state.respiratory}  disabled/>
                                    <Form.Label>Respiratory </Form.Label>
                                </Form.Group>

                                <Form.Group as={Col} xs={4} controlId="abdominal">
                                    <Form.Control type="text" placeholder="Enter Address" value={state.abdominal} disabled />
                                    <Form.Label>Abdominal </Form.Label>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={4} controlId="neurology">
                                    <Form.Control type="text" placeholder="Enter Address" value={state.neurology}  disabled/>
                                    <Form.Label>Neurology </Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} xs={4} controlId="musculoskeletal">
                                    <Form.Control type="text" placeholder="Enter Address" value={state.musculoskeletal}  disabled/>
                                    <Form.Label>Musculoskeletal </Form.Label>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={9} controlId="otherSystems">
                                    <Form.Control as="textarea" rows="1" placeholder="Other Systems" value={state.otherSystems}  disabled/>
                                    <Form.Label>Other Systems</Form.Label>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={9} controlId="provisionalDiagnosis">
                                    <Form.Control as="textarea" rows="4" placeholder="Provisional Diagnosis" value={state.provisionalDiagnosis}  disabled/>
                                    <Form.Label>Provisional Diagnosis</Form.Label>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} xs={9} controlId="planOfCare">
                                    <Form.Control as="textarea" rows="6" placeholder="Plan of care" value={state.planOfCare}  disabled/>
                                    <Form.Label>Plan of care</Form.Label>
                                </Form.Group>
                            </Form.Row>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>Close</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default OpdHistory