import React from 'react';
import PatientInfoService from '../../../../services/administration/PatientInfoService';

/**
 * @description: CRUD operations for Patient
 * @author: Lakshmi
 * @param {*}
 * @function Branch
 * @date : 13-08-2020
 */



// ========================================================================
//  Branch Create Operation
//  This will create Branch and save the details 
// ========================================================================
export const retrieveDrug = async (obj) => {
    let finalRes=[];
    await PatientInfoService.getDrugData(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

export const saveDrugData = async (obj) => {
    let finalRes=[];
    await PatientInfoService.saveDrug(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

export const dischargePatient = async (obj) => {
    let finalRes=[];
    await PatientInfoService.dischargePatient(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}


