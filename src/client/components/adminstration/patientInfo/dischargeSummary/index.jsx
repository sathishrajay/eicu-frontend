import React, { useContext, useEffect, useReducer, useRef, useState } from 'react';
import { Alert, Col, Form, Modal } from "react-bootstrap";
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { useHistory } from 'react-router-dom';
import { Context, LocationContext } from "../../../../../App";
import Button from '../../../common/buttons/Button';
import Icon from '../../../common/icomoonIcon/Icon';
import Footer from '../../../common/Footer';
import Header from '../../../common/Header';
import MainMenu from '../../../common/MainMenu';
import {retrieveDrug , saveDrugData , dischargePatient} from './operations';
import moment from 'moment';
import DateUtils from '../../../commonUtilities/DateUtils';
/**
 * @description:This function will creaet Hospitales and this will provide the ADD,EDIT and Remove functionalities
 * @author: Lakshmi
 * @param {*}
 * @function Hospital
 * @date : 13-08-2020
 */

/**
 * Initial State Declaration
 */
const initialState = {
   discSummary:""
};
//=====================================
// Reducer function for userReact Hook
// which will update the patientData
//=====================================
const reducer = (patientData, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...patientData
            };
        case 'editHospital':
            return Object.assign(patientData, action.payload);
        default:
            return { ...patientData, [action.type]: action.payload };
    }
};

function DischargeSummary(props) {
    const [context, setContext] = useContext(Context);
    const [state, dispatch] = useReducer(reducer, initialState);
    const history = useHistory();
    const [notiMessage, setNotiMessage] = useState('');
    const [notiMessageShow, setNotiMessageShow] = useState(false);
    const [notiVarient, setNotiVarient] = useState();
    const [patientData,setPatientData] = useState();
    const [showPopup, setShowPopup] = useState(false);
    const [fromDate, setFromDate] = useState(new Date());
    const [drugs , setDrugData]= useState({drugdata:[]})
    const [patientId,setPatientId] = useState();
    const [drugdate, setDate] = useState(new Date());
    const [hinfo , setHinfo] = useState()

    useEffect(() => {
        let nowDate =  moment(new Date()).format('YYYY-MM-DD HH:mm');
        setDrugData({drugdata: [{drugName:"",prescBy:context.logindata.firstName+" "+context.logindata.lastName,dateTime:nowDate, isStop:false,dose:"",route :"",freq:"",stoppedBy:"", stopDate:nowDate,stat:false}]});
      if(props.location!==undefined && props.location.state!==undefined && props.location.state.data!==undefined){
        setPatientData(props.location.state.data);
        const payload={"patientId":props.location.state.data._id}
        setPatientId(props.location.state.data._id)
        setHinfo(props.location.state.hinfo)
        retrieveDrug(payload).then((response) => {
              if (response.suc){
                setValuesToDrug(response.res);
              }  
          });
      }
    }, []);

    const setValuesToDrug =(drugData) => {
        let dataArr=[];
        drugData.map((itm, index) => {
            let data = {
                _id:itm._id,
                drugName: itm.drugName,
                dateTime:itm.dateTime,
                prescBy: itm.prescBy,
                isStop: itm.isStop,
                dose:  itm.dose,
                route: itm.route,
                freq: itm.freq,
                stopDate: itm.stopDate,
                stoppedBy:itm.stoppedBy,
                stat:itm.stat,
                infusion:itm.infusion
            }
            dataArr.push(data);
          })
          setDrugData({drugdata:dataArr});
      }

       // OnChange event we will call this and update the state
    const handleChange = (e) => {
        const { id, value } = e.target;
         dispatch({ type: id, payload: value });      
    }

    const handleChangeDrug = idx => evt => {
        try {
          let newDrugs = drugs.drugdata.map((obj, sidx) => {
            if (idx !== sidx) return obj;
            else {
            return { ...obj, [evt.target.id]: evt.target.value };
            }
          });
          setDrugData({ drugdata: newDrugs })
        } catch (err) {
         // log.error('Exception occured in CompanyComponent function---' + err);
        }
      }
  

    const dischargePatientPopup = () => {
        setShowPopup(true);
    }

    const dischargePatientFn = () => {
        const payload={
           "_id":"",
           "usrid":context.logindata._id,
           "patientId":patientId,
           "drugdata":drugs.drugdata
        }
        console.log("Drugs request :"+JSON.stringify(payload));
        saveDrugData(payload).then((resp) => {
            if (resp.suc && hinfo!==undefined){
                let req = {
                    hspCode : hinfo.hcode,
                    bed_Num  : hinfo.bed_id,
                    hid : hinfo.hospital_id,
                    patient_id: patientId,
                    dischargedesc:state.discSummary
                }
                console.log("Discharge request :"+JSON.stringify(req))
                dischargePatient(req).then((resp) => {
                    if (resp.suc){
                        setNotiMessageShow(true);
                        setNotiMessage("Patient discharged successfully");
                        setNotiVarient('success')
                        hidemessage();
                        return window.location = "/eicu/admin/location/"+hinfo.hcode+"/"+hinfo.bed_id;
                    }else{
                        setNotiMessageShow(true);
                        setNotiMessage("Unable to discharge the patient");
                        setNotiVarient('danger')
                    }
                });
            }
        });
    }
    
    const cancelDischarge=()=>{
        return window.location = "/eicu/admin/location/"+hinfo.hcode+"/"+hinfo.bed_id;
    }

    const hidemessage=() =>{
        setTimeout(function () {
            setNotiMessageShow(false);
            setNotiMessage('');
            setNotiVarient('');
        }, 5000);
      }

      // OnChange event we will call this and update the state
  const handleDateChange = idx => date => {
    const convertDate = DateUtils.convertStringToDate(date);
    let newData = drugs.drugdata.map((obj, sidx) => {
      if (idx !== sidx) return obj;
      return {
        ...obj, dateTime: convertDate
      };
    });
    setDrugData({ drugdata:newData})
  }

    return (
        <>    <Header />
              <MainMenu active='Location' />
            <div className="container">
            <br/>
            <span>Discharge Summary :  <b>{patientData!==undefined && patientData.name} ({patientData!==undefined && patientData.mrNum})</b></span>
            <hr />
            <Alert className="notification" variant={notiVarient} show={notiMessageShow}
                onClose={() => setNotiMessageShow(false)}
                dismissible> {notiMessage}</Alert> 
                   {patientData!==undefined &&
                    <div className="cardMainlayout pt-0 mt-0">
                    <div className="contentArea">
                        <Form.Row>
                            <Form.Group as={Col} xs={3} controlId="name">
                                <Form.Control type="text" disabled="true" placeholder="Enter Name" autoComplete="off" value={patientData.name} onChange={handleChange} />
                                <Form.Label>Name <sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="age">
                                <Form.Control type="text" disabled="true" placeholder="Enter Age" autoComplete="off" value={patientData.age} onChange={handleChange} />
                                <Form.Label>Age <sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="sex">
                                <Form.Control as="select" disabled="true" value={patientData.sex} onChange={handleChange}>
                                    <option value="">Select</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </Form.Control>
                                <Form.Label>Sex <sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="phoneNum">
                                <Form.Control type="text" disabled="true" placeholder="Contact Number" value={patientData.phoneNum} onChange={handleChange} />
                                <Form.Label>Contact Number <sup>*</sup></Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={3} controlId="mrNum">
                                <Form.Control type="text" disabled="true" placeholder="Enter MR" disabled="true" value={patientData.mrNum} onChange={handleChange} />
                                <Form.Label>MR Number <sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} className="selectplugin datePicker" controlId="dtOfAdmission">
                            <DatePicker
                            selected={fromDate}
                            value={patientData.dtOfAdmission}
                            //onChange={date => setFromDate(date)}
                            fromDate={fromDate}
                            disabled="true"
                            minDate={new Date()}
							/>
                            <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                            <Form.Label>Date of Admission</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} className="selectplugin datePicker" controlId="dtOfAdmission">
                            <DatePicker
                            disabled="true"
                            selected={new Date()}
                            value={new Date()}
                            //onChange={date => setFromDate(date)}
                            fromDate={new Date()}
                            minDate={new Date()}
							/>
                            <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                            <Form.Label>Date of Discharge</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={9} controlId="address">
                                <Form.Control as="textarea" disabled="true" rows="2" placeholder="Address" value={patientData.address} onChange={handleChange} />
                                <Form.Label>Address<sup>*</sup></Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={9} controlId="sourOfHistory">
                                <Form.Control as="textarea"  disabled="true" rows="2" placeholder="Sourece of History" value={patientData.sourOfHistory} onChange={handleChange} />
                                <Form.Label>Source of History</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={9} controlId="prestComplaints">
                                <Form.Control as="textarea" disabled="true" rows="2" placeholder="Preseinting Complaints" value={patientData.prestComplaints} onChange={handleChange} />
                                <Form.Label>Presenting Complaints</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={9} controlId="histyOfPrestIllness">
                                <Form.Control as="textarea" disabled="true" rows="2" placeholder="Hoistory of Present Illness" value={patientData.histyOfPrestIllness} onChange={handleChange} />
                                <Form.Label>History of Present Illness</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={9} controlId="medical">
                                <Form.Control as="textarea" disabled="true" rows="2" placeholder="Medical" value={patientData.medical} onChange={handleChange} />
                                <Form.Label>Medical</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                        <Form.Group as={Col}  xs={1} controlId="dm">
                                <Form.Check
                                    type="checkbox"
                                    id="dm"
                                    label="DM"
                                    checked={patientData.dm}
                                    disabled="true"
                                    value={patientData.dm} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={1} controlId="ihd">
                                <Form.Check
                                    type="checkbox"
                                    id="ihd"
                                    label="IHD"
                                    checked={patientData.ihd}
                                    disabled="true"
                                    value={patientData.ihd} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={1} controlId="cva">
                                <Form.Check
                                    type="checkbox"
                                    id="cva"
                                    label="CVA"
                                    checked={patientData.cva}
                                    disabled="true"
                                    value={patientData.cva} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={1} controlId="crf">
                                <Form.Check
                                    type="checkbox"
                                    id="crf"
                                    label="CRF"
                                    checked={patientData.crf}
                                    disabled="true"
                                    value={patientData.crf} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={1} controlId="htn">
                                <Form.Check
                                    type="checkbox"
                                    id="htn"
                                    label="HTN"
                                    checked={patientData.htn}
                                    disabled="true"
                                    value={patientData.htn} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={2} controlId="asthma">
                                <Form.Check
                                    type="checkbox"
                                    id="asthma"
                                    label="Asthma/COPD"
                                    checked={patientData.asthma}
                                    disabled="true"
                                    value={patientData.asthma} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={2} controlId="epilepsy">
                                <Form.Check
                                    type="checkbox"
                                    id="epilepsy"
                                    label="Epilepsy"
                                    disabled="true"
                                    checked={patientData.epilepsy}
                                    value={patientData.epilepsy} onChange={handleChange}
                                />
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                        <Form.Group as={Col} xs={9} controlId="significantHistory">
                                <Form.Control as="textarea" disabled="true" rows="2" placeholder="Significant History" value={patientData.significantHistory} onChange={handleChange} />
                                <Form.Label>Significant History</Form.Label>
                            </Form.Group>
                            </Form.Row>
                            <Form.Row>
                        <Form.Group as={Col}  xs={2} controlId="smoking" className="mb-0">
                                <Form.Check
                                    type="checkbox"
                                    id="smoking"
                                    label="Smoking"
                                    checked={patientData.smoking}
                                    disabled="true"
                                    value={patientData.smoking} onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Group as={Col}  xs={2} controlId="alcohol" className="mb-0">
                                <Form.Check
                                    type="checkbox"
                                    id="alcohol"
                                    label="Alcohol Use"
                                    checked={patientData.alcohol}
                                    disabled="true"
                                    value={patientData.alcohol} onChange={handleChange}
                                />
                            </Form.Group>
                            </Form.Row>
                            <hr />
        {patientData.medicalHistory.length!==0 &&
        <>
        <span>Medication History</span>
        <div className="contact d-flex row">
          <div className="col-md-12">
              {patientData.medicalHistory.map((contact, idx) => (
              <Form.Row className="medicationh">
                <React.Fragment key={idx}>
                  <Form.Group as={Col} xs={3} controlId="name">
                    <Form.Control type="text" disabled="true" placeholder="Enter Name" value={contact.name} />
                    <Form.Label>Name </Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={2} controlId="dose">
                    <Form.Control type="text" disabled="true" placeholder="Enter Email" value={contact.dose}  />
                    <Form.Label>Dose</Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={2} controlId="route">
                    <Form.Control type="text" disabled="true" placeholder="Route" value={contact.route} />
                    <Form.Label>Route</Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={2} controlId="frq">
                    <Form.Control type="text" disabled="true" placeholder="Frequency" value={contact.frq}  />
                    <Form.Label>Frequency</Form.Label>
                  </Form.Group>
                  <Form.Group as={Col}  xs={2} controlId="isrecncl">
                   <Form.Check
                    type="checkbox"
                    id="isrecncl"
                    label="Reconciliation"
                    checked={contact.isrecncl}
                    disabled="true"
                    value={contact.isrecncl}
                    //onChange={handleMedicationChange(idx)}
                    />
                  </Form.Group>
                </React.Fragment>
              </Form.Row>
            ))}
            
            </div>
           </div>
           </>
                   }
           <hr />
                            <Form.Row>
                        <Form.Group as={Col}  xs={2} controlId="st">
                                <Form.Check
                                    type="checkbox"
                                    id="food"
                                    label="Food Reaction"
                                    checked={patientData.foodReactions}
                                    disabled="true"
                                    value={patientData.foodReactions} onChange={handleChange}
                                />
                            </Form.Group>
                            {patientData.foodReactions &&
                            <Form.Group as={Col} xs={9} controlId="foodDesc">
                                <Form.Control as="textarea" rows="2" disabled="true" placeholder="Food" value={patientData.foodDesc} onChange={handleChange} />
                                <Form.Label>Food</Form.Label>
                            </Form.Group>
                            }
                            </Form.Row>
                            <Form.Row>
                            <Form.Group as={Col}  xs={2} controlId="st">
                                <Form.Check
                                    type="checkbox"
                                    id="drug"
                                    disabled="true"
                                    label="Drug Reaction"
                                    checked={patientData.drugReactions}
                                    value={patientData.drugReactions} onChange={handleChange}
                                />
                            </Form.Group>
                            {patientData.drugReactions &&
                            <Form.Group as={Col} xs={9} controlId="drugDesc">
                                <Form.Control as="textarea" disabled="true" rows="2" placeholder="Drug" value={patientData.drugDesc} onChange={handleChange} />
                                <Form.Label>Drug</Form.Label>
                            </Form.Group>
                             }
                        </Form.Row>
                        
                        <div><span>Vital Data</span></div>
                        <Form.Row className="medicationh">
                          <Form.Group as={Col} xs={2} controlId="pulse">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.pulse} onChange={handleChange} />
                                <Form.Label>Pulse /min </Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={2} controlId="bp">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.bp} onChange={handleChange} />
                                <Form.Label>BP mmHg </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="resp">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.resp} onChange={handleChange} />
                                <Form.Label>Resp /min </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="temp">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.temp} onChange={handleChange} />
                                <Form.Label>Temp F</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="spo">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.spo} onChange={handleChange} />
                                <Form.Label>SPO2 </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="wt">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.wt} onChange={handleChange} />
                                <Form.Label>WT Kgs </Form.Label>
                            </Form.Group>
                         </Form.Row>
                         <Form.Row className="mt-4">
                          <Form.Group as={Col} xs={4} controlId="respiratory">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.respiratory} onChange={handleChange} />
                                <Form.Label>Respiratory </Form.Label>
                            </Form.Group>

                            <Form.Group as={Col} xs={4} controlId="abdominal">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.abdominal} onChange={handleChange} />
                                <Form.Label>Abdominal </Form.Label>
                            </Form.Group>
                            </Form.Row>
                          <Form.Row>
                            <Form.Group as={Col} xs={4} controlId="neurology">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.neurology} onChange={handleChange} />
                                <Form.Label>Neurology </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={4} controlId="musculoskeletal">
                                <Form.Control type="text" disabled="true" placeholder="Enter Address" value={patientData.musculoskeletal} onChange={handleChange} />
                                <Form.Label>Musculoskeletal </Form.Label>
                            </Form.Group>
                            </Form.Row>
                             <Form.Row>
                             <Form.Group as={Col} xs={9} controlId="otherSystems">
                                <Form.Control as="textarea" disabled="true" rows="1" placeholder="Other Systems" value={patientData.otherSystems} onChange={handleChange} />
                                <Form.Label>Other Systems</Form.Label>
                            </Form.Group>
                             </Form.Row>
                             <Form.Row>
                             <Form.Group as={Col} xs={9} controlId="provisionalDiagnosis">
                                <Form.Control as="textarea" disabled="true" rows="4" placeholder="Provisional Diagnosis" value={patientData.provisionalDiagnosis} onChange={handleChange} />
                                <Form.Label>Provisional Diagnosis</Form.Label>
                            </Form.Group>
                             </Form.Row>
            <span>Discharge Advise</span>
            <hr />
            <div className="cardMainlayout pt-0 min-height">
            <div className="container drugchart mt-3">
            {drugs.drugdata && drugs.drugdata.map((drug, idx) => (
            <>
            {!drug.isStop &&
             <>
              <Form.Row className="divider"
              className={"divider " + (drug.isStop ? ' in-active' : '')}>
                <div className="col-sm-12">
                {!drug.infusion && !drug.stat &&
                <h6 className="mb-3 text-primary">Drug</h6>
                }
                {drug.infusion &&
                <h6 className="mb-3 text-primary">Drug Infusions</h6>
                }
                {drug.stat &&
                <h6 className="mb-3 text-primary">STAT Drug </h6>
                }
               </div>
                <React.Fragment key={idx}>
                  <Form.Group as={Col} xs={3} controlId="drugName">
                    <Form.Control type="text" placeholder="Enter Name" disabled={drug.isStop} value={drug.drugName} onChange={handleChangeDrug(idx)} />
                    <Form.Label>Drug Name <sup>*</sup></Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={3} controlId="prescBy">
                    <Form.Control type="text" disabled="true" placeholder="Enter Email" value={drug.prescBy} onChange={handleChangeDrug(idx)} />
                    <Form.Label>Prescribed By<sup>*</sup></Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={2} className="selectplugin datePicker" controlId="dob">
                            <DatePicker
                            selected={drugdate}
                            onChange={handleDateChange(idx)}
                            value={drug.dateTime}
                            minDate={new Date()}
                            dateFormat="dd-MMM-YYY HH:mm"
							 />
                            <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                            {!drug.stat &&
                            <Form.Label>Start Date & Time <sup>*</sup></Form.Label>
                            }
                            {drug.stat &&
                            <Form.Label>Date & Time <sup>*</sup></Form.Label>
                            }
                            </Form.Group>  
                  <Form.Row>
                  <Form.Group as={Col} xs={2} controlId="dose">
                    <Form.Control type="text" placeholder="Dose" disabled={drug.isStop} value={drug.dose}  onChange={handleChangeDrug(idx)} />
                    <Form.Label>Dose</Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={2} controlId="route">
                    <Form.Control type="text" placeholder="Route"  disabled={drug.isStop} value={drug.route} onChange={handleChangeDrug(idx)} />
                    <Form.Label>Route</Form.Label>
                  </Form.Group>
                  {!drug.infusion && !drug.stat &&
                  <Form.Group as={Col} xs={2} controlId="freq">
                    <Form.Control type="text" placeholder="Frequency"  disabled={drug.isStop} value={drug.freq} onChange={handleChangeDrug(idx)} />
                    <Form.Label>Frequency</Form.Label>
                  </Form.Group>
                  } 
                  {drug.infusion && !drug.stat &&
                  <Form.Group as={Col} xs={2} controlId="freq">
                    <Form.Control type="text" placeholder="Frequency"  disabled={true} value={drug.freq} onChange={handleChangeDrug(idx)} />
                    <Form.Label>Frequency</Form.Label>
                  </Form.Group>
                  }
                  {drug.isStop && !drug.stat &&
                  <Form.Group as={Col} xs={3} controlId="stoppedBy">
                    <Form.Control type="text" disabled="true" placeholder="Enter Email" value={drug.stoppedBy} />
                    <Form.Label>Stopped By<sup>*</sup></Form.Label>
                  </Form.Group>
                  }
                   {!drug.stat &&
                  <Form.Group as={Col} xs={3} className="selectplugin datePicker" controlId="dob">
                            <DatePicker
                            //selected={drugdate}
                            value={drug.stopDate}
                            minDate={new Date()}
                            dateFormat="dd-MMM-YYY HH:mm"
                            disabled={true}
						 />
                            <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                            <Form.Label>Stop Date & Time</Form.Label>
                            </Form.Group>
                      }
                    </Form.Row>                 
                </React.Fragment>
              </Form.Row>
              </>
            }
            </>
            ))}
            </div></div>
                             <Form.Row>
                             <Form.Group as={Col} xs={9} controlId="discSummary">
                                <Form.Control as="textarea" rows="6" placeholder="Plan of care" value={state.discSummary} onChange={handleChange} />
                                <Form.Label>Summary</Form.Label>
                            </Form.Group>
                             </Form.Row>
                             <div class="row">
                             <div className="text-left buttonGrop pb-0 pl-1 col-sm-6">
                                      <Button
                                            size="xs"
                                            variant="outline-primary"
                                            onClick={cancelDischarge}
                                            loading={patientData.loadOfice}
                                            type="submit"
                                        >Cancel
                                        </Button>
                                        
                                        <Button
                                            size="xs"
                                            variant="outline-primary"
                                            onClick={dischargePatientPopup}
                                            loading={patientData.loadOfice}
                                            type="submit"
                                        >Submit Discharge 
                                        </Button>
                             </div>
                             </div>

                        {showPopup &&
                                <Modal
                                    show={showPopup}
                                    onHide={() => setShowPopup(false)}
                                    backdrop="static"
                                    size="small"
                                    className="importPNRMd1"
                                    keyboard={false}>
                                        <Modal.Header>
                                            <Modal.Title>Discharge Patient</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                        <div className="icons warning"></div>
                                                <div>
                                                    <Form.Label className="custom-dailog-sub-title">Patient Name : <b>{patientData.name}</b>  <span class="ml-4"> MR Number : <b>{patientData.mrNum}</b></span></Form.Label>
                                                    <Form.Label className="custom-dailog-title">Are you sure you want to discharge this patient ?</Form.Label>
                                                </div>
                                                <div className="text-center buttonGrop">
                                                    <Button 
                                                        size="xs"
                                                        className="mr-3"
                                                        variant="outline-primary"
                                                        onClick={dischargePatientFn}
                                                        type="submit"> Yes </Button>
                                                    <Button
                                                        size="xs"
                                                        variant="outline-dark"
                                                        onClick={() => setShowPopup(false)}
                                                        type="submit"> No </Button>
                                                </div>
                                            
                                         </Modal.Body>
                                </Modal>
                            }
                    </div>
                </div>
                }
            </div>
            <Footer />
        </>
    )
}
export default DischargeSummary