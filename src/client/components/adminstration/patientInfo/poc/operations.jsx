import React from 'react';
import PatientInfoService from '../../../../services/administration/PatientInfoService';

/**
 * @description: CRUD operations for Patient
 * @author: Lakshmi
 * @param {*}
 * @function Drug Chart
 * @date : 13-08-2020
 */



// ========================================================================
//  Branch Create Operation
//  This will create Branch and save the details 
// ========================================================================
export const saveSpocData = async (obj) => {
    let finalRes=[];
    await PatientInfoService.saveSpoc(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

export const retrieveSpoc = async (obj) => {
    let finalRes=[];
    await PatientInfoService.getSpocData(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

