import React, { useCallback, useEffect, useReducer , useContext} from 'react';
import { useState } from 'react';
import { Col, Form, Table, Alert } from "react-bootstrap";
import { useHistory } from 'react-router-dom';
import Button from '../../../common/buttons/Button';
import "react-datepicker/dist/react-datepicker.css";
import {saveSpocData,retrieveSpoc} from './operations';
import { Context } from "../../../../../App";
import DateUtils from '../../../commonUtilities/DateUtils';

/**
 * @description:This function will creaet Hospitales and this will provide the ADD,EDIT and Remove functionalities
 * @author: Lakshmi
 * @param {*}
 * @function Hospital
 * @date : 13-08-2020
 */

/**
 * Initial State Declaration
 */
const initialState = {
    _id:"",spoc:""
  
};
//=====================================
// Reducer function for userReact Hook
// which will update the state
//=====================================
const reducer = (state, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...state
            };
        case 'editHospital':
            return Object.assign(state, action.payload);
        default:
            return { ...state, [action.type]: action.payload };
    }
};

function SPOC(props) {
    const [state, dispatch] = useReducer(reducer, initialState);
    const history = useHistory();
    const [spocsData, setSpocsData] =useState({spocList:[]});
    const [notiMessage, setNotiMessage] = useState('');
    const [notiMessageShow, setNotiMessageShow] = useState(false);
    const [notiVarient, setNotiVarient] = useState();
    const [patientid, setPatientId] = useState()
    const [userId , setUserId] = useState()
    const [context, setContext] = useContext(Context);
    const [data, setData] = useState()

    useEffect(() => {
        window.scrollTo(0, 0);
        if(props.showMsg){
          setNotiMessageShow(true);
          setNotiMessage('Lab data Updated Successfully..');
          setNotiVarient('success');
          hidemessage();
        }
      }, [props.showMsg]);

    useEffect(() => {
        if(props.patientData!==undefined){
        setPatientId(props.patientData.patientId)
        setUserId(context.logindata._id);
        setData(props.patientData);
        //Load all Spocs
        retrieveSpoc(props.patientData.patientId).then((response) => {
            if (response.suc && response.res!==undefined){
               setSpocsData({spocList:response.res});
            }  
        });
        }
      window.scrollTo(0, 0);
    }, [props.patientData]);

    const handleChange = (e) => {
        const { id, value } = e.target;
        dispatch({ type: id, payload: value });
    }
   //Save SPOC Remarks
    const saveSpoc=()=>{
        if(state.spoc!==undefined && state.spoc!==""){
        const req={
            "spoc":state.spoc,
            "patientId":props.patientData.patientId,
            "usrid":userId
            }
        saveSpocData(req).then((resp) => {
            if (resp.suc){
                    setNotiMessageShow(true);
                    setNotiMessage("Spoc created Successfully");
                    setNotiVarient('danger')
                    hidemessage();
             //Load all Spocs
              retrieveSpoc(props.patientData.patientId).then((response) => {
                 if (response.suc && response.res!==undefined){
                    setSpocsData({spocList:response.res});
                 }  
             });
             dispatch({ type: "spoc", payload: "" })
             props.sendSpoc("Spoc")
            }else{
                setNotiMessageShow(true);
                setNotiMessage("Spoc creation failed");
                setNotiVarient('danger')
                hidemessage();
            }
        });
    }else{
        setNotiMessageShow(true);
        setNotiMessage("Please enter Spoc description");
        setNotiVarient('danger')
        hidemessage();
    }
    }
    const hidemessage=() =>{
        setTimeout(function () {
            setNotiMessageShow(false);
            setNotiMessage('');
            setNotiVarient('');
        }, 5000);
      }
  
    return (
        <>
        <Alert className="notification" variant={notiVarient} show={notiMessageShow}
                onClose={() => setNotiMessageShow(false)}
                dismissible> {notiMessage}</Alert> 
        {data!==undefined &&
            <div className="row">
                <div className="col-sm-6">Patient Name : <b>{data.name}</b></div>
                <div className="col-sm-6 text-right">MR Number : <b>{data.mrNum}</b></div>
            </div>
        }
        <hr/>
           <Form.Row>
            <Form.Group as={Col} xs={12} controlId="spoc">
            <Form.Control as="textarea" rows="4" placeholder="Enter spoc" value={state.spoc} onChange={handleChange} />
            <Form.Label>SPOC</Form.Label>
            </Form.Group>
            </Form.Row>
             {context.logindata.permissions!==undefined && context.logindata.permissions.includes("ASPOC") &&
             <div className="text-right buttonGrop pb-0">
               <Button
                  size="xs"
                  variant="outline-primary"
                  onClick={saveSpoc}
                  type="submit"
                  loading={state.loadOfice}
              > Save</Button>
             </div>
            }
             <hr/>
             <div className="container drugchart">
              {spocsData.spocList.length!==0 && spocsData.spocList.map((obj,idx) => (
             <React.Fragment key={idx}>
                   <Form.Row>
                   <Form.Group as={Col} xs={12} controlId="drugDesc" className="mb-1 mt-3">
                    <Form.Control as="textarea" rows="2" disabled="true" placeholder="Drug" value={obj.spoc} />
                    <Form.Label>SPOC</Form.Label>
                    </Form.Group>
                   </Form.Row>
                   <Form.Row>
                   <div className="col-sm-6">Date & Time : <b>{obj.createdDate}</b></div>
                   <div className="col-sm-6">Created By :  <b>{obj.createdBy}</b></div>
                   </Form.Row>
                   </React.Fragment>
                ))}
             </div>
           
        </>
    )
}
export default SPOC