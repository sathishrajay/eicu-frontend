import React, { useCallback, useEffect, useReducer , useContext} from 'react';
import { useState } from 'react';
import { Col, Form, Table, Alert } from "react-bootstrap";
import { useHistory } from 'react-router-dom';
import Button from '../../../common/buttons/Button';
import Icon from '../../../common/icomoonIcon/Icon';
import {saveDrugData,retrieveDrug,stopDrug} from './operations';
import DatePicker from 'react-datepicker';
import { Context } from "../../../../../App";
import Notification from '../../../common/notification/Notification';
import DateUtils from '../../../commonUtilities/DateUtils';
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';



/**
 * @description:This function will creaet Hospitales and this will provide the ADD,EDIT and Remove functionalities
 * @author: Lakshmi
 * @param {*}
 * @function Hospital
 * @date : 13-08-2020
 */

/**
 * Initial State Declaration
 */
const initialState = {
    _id:""
  
};
//=====================================
// Reducer function for userReact Hook
// which will update the state
//=====================================
const reducer = (state, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...state
            };
        case 'editHospital':
            return Object.assign(state, action.payload);
        default:
            return { ...state, [action.type]: action.payload };
    }
};

function DrugChart(props) {
    const [state, dispatch] = useReducer(reducer, initialState);
    const history = useHistory();
    const [notiMessage, setNotiMessage] = useState('');
    const [notiMessageShow, setNotiMessageShow] = useState(false);
    const [notiVarient, setNotiVarient] = useState();
    const [drugdate, setDate] = useState(new Date());
    const [drugs , setDrugData]= useState({drugdata:[]})
    const [context, setContext] = useContext(Context);
    const [data,setData] =useState()
    const [patientId, setPatientId] =useState();
    const [disableDrug, setDisableDrug] =useState(false);
    const [enableBtn, setEnableBtn] =useState(false);
    const [userId , setUserId] = useState()
    const [drugsTemp , setDrugDataTemp]= useState([])
    const [disableFrq, setDisabelFrq] =useState(false);
      
    useEffect(() => {
      let nowDate =  moment(new Date()).format('YYYY-MM-DD HH:mm');
      setDrugData({drugdata: [{drugName:"",prescBy:context.logindata.firstName+" "+context.logindata.lastName,dateTime:nowDate, isStop:false,dose:"",route :"",freq:"",stoppedBy:"", stopDate:nowDate,stat:false}]});
      window.scrollTo(0, 0);
    }, []);

    useEffect(() => {
      window.scrollTo(0, 0);
      if(props.showMsg){
        setNotiMessageShow(true);
        setNotiMessage('SPOC Data Updated Successfully..');
        setNotiVarient('success');
        hidemessage();
      }
    }, [props.showMsg]);

       useEffect(() => {
        if(context.logindata!==undefined){
          setUserId(context.logindata._id);
        }
        }, [context.logindata]);

        const hidemessage=() =>{
          setTimeout(function () {
              setNotiMessageShow(false);
              setNotiMessage('');
              setNotiVarient('');
          }, 5000);
        }
      useEffect(() => {
        if(props.patientData!==undefined){
            setData(props.patientData);
            setPatientId(props.patientData.patientId);
            if(props.patientData.patientId!==undefined && props.patientData.patientId!==null){
                const payload={"patientId":props.patientData.patientId}
                retrieveDrug(payload).then((response) => {
                    if (response.suc){
                      setEnableBtn(true);
                      setValuesToDrug(response.res);
                    }  
                });
            }
            let nowDate =  moment(new Date()).format('YYYY-MM-DD HH:mm');
          if(props.patientData.drugsList!==undefined && props.patientData.drugsList!==0){
            let finalList=[];
            for(let val of props.patientData.drugsList){
               const payload={
                drugName:val.name,
                prescBy:context.logindata.firstName+" "+context.logindata.lastName,
                dateTime:nowDate,
                isStop:false,
                dose:val.dose,
                route :val.route,
                freq:val.frq,
                stoppedBy:"", 
                stopDate:nowDate
               }
               finalList.push(payload);
               setEnableBtn(true);
            }
            setDrugData({drugdata:finalList});
          }
        }
      }, [props.patientData]);

      const setValuesToDrug =(drugData) => {
        let dataArr=[];
        drugData.map((itm, index) => {
            let data = {
                _id:itm._id,
                drugName: itm.drugName,
                dateTime:itm.dateTime,
                prescBy: itm.prescBy,
                isStop: itm.isStop,
                dose:  itm.dose,
                route: itm.route,
                freq: itm.freq,
                stopDate: itm.stopDate,
                stoppedBy:itm.stoppedBy,
                stat:itm.stat,
                infusion:itm.infusion
            }
            dataArr.push(data);
          })
          setDrugData({drugdata:dataArr});
          setDrugDataTemp({drugsTemp:dataArr})
      }


    const stopActiveDrug = (id)  => () => {
        const payload={"_id":id,"usrid":userId}
        let drugList=[];
        drugList.push(drugs);
        stopDrug(payload).then((response) => {
                    if (response.suc && drugs.drugdata!==undefined){
                      const req={"patientId":props.patientData.patientId}
                      retrieveDrug(req).then((response) => {
                         if (response.suc && response.res!==undefined){
                           setValuesToDrug(response.res);
                           setEnableBtn(true);
                         }  
                     });
                      setDisableDrug(true);
                      setNotiMessageShow(true);
                      setNotiMessage('Drug Stoped Successfully..');
                      setNotiVarient('success');
                      window.scrollTo(0, 0);
                      hidemessage();
                    }else{
                      setNotiMessageShow(true);
                      setNotiMessage('Drug Stoped Failed..');
                      setNotiVarient('danger');
                      window.scrollTo(0, 0);
                      hidemessage();
                    }
                });
      }
    

    const handleChange = idx => evt => {
      try {
        let newDrugs = drugs.drugdata.map((obj, sidx) => {
          if (idx !== sidx) return obj;
          else {
          return { ...obj, [evt.target.id]: evt.target.value };
          }
        });
        setDrugData({ drugdata: newDrugs })
      } catch (err) {
       // log.error('Exception occured in CompanyComponent function---' + err);
      }
    }

    const removeDrugData = idx => () => {
      setDrugData({
        drugdata:drugs.drugdata.filter((s, sidx) => idx !== sidx)
      });
    };

    const addDrugData = (num) =>() => {
      let nowDate =  moment(new Date()).format('YYYY-MM-DD HH:mm');
      var stat=false;
      if(num==="1"){
        setDrugData({
          drugdata:drugs.drugdata.concat([{drugName:"",prescBy:context.logindata.firstName+" "+context.logindata.lastName,dateTime:nowDate, isStop:false,dose:"",route :"",freq:"",stopDate:nowDate,stat:false}])
        });
      }else if(num==="2"){
        setDrugData({
          drugdata:drugs.drugdata.concat([{drugName:"",prescBy:context.logindata.firstName+" "+context.logindata.lastName,dateTime:nowDate, isStop:false,dose:"",route :"",freq:"",stopDate:nowDate,infusion:true,stat:false}])
        });
      }else if(num==="3"){
        setDrugData({
          drugdata:drugs.drugdata.concat([{drugName:"",prescBy:context.logindata.firstName+" "+context.logindata.lastName,dateTime:nowDate,isStop:false,dose:"",route :"",freq:"",stopDate:nowDate,stat:true}])
        });
        };
      }
      


    // OnChange event we will call this and update the state
  const handleDateChange = idx => date => {
    const convertDate = DateUtils.convertStringToDate(date);
    let newData = drugs.drugdata.map((obj, sidx) => {
      if (idx !== sidx) return obj;
      return {
        ...obj, dateTime: convertDate
      };
    });
    setDrugData({ drugdata:newData})
  }

    const validateDrug=()=>{
      if(drugs.drugdata[0].drugName===""){
        setNotiMessageShow(true);
        setNotiMessage("Please enter drug name");
        setNotiVarient('danger')
        return false;
      }
      return true;
    }
    
    const saveDrugDetails = () => {
      if(validateDrug()){
        for(let val of drugs.drugdata){
          if(val._id===""){
            val.dateTime=DateUtils.convertStringToDate(new Date());
            val.stopDate=DateUtils.convertStringToDate(new Date());
          }
        }
      const payload={
         "_id":"",
         "usrid":userId,
         "patientId":props.patientData.patientId,
         "drugdata":drugs.drugdata
      }
      saveDrugData(payload).then((resp) => {
          if (resp.suc){
            const req={"patientId":props.patientData.patientId}
            retrieveDrug(req).then((response) => {
               if (response.suc && response.res!==undefined){
                 setValuesToDrug(response.res);
                 setEnableBtn(true);
               }  
           });
            props.sendDrug("Drug Suc");
          }else{
              setNotiMessageShow(true);
              setNotiMessage("Patient Creattion Failed");
              setNotiVarient('danger')
              hidemessage();
          }
      });
  }
  window.scrollTo(0, 0);
  }
    return (
        <>
        {props.patientData!==undefined &&
                <div className="row">
                  <div className="col-sm-6">Patient Name : <b>{props.patientData.name}</b></div>
                  <div className="col-sm-6 text-right">MR Number : <b>{props.patientData.mrNum}</b></div>
                  
                   {props.patientData.drugReactions!==undefined &&
                   <div className="container drugchart">
                     <hr/>
                   <Form.Row>
                   <Form.Group as={Col} xs={12} controlId="drugDesc" className="mb-1 mt-3">
                    <Form.Control as="textarea" rows="2" placeholder="Drug" disabled="true" value={props.patientData.drugReactions} />
                    <Form.Label>Drug Reactions</Form.Label>
                    </Form.Group>
                   </Form.Row>
                   </div>
                  }
                </div>
                }
            <div className="container drugchart mt-3">
            <Alert className="notification" variant={notiVarient} show={notiMessageShow}
                onClose={() => setNotiMessageShow(false)}
                dismissible> {notiMessage}</Alert> 
            <div className="">
             
              {drugs.drugdata && drugs.drugdata.map((drug, idx) => (
              <Form.Row className="divider"
              className={"divider " + (drug.isStop ? ' in-active' : '')}>
                <div className="col-sm-12">
                {!drug.infusion && !drug.stat &&
                <h6 className="mb-3 text-primary">Drug</h6>
                }
                {drug.infusion &&
                <h6 className="mb-3 text-primary">Drug Infusions</h6>
                }
                {drug.stat &&
                <h6 className="mb-3 text-primary">STAT Drug </h6>
                }
               </div>
                <React.Fragment key={idx}>
                  <Form.Group as={Col} xs={4} controlId="drugName">
                    <Form.Control type="text" placeholder="Enter Name" disabled={drug.isStop} value={drug.drugName} onChange={handleChange(idx)} />
                    <Form.Label>Drug Name <sup>*</sup></Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={4} controlId="prescBy">
                    <Form.Control type="text" disabled="true" placeholder="Enter Email" value={drug.prescBy} onChange={handleChange(idx)} />
                    <Form.Label>Prescribed By<sup>*</sup></Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={3} className="selectplugin datePicker" controlId="dob">
                            <DatePicker
                            selected={drugdate}
                            onChange={handleDateChange(idx)}
                            value={drug.dateTime}
                            minDate={new Date()}
                            dateFormat="dd-MMM-YYY HH:mm"
                            disabled={true}
							               />
                            <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                            {!drug.stat &&
                            <Form.Label>Start Date & Time <sup>*</sup></Form.Label>
                            }
                            {drug.stat &&
                            <Form.Label>Date & Time <sup>*</sup></Form.Label>
                            }
                            </Form.Group>
                            
                    {(idx > 0) && drug._id===undefined && !drug.isStop ? (
                   
                  <Button
                    size="xs"
                    variant="link"
                    className="dlt iconBtn mt-1 ml-3 mr-3"
                    onClick={removeDrugData(idx)}
                    type="submit"><Icon className="edit ml-1 mr-1" color="#475F7B" size={20} icon="delete" />
                  </Button>
                  ) : ''}
                 
               
                               
                  <Form.Group as={Col} xs={2} controlId="dose">
                    <Form.Control type="text" placeholder="Dose" disabled={drug.isStop} value={drug.dose}  onChange={handleChange(idx)} />
                    <Form.Label>Dose</Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} xs={2} controlId="route">
                    <Form.Control type="text" placeholder="Route"  disabled={drug.isStop} value={drug.route} onChange={handleChange(idx)} />
                    <Form.Label>Route</Form.Label>
                  </Form.Group>
                  {!drug.infusion && !drug.stat &&
                  <Form.Group as={Col} xs={2} controlId="freq">
                    <Form.Control type="text" placeholder="Frequency"  disabled={drug.isStop} value={drug.freq} onChange={handleChange(idx)} />
                    <Form.Label>Frequency</Form.Label>
                  </Form.Group>
                  } 
                  {drug.infusion && !drug.stat &&
                  <Form.Group as={Col} xs={2} controlId="freq">
                    <Form.Control type="text" placeholder="Frequency"  disabled={true} value={drug.freq} onChange={handleChange(idx)} />
                    <Form.Label>Frequency</Form.Label>
                  </Form.Group>
                  }
                  {drug.isStop && !drug.stat &&
                  <Form.Group as={Col} xs={3} controlId="stoppedBy">
                    <Form.Control type="text" disabled="true" placeholder="Enter Email" value={drug.stoppedBy} />
                    <Form.Label>Stopped By<sup>*</sup></Form.Label>
                  </Form.Group>
                  }
                   {!drug.stat &&
                  <Form.Group as={Col} xs={3} className="selectplugin datePicker" controlId="dob">
                  
                            <DatePicker
                            selected={drugdate}
                            value={drug.stopDate}
                            minDate={new Date()}
                            dateFormat="dd-MMM-YYY HH:mm"
                            disabled={true}
							               />
                            <Icon className="calendar" color="#9892AC" size={18} icon="calendar1" />
                            <Form.Label>Stop Date & Time</Form.Label>
                            </Form.Group>
                      }                 
                  {!drug.stat && enableBtn && !drug.isStop && drug._id!==undefined && context.logindata.permissions!==undefined && context.logindata.permissions.includes("ED") &&
                            <Form.Group>
                               <Button
                                    size="xs"
                                    variant="outline-primary"
                                    onClick={stopActiveDrug(drug._id)}
                                    type="submit"
                                > Stop Drug </Button>
                             </Form.Group>
                             
                              }
                     
                
               
                </React.Fragment>
              </Form.Row>
            ))}
             {context.logindata.permissions!==undefined && context.logindata.permissions.includes("AD")  &&
            <div className="row">
              <Button
              size="xs"
              variant="link"
              className="add addbtn align-self-end mb-4-3"
              onClick={addDrugData("1")}
              type="submit">Add New Drug</Button><br/>
              <Button
              size="xs"
              variant="link"
              className="add addbtn align-self-end mb-4-3"
              onClick={addDrugData("2")}
              type="submit">Add Drug Infusions</Button><br/>
              <Button
              size="xs"
              variant="link"
              className="add addbtn align-self-end mb-4-3"
              onClick={addDrugData("3")}
              type="submit">Add STAT Drug</Button>
            </div>
             }
           </div>
           <hr />
           {context.logindata.permissions!==undefined && context.logindata.permissions.includes("AD")  &&
            <div className="text-right buttonGrop pb-0">
              <Button
                  size="xs"
                  variant="outline-primary"
                  onClick={saveDrugDetails}
                  type="submit"
                  loading={state.loadOfice}
              > Update</Button>
          </div>
           }
      </div>
        </>
    )
}
export default DrugChart