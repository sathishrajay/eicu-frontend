import React, { useEffect, useReducer, useState , useContext } from 'react';
import { Col, Form, Table, Alert } from "react-bootstrap";
import { useHistory } from 'react-router-dom';
import Button from '../../common/buttons/Button';
import Footer from '../../common/Footer';
import Header from '../../common/Header';
import Icon from '../../common/icomoonIcon/Icon';
import MainMenu from '../../common/MainMenu';
import LocationService from '../../../services/administration/LoationService';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { creation, retriveHospitals, update } from './operations';
import { Context,LocationContext } from "../../../../App";

import { Link } from 'react-router-dom';
var cities = require('../../masters/Cities.json');
var bedList = require('../../masters/Beds.json');


/**
 * @description:This function will creaet Hospitales and this will provide the ADD,EDIT and Remove functionalities
 * @author: Lakshmi
 * @param {*}
 * @function Hospital
 * @date : 13-08-2020
 */

/**
 * Initial State Declaration
 */
const initialState = {
    _id: undefined,hnm:"",st:true,em:"",ph:"",add:"",sta:"",cty:"",desc:"",pcode:"",lcode:"",lnumber:"",noofbed:0,citiesMenu:[],response:[],bedsList:[],citiesList:[],enabelUpdate:false,notiMessage:"",notiMessageShow:false,notiVarient:"",totalBeds:0,oldBeds:0
};
//=====================================
// Reducer function for userReact Hook
// which will update the state
//=====================================
const reducer = (state, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...state,hnm:"",st:true,em:"",ph:"",add:"",sta:"",cty:"",desc:"",pcode:"",lcode:"",noofbed:0,bedsList:[],enabelUpdate:false,code:"",lnumber:""
            };
        case 'editHospital':
            return Object.assign(state, action.payload);
        default:
            return { ...state, [action.type]: action.payload };
    }
};

function AdmissionForm() {
    const [state, dispatch] = useReducer(reducer, initialState);
    const history = useHistory();
    const [response, setResponse] =useState({hospList:[]})
    const [locationContext, setLocationContext] = useContext(LocationContext);
    const [context, setContext] = useContext(Context);
    const [errorMsges, setErrorMsges] = useState([]);


    useEffect(() => {
      dispatch({ type: "st", payload: true })
      dispatch({ type: "citiesList", payload: cities })

       let beds = bedList.map((bed) =>
       <option value={bed.value}>{bed.label}</option>
       );
       dispatch({ type: "bedsList", payload: beds })

        let ctyList = cities.map((cty) =>
        <option value={cty.ccode}>{cty.cname}</option>
        );
        dispatch({ type: "citiesMenu", payload: ctyList })
        
        let shm = localStorage.getItem("shm");
        if(shm !== undefined && shm !== null && shm === "yes"){
            dispatch({ type: 'showMsg', payload: true })
            dispatch({ type: 'notiMessageShow', payload: true })
            dispatch({ type: 'notiMessage', payload: 'Hospital Details Added successfully' })
            dispatch({ type: 'notiVarient', payload: 'success' })
            hidemessage();
            localStorage.removeItem("shm");
        }
        retriveHospitals().then(response => {
               if(response.data.suc){
                loadHospitals(response.data.res);
                }
        })
    }, []);

    const loadHospitals=(response)=>{
        let data=[];
        for(let val of response){
            var state="";
            if(val.state==="TS"){
                state="Telanagana";
            }else{
                state="Andhra Pradesh";
            }
          var cityName=LocationService.fetchCityName(val.city)
          data.push({hospCode: val.code.toUpperCase(),hospName:<>{val.isactive && <Link type="submit" variant="primary" size="lg" block  onClick={redirectToHome(val.code)}>{val.name}</Link>} {!val.isactive && val.name} </>,country:"India",state:state,city:cityName,beds:val.noofbeds,status:(val.isactive)?"Active":"Inactive", options: <><a onClick={editHospital(val)}><Icon className="edit ml-1 mr-1" color="#475F7B" size={15} icon="edit" /></a></> })
          }
          setResponse({hospList:data})
    }
    const redirectToHome=(code)=>()=>{
        window.location = "/eicu/admin/location/"+code+"/"+1;
    }

    // OnChange event we will call this and update the state
    const handleChange = (e) => {
        setErrorMsges([]);
        const { id, value } = e.target;
            if (e.target.id === "act") {
                if (state.st) {
                  dispatch({ type: 'st', payload: false })
                } else {
                  dispatch({ type: 'st', payload: true })
                }
              }
            //   if(e.target.id==="noofbed"){
            //       let beds=Number(state.oldBeds) + Number(value);
            //     dispatch({ type: 'totalBeds', payload: beds })
            //   }

              dispatch({ type: id, payload: value });
    }

    const validateSearch = () => {
        if (state.src === "") {
          document.querySelector('.origin').classList.add('errorSerField');
      
          return false
        }
         return true;
    }

    const addHospital = () => {
        let errorMsg = validateHspData();
        if(errorMsg.length > 0){
            setErrorMsges(errorMsg);
            dispatch({ type: 'notiMessageShow', payload: true })
            dispatch({ type: 'notiVarient', payload: 'danger' })
            window.scrollTo(0,0);
        }else{
            setErrorMsges([]);
            addHospitalFn();
        }
    }

    const validateHspData = () => {
        let errorMsg =[];
        if(state.hnm === null || state.hnm === ""){
            errorMsg.push("Hospital Name must be provided");
        }else if(state.em === null || state.em === ""){
            errorMsg.push("Email must be provided");
        }else if(state.ph === null || state.ph === ""){
            errorMsg.push("Contact Number must be provided");
        }else if(state.sta === null || state.sta === ""){
            errorMsg.push("State must be provided");
        }else if(state.cty === null || state.cty === ""){
            errorMsg.push("City must be provided");
        }else if(state.noofbed === null || state.noofbed === "" || state.noofbed === 0){
            errorMsg.push("No of Beds must be provided");
        }
        return errorMsg;
    }
    //Add Hospital
    const addHospitalFn = () => {
        const payload={
            name:state.hnm,
            isactive:state.st,
            email:state.em,
            phone:state.ph,
            pincode:state.pcode,
            state:state.sta,
            city:state.cty,
            noofbeds:state.totalBeds,
            description:state.desc,
            address:state.add,
            crby:"eicuuser@gmail.com",
            landLineCode:state.lcode,
            landLineNumber:state.lnumber
        }
        creation(payload).then(async (resp) => {
            if (resp.suc) {
                dispatch({ type: 'clear', payload: true })
                dispatch({ type: 'showMsg', payload: true })
                dispatch({ type: 'notiMessageShow', payload: true })
                dispatch({ type: 'notiMessage', payload: 'Hospital Details Added successfully' })
                dispatch({ type: 'notiVarient', payload: 'success' })
                retriveHospitals().then(response => {
                    if(response.data.suc){
                     loadHospitals(response.data.res);
                     }
                });
                await loadLocationData();
                localStorage.setItem("shm", "yes");
                hidemessage();
                window.location = "/eicu/admin/hospital";
            }else{
                dispatch({ type: 'notiMessageShow', payload: true })
                dispatch({ type: 'notiMessage', payload: 'Hospital Creattion Failed' })
                dispatch({ type: 'notiVarient', payload: 'danger' })
                hidemessage();
            }
        });
        window.scrollTo(0, 0);
    }
    //Update Hospital

    const updateHospital = () => {
        let errorMsg = validateHspData();
        if(errorMsg.length > 0){
            setErrorMsges(errorMsg);
            dispatch({ type: 'notiMessageShow', payload: true })
            dispatch({ type: 'notiVarient', payload: 'danger' })
            window.scrollTo(0,0);
        }else{
            setErrorMsges([]);
            updateHospitalFn();
        }
    }
    const updateHospitalFn = () => {
        const payload={
            _id:state._id,
            code:state.code,
            name:state.hnm,
            isactive:state.st,
            email:state.em,
            phone:state.ph,
            pincode:state.pcode,
            state:state.sta,
            city:state.cty,
            noofbeds:Number(state.totalBeds) + Number(state.noofbed),
            description:state.desc,
            address:state.add,
            landLineCode:state.lcode,
            landLineNumber:state.lnumber,
            upby:"eicuuser@gmail.com"
        }
        update(payload).then((updateRes) => {
            if (updateRes.data.suc) {
                dispatch({ type: 'clear', payload: true })
                dispatch({ type: 'enabelUpdate', payload: false })
                dispatch({ type: 'showMsg', payload: true })

                dispatch({ type: 'notiMessageShow', payload: true })
                dispatch({ type: 'notiMessage', payload: 'Hospital Details Updated successfully' })
                dispatch({ type: 'notiVarient', payload: 'success' })
                loadLocationData();
                let beds = bedList.map((bed) =>
                <option value={bed.value}>{bed.label}</option>
                );
                dispatch({ type: "bedsList", payload: beds })
                retriveHospitals().then(response => {
                    if(response.data.suc){
                     loadHospitals(response.data.res);
                     }
             })
            }
            window.scrollTo(0, 0);
            hidemessage();
        });
    }
    
    const hidemessage=() =>{
        setTimeout(function () {
            dispatch({ type: 'notiMessageShow', payload: false })
            dispatch({ type: 'notiMessage', payload: '' })
            dispatch({ type: 'notiVarient', payload: '' })
        }, 5000);
      }

    // Edit Hospital
        const editHospital = (obj) => () => {
        dispatch({ type: 'enabelUpdate', payload: true })
        dispatch({ type: '_id', payload: obj._id })
        dispatch({ type: 'code', payload: obj.code })
        dispatch({ type: 'hnm', payload: obj.name })
        dispatch({ type: 'st', payload: obj.isactive })
        dispatch({ type: 'em', payload: obj.email })
        dispatch({ type: 'ph', payload: obj.phone })
        dispatch({ type: 'pcode', payload: obj.pincode })
        dispatch({ type: 'sta', payload: obj.state })
        dispatch({ type: 'cty', payload: obj.city })
        dispatch({ type: 'desc', payload: obj.description })
        dispatch({ type: 'add', payload: obj.address })
        //dispatch({ type: 'noofbed', payload: Number(obj.noofbeds)})
        dispatch({ type: 'totalBeds', payload: Number(obj.noofbeds)})
        dispatch({ type: 'oldBeds', payload: Number(obj.noofbeds)})
        dispatch({ type: 'lcode', payload: obj.landLineCode})
        dispatch({ type: 'lnumber', payload: obj.landLineNumber})

        let citieList=cities.filter((s, sidx) => s.sta===obj.state)
        let ctyList = citieList.map((cty) =>
        <option value={cty.ccode}>{cty.cname}</option>
        );
        dispatch({ type: "citiesMenu", payload: ctyList })

        let bedsList=bedList.filter((s, sidx) => Number(s.value)===Number(obj.noofbeds))
        let beds = bedList.map((bed) =>
        <option value={bed.value}>{bed.label}</option>
        );
       dispatch({ type: "bedsList", payload: beds })
       window.scrollTo(0, 0);
    }

    const clear = () => {
        window.location="/eicu/admin/hospital";
    }

    const handleStateChange = (e) => {
            const { id, value } = e.target;
            let cities=state.citiesList.filter((s, sidx) => s.sta===value)
            let ctyList = cities.map((cty) =>
            <option value={cty.ccode}>{cty.cname}</option>
            );
            dispatch({ type: "citiesMenu", payload: ctyList })
            dispatch({ type: id, payload: value });
      }
      const resetState=()=>{
        dispatch({ type: 'clear', payload: true })
        
      }


      const loadLocationData = () => {
          LocationService.fetchMenus().then((resp) => {
          if (resp != undefined && resp.data !== undefined && resp.data.res !== undefined && resp.data.res !== null ) {
              setLocationContext({locations:resp.data.res})
          }else{
              setLocationContext()
          }
        });
      }

      const columns = [{
        dataField: 'hospCode',
        text: 'Hospital Code',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'hospName',
        text: 'Hospital Name',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'country',
        text: 'Country',
        sort: false,
        filter:false
      },
      {
        dataField: 'state',
        text: 'State',
        sort: false,
        filter:textFilter()
      },
      {
        dataField: 'city',
        text: 'City',
        sort: true,
        filter: textFilter()
      },

      {
        dataField: 'beds',
        text: 'Beds',
        sort: true,
        filter:textFilter()
      },
      {
        dataField: 'status',
        text: 'Status',
        sort: false,
        filter: false
      },

      
      {
        dataField: 'options',
        text: 'Actions',
        class:'tes'
      }];
    
      const defaultSorted = [{
        dataField: 'id',
        order: 'asc'
      }];
    
    //===================
    // Template Part
    //===================

    return (
        <>
            <Header />
            <MainMenu active='Hospital Creation' />

            <Alert className="notification" variant={state.notiVarient} show={state.notiMessageShow}
                onClose={() => dispatch({ type: "notiMessageShow", payload: false })}
                dismissible> {state.notiMessage}</Alert>

            {errorMsges !== null && errorMsges.length > 0 && 
            <Alert className="notification" variant={state.notiVarient} show={state.notiMessageShow}
                onClose={() => dispatch({ type: "notiMessageShow", payload: false })}
                dismissible> {errorMsges.map(itm => {
                    return(<li>{itm}</li>)
                })}
            </Alert>
            } 
            
            <div className="container">
                <div className="cardMainlayout pt-0">
                    <div className="contentArea main-wrapper">
                        <Form.Row>
                            <Form.Group as={Col} xs={3} controlId="hnm">
                                <Form.Control type="text" placeholder="Enter Office Name" autoComplete="off" value={state.hnm} onChange={handleChange} />
                                <Form.Label>Hospital Name <sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} className=" text-right" xs={3} controlId="st">
                                <Form.Check
                                    type="switch"
                                    id="act"
                                    label="Status"
                                    checked={state.st} value={state.st} onChange={handleChange}
                                />
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={3} controlId="em">
                                <Form.Control type="text" placeholder="Enter Address" value={state.em} onChange={handleChange} />
                                <Form.Label>Hospital Email <sup>*</sup></Form.Label>
                            </Form.Group>
            
                            <Form.Group as={Col} xs={3} controlId="ph">
                                <Form.Control type="text" placeholder="Enter Landmark" value={state.ph} onChange={handleChange} />
                                <Form.Label>Contact Number <sup>*</sup></Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                        <Form.Group as={Col} xs={1} controlId="lcode">
                                <Form.Control type="text" placeholder="Enter Address" value={state.lcode} onChange={handleChange} />
                                <Form.Label>Code</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={2} controlId="lnumber">
                                <Form.Control type="text" placeholder="Enter Address" value={state.lnumber} onChange={handleChange} />
                                <Form.Label>Landline Number</Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="pcode">
                                <Form.Control type="text" placeholder="Enter Address" value={state.pcode} onChange={handleChange} />
                                <Form.Label>Postal Code</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={6} controlId="add">
                                <Form.Control as="textarea" rows="2" placeholder="Enter Description" value={state.add} onChange={handleChange} />
                                <Form.Label>Address</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                        <Form.Group as={Col} xs={2} controlId="sta">
                                {/* <Form.Label></Form.Label> */}
                                <Form.Control as="select" value={state.sta} onChange={handleStateChange}>
                                    <option value="">Select</option>
                                    <option value="TS">Telanagana</option>
                                    <option value="AP">Andhra Pradesh</option>
                                </Form.Control>
                                <Form.Label>State <sup>*</sup></Form.Label>
                            </Form.Group>
                            
                            <Form.Group as={Col} xs={2} controlId="cty">
                                {/* <Form.Label></Form.Label> */}
                                <Form.Control as="select" value={state.cty} onChange={handleChange}>
                                    <option value="">Select</option>
                                    {state.citiesMenu}
                                </Form.Control>
                                <Form.Label>City <sup>*</sup></Form.Label>
                            </Form.Group>
                            {state.enabelUpdate &&
                            <Form.Group as={Col} xs={1} controlId="totalBeds">
                            <Form.Control type="text" disabled="true"  placeholder="Enter Address" value={state.totalBeds} />
                            <Form.Label>Total Beds</Form.Label>
                            </Form.Group>
                            }
                            <Form.Group as={Col} xs={state.enabelUpdate?1:2} controlId="noofbed">
                                {/* <Form.Label></Form.Label> */}
                                <Form.Control as="select" value={state.noofbed} onChange={handleChange}>
                                    <option value="">Select</option>
                                    {state.bedsList}
                                </Form.Control>
                                {state.enabelUpdate &&
                                  <Form.Label>Add Beds <sup>*</sup></Form.Label>
                                }
                                 {!state.enabelUpdate &&
                                  <Form.Label>No of Beds <sup>*</sup></Form.Label>
                                }
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col}xs={6} controlId="desc">
                                <Form.Control as="textarea" rows="2" placeholder="Enter Description" value={state.desc} onChange={handleChange} />
                                <Form.Label>Description</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <div className="text-right buttonGrop pb-0">
                            {!state.enabelUpdate && context.logindata!==undefined && context.logindata.permissions!==undefined && context.logindata.permissions.includes("AH") && (
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    onClick={addHospital}
                                    loading={state.loadOfice}
                                    type="submit"
                                > Add Hospital</Button>
                            )}

                           {state.enabelUpdate && context.logindata.permissions!==undefined && context.logindata.permissions.includes("EH") && (
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    onClick={clear}
                                    loading={state.loadOfice}
                                    type="submit"
                                > Clear</Button>
                            )}
                            
                            {state.enabelUpdate && context.logindata.permissions!==undefined && context.logindata.permissions.includes("EH") && (
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    onClick={updateHospital}
                                    loading={state.loadOfice}
                                    type="submit"
                                > Update Hospital</Button>
                            )}

                        </div>
                    </div>
                </div>
                <div className="cardMainlayout p-0">
                    <div className="cardMainlayout p-0">
                               {response.hospList.length!==0 &&
                                   <BootstrapTable
                                    bootstrap4
                                    keyField="id"
                                    classes ="markup filterTbl"
                                    data={response.hospList}
                                    columns={ columns }
                                    defaultSorted={ defaultSorted }
                                    filter={ filterFactory()}
                                    pagination={ paginationFactory() }
                                    />
                               }
                               {response.hospList.length===0 &&
                                <span>No records found</span>
                               }
                            </div>
                </div>
            </div>
            <Footer />
        </>
    )
}
export default AdmissionForm