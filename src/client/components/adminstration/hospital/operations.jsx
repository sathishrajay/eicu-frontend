import React from 'react';
import HospitalCreationService from '../../../services/administration/HospitalCreationService';

/**
 * @description: CRUD operations for Hospital
 * @author: Lakshmi
 * @param {*}
 * @function Branch
 * @date : 13-08-2020
 */



// ========================================================================
//  Branch Create Operation
//  This will create Branch and save the details 
// ========================================================================
export const creation = async (obj) => {
    let finalRes=[];
    await HospitalCreationService.saveHospital(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}


// ========================================================================
//  Branch Update Operation
//  This function will call the service to delete the selected branch
// ========================================================================
export const update = async (obj) => {
    let finalRes = {};
    await HospitalCreationService.updateHospital(obj).then(response => {
        finalRes = response;
    });
    return Promise.resolve(finalRes);
}

// ========================================================================
// All Branch Data List
//  This function update the state values while clicking on Edit
// ========================================================================
export const retriveHospitals = async () => {
    let hopitalsList = [];
    await HospitalCreationService.getHospitalsList().then(response => {
        if (response.data.suc) {
            hopitalsList = response;
        }
    });
    return Promise.resolve(hopitalsList);
}





