import React, { useEffect, useReducer, useState } from 'react';
import { Line } from 'react-chartjs-2';
import { useHistory } from 'react-router-dom';
import { CardDeck, Card, Button, Modal} from 'react-bootstrap';
import {getHospMRCodes,checkAuthenticate,getPatientData,authRenew} from './operations';
export const AUTH_TOKEN = "authToken";
const initialState = {
    info: ""
};

const reducer = (state, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...state, info: ""
            };
        default:
            return { ...state, [action.type]: action.payload };
    }
};

function StasisInfo(props) {
    const [state, dispatch] = useReducer(reducer, initialState);
    const history = useHistory();
    const [viewAll, setViewAll] = useState();
    const [patiensList, setPatientsList] = useState([]);
    const [selPatientInfo, setSelPatientInfo] = useState();
    const [show, setShow] = useState(props.openPopup);
    const [graphData, setGraphData] = useState([]);
    const [testData, setTestDaata] = useState([]);
    const [intVal, setIntVal] =useState(0);

    


    const randomGen = () => {
        let dataAr = [];
        var min = 1;
        var max = 100;
        let ddd = [1,2,3,4,5,6,7,8,9,10,1,1,1,1,1,1,1,1,11,1,1,11,1,1,1,11,1,1,1,1,11,1,1,1,1,1,11,1,1];
        for (let val of ddd){
            var rand =  min + (Math.random() * (max-min));
            dataAr.push(rand);
        }
        return dataAr;
    }


    const MINUTE_MS_1 = 1000;
    useEffect(() => {
    const interval = setInterval(() => {
        testData.push(intVal);
        setIntVal(intVal+1)
    }, MINUTE_MS_1 );

    return () => clearInterval(interval); // This represents the unmount function, in which you need to clear your interval to prevent memory leaks.
    }, []);

    const MINUTE_MS = 2000;
        useEffect(() => {
        const interval = setInterval(() => {
            //let data =["5","10","5","20","50","30","15","50","30","10","45"];
            let dd = randomGen();
            graphDataFn(dd);
            setShow(true);
        }, MINUTE_MS );

        return () => clearInterval(interval); // This represents the unmount function, in which you need to clear your interval to prevent memory leaks.
        }, []);

    const [showPopup,setShowPopup] = useState(false);
    
    useEffect(() => {
        setViewAll(true);
        let data = [1,2,3,7];
        setPatientsList(data);
        var hospital_id=props.match.params.hospitalId;
        // if(window.location.pathname!==undefined){
        //  let hoptArray=window.location.pathname.split("/");
        //  hospital_id=hoptArray[hoptArray.length-1]
        // }else{
        //  let hoptArray=window.location.href.split("/");
        //  hospital_id=hoptArray[hoptArray.length-1]
        // }
        if(hospital_id!==undefined){
            loadPatientDetails(hospital_id);
        }
    }, []);

    
    useEffect(() => {
    }, [graphData]);

    const loadPatientDetails= async (hospital_id)=>{
        await getHospMRCodes({"hospId" : hospital_id}).then((mrresp) => {
           console.log("MR Response :"+JSON.stringify(mrresp));
         if(mrresp.res.length!=0){
            callAuthentication();
           let patiendata=[];
           let authExp=false;
           if(mrresp.res.length!=0){
            for(let val of mrresp.res){
              if(!authExp){
               getPatientData(val).then((presp) => {
                    console.log("Patient Response :"+JSON.stringify(presp.data));
                    if(presp!==undefined && presp.data!==undefined && presp.data.status==="200"){
                        patiendata.push(presp.data.res);
                    }else if(presp!==undefined && presp.data!==undefined && presp.data.status==="401"){
                        authExp=true;
                        callRenewAuth();
                    }else{
                        setShowPopup(true);
                    }
                    });
              }else{
                  break;
              }
              }
              //setPatientsList(patiendata);
              }
          let checResp=false;
          if(authExp){
            patiendata=[];
            if(mrresp.suc && mrresp.res.length!=0){
              for(let val of mrresp.res){
                if(!checResp){
                getPatientData(val).then((presp) => {
                    console.log("Patient Response 2 :"+JSON.stringify(presp.data));
                    if(presp!==undefined && presp.data!==undefined && presp.data.status==="200"){
                        patiendata.push(presp.data.res);
                    }else if(presp!==undefined && presp.data!==undefined && presp.data.status==="401"){
                        checResp=true;
                    }else{
                        setShowPopup(true);
                    }
                });
              }else{
                setShowPopup(true);
                  break;
              }
            }
             //setPatientsList(patiendata);
          }
        }
       }else{
        setShowPopup(true);
       }
     });
       }

       const getPatintResp = (val)=>{
          getPatientData(val).then((presp) => {
            console.log("Patientttttttttttttttttt :"+JSON.stringify(presp.data));
            if(presp!==undefined){
            return presp;
            }
          });
       }
      
     //Creating the New Auth Key
      const callRenewAuth=()=>{
        authRenew().then((res) => {
          console.log("Re new Response :"+JSON.stringify(res));
          if(res.suc && res.status==="200"){
           localStorage.removeItem(AUTH_TOKEN);
           localStorage.setItem(AUTH_TOKEN,res.res);
           return true;
          }else{
           return false;
          }
        });
      }
     //Creatingh the Auth Key
      const callAuthentication= async ()=>{
        var authToken=localStorage.getItem(AUTH_TOKEN);
        if(authToken===undefined || authToken==="" || authToken===null){
          await checkAuthenticate().then((authresp)  => {
            console.log("Auth Response :"+authresp.status);
            if(authresp.status==="200"){
              localStorage.setItem(AUTH_TOKEN,authresp.res);
              return true;
            }else{
              return callRenewAuth();
            }
        }
        )};
      }

    const selCardInfo = (inx) => {
        setSelPatientInfo("Selected Patient "+inx);
        let data =["10","20","5","30","25","40","35","50","20","60","10"];
        graphDataFn(data);
        setShow(true);
        setViewAll(false);
    }
    const changeReq = () => {
        setSelPatientInfo("");
        setViewAll(true);
    }

    const graphDataFn = (data) => {
        let graphData = {
            labels: data,
            datasets: [
              {
                label: "Car Speed",
                data: data,
                fill: false,
                lineTension: 0.5,
                backgroundColor: '#5e72e4',
                borderColor: '#5e72e4',
                borderWidth: 2,
              }
            ]
          }
          setGraphData(graphData);
    }
    
    

    return (
        <>
         <div className="container">
            <h1> Stasis integration under construction </h1>
            <div className="cardMainlayout pt-0 mt-0 black-bg">
                { viewAll && 
                        <div className="contentArea">
                        <CardDeck>
                            { patiensList.map((itm) => {
                                return (
                                    <Card onClick={() => selCardInfo(itm)}>
                                        <Card.Img variant="top" src="holder.js/100px160" />
                                        <Card.Body>
                                            <Card.Title>Patient {itm}</Card.Title>
                                            <Card.Text>
                                                Patient Info 
                                            </Card.Text>
                                        </Card.Body>
                                        <Card.Footer>
                                            <small className="text-muted">Last updated 3 mins ago</small>
                                        </Card.Footer>
                                    </Card>
                                    )
                                })
                            }
                        </CardDeck>
                    </div>
                }

                    { !viewAll && 
                        <div className="contentArea">
                        <CardDeck>
                            <Card className="black-bg">
                                {/* <Card.Img variant="top" src="holder.js/100px160" /> */}
                                <Card.Body>
                                    <Card.Title>{selPatientInfo}</Card.Title>
                                    <div className="row">
                                        <div className="col-sm-10">
                                        <div className="chart-panel">
                                        <Line
                                                data={graphData}
                                                options={{
                                                    responsive: true,
                                                    maintainAspectRatio: false,
                                                    title:{
                                                    display:true,
                                                    text: "test info",
                                                    fontSize:14,
                                                    position: 'left',
                                                    },
                                                    legend:{
                                                    display:true,
                                                    position:'right'
                                                    },
                                                    scales: {
                                                        yAxes: [{
                                                        gridLines: {
                                                            drawBorder: false,
                                                            display: false
                                                        },
                                                        ticks: {
                                                            display: false
                                                        }
                                                        }],
                                                        xAxes: [{
                                                            gridLines: {
                                                            display: false,
                                                            },
                                                            ticks: {
                                                                display: false
                                                            }
                                                        }]
                                                    },
                                                    legend: {
                                                        display: false,
                                                    },
                                                }}
                                            />
                                    </div>
                                    <div className="chart-panel">
                                        <Line
                                                data={graphData}
                                                options={{
                                                    responsive: true,
                                                    maintainAspectRatio: false,
                                                    title:{
                                                    display:true,
                                                    text: "test info",
                                                    fontSize:14,
                                                    position: 'left',
                                                    },
                                                    legend:{
                                                    display:true,
                                                    position:'right'
                                                    },
                                                    scales: {
                                                        yAxes: [{
                                                        gridLines: {
                                                            drawBorder: false,
                                                            display: false
                                                        },
                                                        ticks: {
                                                            display: false
                                                        }
                                                        }],
                                                        xAxes: [{
                                                            gridLines: {
                                                            display: false,
                                                            },
                                                            ticks: {
                                                                display: false
                                                            }
                                                        }]
                                                    },
                                                    legend: {
                                                        display: false,
                                                    },
                                                }}
                                            />
                                    </div>
                                    <div className="chart-panel">
                                        <Line
                                                data={graphData}
                                                options={{
                                                    responsive: true,
                                                    maintainAspectRatio: false,
                                                    title:{
                                                    display:true,
                                                    text: "test info",
                                                    fontSize:14,
                                                    position: 'left',
                                                    },
                                                    legend:{
                                                    display:true,
                                                    position:'right'
                                                    },
                                                    scales: {
                                                        yAxes: [{
                                                        gridLines: {
                                                            drawBorder: false,
                                                            display: false
                                                        },
                                                        ticks: {
                                                            display: false
                                                        }
                                                        }],
                                                        xAxes: [{
                                                            gridLines: {
                                                            display: false,
                                                            },
                                                            ticks: {
                                                                display: false
                                                            }
                                                        }]
                                                    },
                                                    legend: {
                                                        display: false,
                                                    },
                                                }}
                                            />
                                    </div>
                                        </div>
                                        <div className="col-sm-2 text-center">
                                            <div>
                                            <h1 className="text-warning display-3 font-weight-bold">76</h1>
                                            <h5 className="text-warning">Pulse</h5>
                                            </div>
                                            <div className="mt-5">
                                            <h1 className="text-info display-3 font-weight-bold">84</h1>
                                            <h5 className="text-info">Temp</h5>
                                            </div>
                                            <div className="mt-5">
                                            <h1 className="text-success display-3 font-weight-bold">18</h1>
                                            <h5 className="text-success">RR</h5>
                                            </div>
                                        </div>
                                        <div className="col-sm-12">
                                        <h1 className="text-primary display-2 font-weight-bold ml-5 mr-5 d-inline-block">121/82</h1>
                                        <h1 className="text-primary display-2 font-weight-bold d-inline-block">(89)</h1>
                                        </div>
                                    </div>
                                    
                                </Card.Body>
                                <Card.Footer>
                                    <small className="text-muted">Last updated 10 mins ago </small>
                                </Card.Footer>
                            </Card>
                        </CardDeck>
                        <Button onClick={() =>changeReq()}>View All</Button>
                    </div>
                }
            </div>
        </div>
    <Modal
        show={showPopup}
        onHide={() => setShowPopup(false)}
        backdrop="static"
        size="small"
        className="custom-dailog"
        keyboard={false}>
            <Modal.Header closeButton>
                <Modal.Title>Alert</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="">
                  <span>No Data found.Please try again later</span>
                </div>
             </Modal.Body>
    </Modal>
        </>
    )
}

export default StasisInfo
