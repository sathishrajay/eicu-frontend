import React from 'react';
import PatientInfoService from '../../../services/administration/PatientInfoService';
import PatientStasisService from '../../../services/administration/PatientStasisService';
export const AUTH_TOKEN = "authToken";
/**
 * @description: CRUD operations for Patient
 * @author: Praveen Varma
 * @param {*}
 * @function Branch
 * @date : 13-08-2020
 */


// ========================================================================
//  Branch Create Operation
//  This will create Branch and save the details 
// ========================================================================
export const getPatient = async (obj) => {
    let finalRes=[];
    await PatientInfoService.retrivePatientInfo(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

// ========================================================================
//  Branch Create Operation checkAuthenticate
//  This will create Branch and save the details 
// ========================================================================
export const getHospMRCodes = async (obj) => {
    let finalRes=[];
    await PatientStasisService.gerMrCodes(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

// ========================================================================
//  Branch Create Operation 
//  This will create Branch and save the details 
// ========================================================================
export const checkAuthenticate = async () => {
    let finalRes=[];
    await PatientStasisService.authentication().then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

// ========================================================================
//  Branch Create Operation 
//  This will create Branch and save the details 
// ========================================================================
export const getPatientData = async (code) => {
    let finalRes=[];
    let key=localStorage.getItem(AUTH_TOKEN);
    await PatientStasisService.getPatientData(code,key).then(response => {
        //console.log("Patient Response :"+JSON.stringify(response));
        finalRes = response;
    });
    return Promise.resolve(finalRes);
}

// ========================================================================
//  Branch Create Operation 
//  This will create Branch and save the details 
// ========================================================================
export const authRenew = async () => {
    let finalRes=[];
    await PatientStasisService.callRenewAuth().then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}






