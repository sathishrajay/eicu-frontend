import React, { useEffect, useReducer, useState , useContext } from 'react';
import { Col, Form, Table , Alert } from "react-bootstrap";
import { useHistory } from 'react-router-dom';
import Button from '../../common/buttons/Button';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { creation, update , getRoles } from './operations';
import { Context} from "../../../../App";
import Icon from '../../common/icomoonIcon/Icon';
var permisnJson = require('../../masters/Permissions.json');


/**
 * @description:This function will creaet Hospitales and this will provide the ADD,EDIT and Remove functionalities
 * @author: Lakshmi
 * @param {*}
 * @function Hospital
 * @date : 13-08-2020
 */

/**
 * Initial State Declaration
 */
const initialState = {
    _id: undefined,active:true,roleName:"",desc:"",notiMessage:"",notiMessageShow:false,notiVarient:"",permissions:[],rolesList:[],
    addHosp:false,viewHosp:false,editHosp:false,
    addPatient:false,viewPatient:false,editPatient:false,
    addLab:false,viewLab:false,editLab:false,
    addDrug:false,viewDrug:false,editDrug:false,
    addCCare:false,viewCCare:false,editCCare:false,
    addUser:false,viewUser:false,editUser:false,
    addRole:false,viewRole:false,editRole:false,
    addSpoc:false,viewSpoc:false
};
//=====================================
// Reducer function for userReact Hook
// which will update the state
//=====================================
const reducer = (state, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...state,_id: undefined,roleName:"",desc:"",active:true,
                addHosp:false,viewHosp:false,editHosp:false,
                addPatient:false,viewPatient:false,editPatient:false,
                addLab:false,viewLab:false,editLab:false,
                addDrug:false,viewDrug:false,editDrug:false,
                addCCare:false,viewCCare:false,editCCare:false,
                addUser:false,viewUser:false,editUser:false,
                addRole:false,viewRole:false,editRole:false,
                addSpoc:false,viewSpoc:false
            };
        case 'editHospital':
            return Object.assign(state, action.payload);
        default:
            return { ...state, [action.type]: action.payload };
    }
};

function RoleManagement() {
    const [state, dispatch] = useReducer(reducer, initialState);
    const history = useHistory();
    const [context, setContext] = useContext(Context);
    const [response, setResponse] =useState({hospList:[]})
    const [enabelUpdate , setEnabelUpdate] = useState(false)
    
    useEffect(() => {
        getRoles().then(response => {
                if(response.data!==undefined && response.data.suc){
                    loadRoles(response.data.res);
                }
        })
    }, []);

    const loadRoles=(response)=>{
        let data=[];
        for(let val of response){
            data.push({roleName: val.roleName,status:(val.isActive)?"Active":"Inactive", options: <><a onClick={editRole(val)}><Icon className="edit ml-1 mr-1" color="#475F7B" size={15} icon="edit" /></a></> })
          }
          dispatch({ type: 'rolesList', payload: data })
    }
   // Edit Hospital
   const editRole = (obj) => () => {
       console.log("ROLE :"+JSON.stringify(obj));
    dispatch({ type: '_id', payload: obj._id })
    dispatch({ type: 'active', payload: obj.isActive })
    dispatch({ type: 'roleName', payload: obj.roleName })
    dispatch({ type: 'desc', payload: obj.desc })

    if(obj.permissions && obj.permissions.length!==0){
     if(obj.permissions.includes("AH")){
        dispatch({ type: 'addHosp', payload: true })
     }else{
        dispatch({ type: 'addHosp', payload: false })
     }
     if(obj.permissions.includes("EH")){
        dispatch({ type: 'editHosp', payload: true })
     }else{
        dispatch({ type: 'editHosp', payload: false })
     }
     if(obj.permissions.includes("VH")){
        dispatch({ type: 'viewHosp', payload: true })
     }else{
        dispatch({ type: 'viewHosp', payload: false })
     }
     if(obj.permissions.includes("AP")){
        dispatch({ type: 'addPatient', payload: true })
     }else{
        dispatch({ type: 'addPatient', payload: false })
     }
     if(obj.permissions.includes("EP")){
        dispatch({ type: 'editPatient', payload: true })
     }else{
        dispatch({ type: 'editPatient', payload: false })
     }
     if(obj.permissions.includes("VP")){
        dispatch({ type: 'viewPatient', payload: true })
     }else{
        dispatch({ type: 'viewPatient', payload: false })
     }

     if(obj.permissions.includes("AL")){
        dispatch({ type: 'addLab', payload: true })
     }else{
        dispatch({ type: 'addLab', payload: false })
     }
     if(obj.permissions.includes("EL")){
        dispatch({ type: 'editLab', payload: true })
     }else{
        dispatch({ type: 'editLab', payload: false })
     }
     if(obj.permissions.includes("VL")){
        dispatch({ type: 'viewLab', payload: true })
     }else{
        dispatch({ type: 'viewLab', payload: false })
     }

     if(obj.permissions.includes("AD")){
        dispatch({ type: 'addDrug', payload: true })
     }else{
        dispatch({ type: 'addDrug', payload: false })
     }
     if(obj.permissions.includes("ED")){
        dispatch({ type: 'editDrug', payload: true })
     }else{
        dispatch({ type: 'editDrug', payload: false })
     }
     if(obj.permissions.includes("VD")){
        dispatch({ type: 'viewDrug', payload: true })
     }else{
        dispatch({ type: 'viewDrug', payload: false })
     }
     if(obj.permissions.includes("AU")){
        dispatch({ type: 'addUser', payload: true })
     }else{
        dispatch({ type: 'addUser', payload: false })
     }
     if(obj.permissions.includes("EU")){
        dispatch({ type: 'editUser', payload: true })
     }else{
        dispatch({ type: 'editUser', payload: false })
     }
     if(obj.permissions.includes("VU")){
        dispatch({ type: 'viewUser', payload: true })
     }else{
        dispatch({ type: 'viewUser', payload: false })
     }
     if(obj.permissions.includes("ACCN")){
        dispatch({ type: 'addCCare', payload: true })
     }else{
        dispatch({ type: 'addCCare', payload: false })
     }
     if(obj.permissions.includes("VCCN")){
        dispatch({ type: 'viewCCare', payload: true })
     }else{
        dispatch({ type: 'viewCCare', payload: false })
     }
     if(obj.permissions.includes("AR")){
        dispatch({ type: 'addRole', payload: true })
     }else{
        dispatch({ type: 'addRole', payload: false })
     }
     if(obj.permissions.includes("ER")){
        dispatch({ type: 'editRole', payload: true })
     }else{
        dispatch({ type: 'editRole', payload: false })
     }
     if(obj.permissions.includes("VR")){
        dispatch({ type: 'viewRole', payload: true })
     }else{
        dispatch({ type: 'viewRole', payload: false })
     }
     if(obj.permissions.includes("ASPOC")){
        dispatch({ type: 'addSpoc', payload: true })
     }
     if(obj.permissions.includes("VSPOC")){
        dispatch({ type: 'viewSpoc', payload: true })
     }
    }
    dispatch({ type: 'enabelUpdate', payload: true })
    window.scrollTo(0, 0);
}
    

        const handleChangePermission = (obj) => evt => {
        try {
            const { id, value } = evt.target;
            dispatch({ type: id, payload: value });
             let hospitalFun=[];
              if (evt.target.id === "AH") {
                if (state.addHosp) {
                    dispatch({ type: 'addHosp', payload: false })
                } else {
                    dispatch({ type: 'addHosp', payload: true })
                }
              }
              if (evt.target.id === "VH") {
                if (state.viewHosp) {
                    dispatch({ type: 'viewHosp', payload: false })
                } else {
                    dispatch({ type: 'viewHosp', payload: true })
                }
              }
              if (evt.target.id === "EH") {
                if (state.editHosp) {
                    dispatch({ type: 'editHosp', payload: false })
                } else {
                    dispatch({ type: 'editHosp', payload: true })
                }
              }
              if (evt.target.id === "AP") {
                if (state.addPatient) {
                    dispatch({ type: 'addPatient', payload: false })
                } else {
                    dispatch({ type: 'addPatient', payload: true })
                }
              }
              if (evt.target.id === "EP") {
                if (state.editPatient) {
                    dispatch({ type: 'editPatient', payload: false })
                } else {
                    dispatch({ type: 'editPatient', payload: true })
                }
              }
              if (evt.target.id === "VP") {
                if (state.viewPatient) {
                    dispatch({ type: 'viewPatient', payload: false })
                } else {
                    dispatch({ type: 'viewPatient', payload: true })
                }
              }

              if (evt.target.id === "AL") {
                if (state.addLab) {
                    dispatch({ type: 'addLab', payload: false })
                } else {
                    dispatch({ type: 'addLab', payload: true })
                }
              }
              if (evt.target.id === "EL") {
                if (state.editLab) {
                    dispatch({ type: 'editLab', payload: false })
                } else {
                    dispatch({ type: 'editLab', payload: true })
                }
              }
              if (evt.target.id === "VL") {
                if (state.viewLab) {
                    dispatch({ type: 'viewLab', payload: false })
                } else {
                    dispatch({ type: 'viewLab', payload: true })
                }
              }

              if (evt.target.id === "AD") {
                if (state.addDrug) {
                    dispatch({ type: 'addDrug', payload: false })
                } else {
                    dispatch({ type: 'addDrug', payload: true })
                }
              }
              if (evt.target.id === "ED") {
                if (state.editDrug) {
                    dispatch({ type: 'editDrug', payload: false })
                } else {
                    dispatch({ type: 'editDrug', payload: true })
                }
              }
              if (evt.target.id === "VD") {
                if (state.viewDrug) {
                    dispatch({ type: 'viewDrug', payload: false })
                } else {
                    dispatch({ type: 'viewDrug', payload: true })
                }
              }

              if (evt.target.id === "ACCN") {
                if (state.addCCare) {
                    dispatch({ type: 'addCCare', payload: false })
                } else {
                    dispatch({ type: 'addCCare', payload: true })
                }
              }
              if (evt.target.id === "VCCN") {
                if (state.viewCCare) {
                    dispatch({ type: 'viewCCare', payload: false })
                } else {
                    dispatch({ type: 'viewCCare', payload: true })
                }
              }

              if (evt.target.id === "AU") {
                if (state.addUser) {
                    dispatch({ type: 'addUser', payload: false })
                } else {
                    dispatch({ type: 'addUser', payload: true })
                }
              }
              if (evt.target.id === "EU") {
                if (state.editUser) {
                    dispatch({ type: 'editUser', payload: false })
                } else {
                    dispatch({ type: 'editUser', payload: true })
                }
              }
              if (evt.target.id === "VU") {
                if (state.viewUser) {
                    dispatch({ type: 'viewUser', payload: false })
                } else {
                    dispatch({ type: 'viewUser', payload: true })
                }
              }

              if (evt.target.id === "AR") {
                if (state.addRole) {
                    dispatch({ type: 'addRole', payload: false })
                } else {
                    dispatch({ type: 'addRole', payload: true })
                }
              }

              if (evt.target.id === "ER") {
                if (state.editRole) {
                    dispatch({ type: 'editRole', payload: false })
                } else {
                    dispatch({ type: 'editRole', payload: true })
                }
              }

              if (evt.target.id === "VR") {
                if (state.viewRole) {
                    dispatch({ type: 'viewRole', payload: false })
                } else {
                    dispatch({ type: 'viewRole', payload: true })
                }
              }

              if (evt.target.id === "ASPOC") {
                if (state.addSpoc) {
                    dispatch({ type: 'addSpoc', payload: false })
                } else {
                    dispatch({ type: 'addSpoc', payload: true })
                }
              }

              if (evt.target.id === "VSPOC") {
                if (state.viewSpoc) {
                    dispatch({ type: 'viewSpoc', payload: false })
                } else {
                    dispatch({ type: 'viewSpoc', payload: true })
                }
              }


        } catch (err) {
         // log.error('Exception occured in CompanyComponent function---' + err);
        }
      }
      


      const handleChange = (evt) => {
        try {
            const { id, value } = evt.target;
            dispatch({ type: id, payload: value });
            if (id === "active") {
                if (state.active) {
                  dispatch({ type: 'active', payload: false })
                } else {
                  dispatch({ type: 'active', payload: true })
                }
              }
        } catch (err) {
         // log.error('Exception occured in CompanyComponent function---' + err);
        }
      }

      const createPermissions=()=>{
          let data=[]
          if(state.addHosp){
            data.push("AH");
          }
          if(state.editHosp){
            data.push("EH");
          }
          if(state.viewHosp){
            data.push("VH");
          }
          if(state.addPatient){
            data.push("AP");
          }
          if(state.viewPatient){
            data.push("VP");
          }
          if(state.editPatient){
            data.push("EP");
          }
          if(state.addLab){
            data.push("AL");
          }
          if(state.editLab){
            data.push("EL");
          }
          if(state.viewLab){
            data.push("VL");
          }
          if(state.addDrug){
            data.push("AD");
          }
          if(state.editDrug){
            data.push("ED");
          }
          if(state.viewDrug){
            data.push("VD");
          }
          if(state.addCCare){
            data.push("ACCN");
          }
          if(state.viewCCare){
            data.push("VCCN");
          }
          if(state.addUser){
            data.push("AU");
          }
          if(state.viewUser){
            data.push("VU");
          }
          if(state.editUser){
            data.push("EU");
          }
          if(state.addRole){
            data.push("AR");
          }
          if(state.editRole){
            data.push("ER");
          }
          if(state.viewRole){
            data.push("VR");
          } 
          if(state.addSpoc){
            data.push("ASPOC");
          } 
          if(state.viewSpoc){
            data.push("VSPOC");
          } 
         return data;
      }
      const validateRole=(data)=>{
        if(state.roleName===""){
        dispatch({ type: 'notiMessageShow', payload: true })
        dispatch({ type: 'notiMessage', payload: 'Please enter Role name' })
        dispatch({ type: 'notiVarient', payload: 'danger' })
        return false;
        }else if(data.length===0){
        dispatch({ type: 'notiMessageShow', payload: true })
        dispatch({ type: 'notiMessage', payload: 'Please select atleast One Functionality' })
        dispatch({ type: 'notiVarient', payload: 'danger' })
        return false;
        }
        return true;
    }
    //Add Role
    const createRole = () => {
        let data=createPermissions()
        if(validateRole(data)){
            const payload={
                roleName:state.roleName,
                desc:state.desc,
                isActive:state.active,
                userId:context.logindata._id,
                permissions:data
              }
           creation(payload).then((resp) => {
            if (resp.suc) {
                dispatch({ type: 'clear', payload: true })
                dispatch({ type: 'showMsg', payload: true })
                dispatch({ type: 'notiMessageShow', payload: true })
                dispatch({ type: 'notiMessage', payload: 'Role Created Successfully' })
                dispatch({ type: 'notiVarient', payload: 'success' })
                return window.location="/eicu/admin/user"
            }else{
                dispatch({ type: 'notiMessageShow', payload: true })
                dispatch({ type: 'notiMessage', payload: 'Role Not Created' })
                dispatch({ type: 'notiVarient', payload: 'danger' })
                hidemessage();
            }
        });
    }
    }
    //Update Hospital
    const updateUser = () => {
        let data=createPermissions()
        if(validateRole(data)){
        const payload={
            _id:state._id,
            roleName:state.roleName,
            desc:state.desc,
            isActive:state.active,
            userId:context.logindata._id,
            permissions:data
          }
        creation(payload).then((updateRes) => {
            if (updateRes.suc) {
                getRoles().then(response => {
                        if(response.data.suc){
                            loadRoles(response.data.res);
                        }
                })
                dispatch({ type: 'clear', payload: true })
                dispatch({ type: 'enabelUpdate', payload: false })
                dispatch({ type: 'notiMessageShow', payload: true })
                dispatch({ type: 'notiMessage', payload: 'Role Updated Successfully' })
                dispatch({ type: 'notiVarient', payload: 'success' })
                return window.location="/eicu/admin/user"
            }else{
                dispatch({ type: 'notiMessageShow', payload: true })
                dispatch({ type: 'notiMessage', payload: 'Role updation failed' })
                dispatch({ type: 'notiVarient', payload: 'danger' })
                hidemessage();
            }
        });
    }
    }

    const hidemessage=() =>{
        setTimeout(function () {
            dispatch({ type: 'notiMessageShow', payload: false })
            dispatch({ type: 'notiMessage', payload: '' })
            dispatch({ type: 'notiVarient', payload: '' })
        }, 5000);
      }
    
      const columns = [{
        dataField: 'roleName',
        text: 'Role Name',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'status',
        text: 'Status',
        sort: true,
        filter: textFilter()
      },
      
      {
        dataField: 'options',
        text: 'Actions',
        class:'tes'
      }];
    
      const defaultSorted = [{
        dataField: 'id',
        order: 'asc'
      }];

      const clear = () => {
        dispatch({ type: 'clear', payload: true })
        dispatch({ type: 'enabelUpdate', payload: false })
    }
    
    //===================
    // Template Part
    //===================

    return (
        <>
            <div className="container">
                <div className="cardMainlayout pt-0">
                    <div className="contentArea main-wrapper">
                    <Alert className="notification" variant={state.notiVarient} show={state.notiMessageShow}
                    onClose={() => dispatch({ type: "notiMessageShow", payload: false })}
                    dismissible> {state.notiMessage}</Alert>
                        <Form.Row>
                            <Form.Group as={Col} xs={4} controlId="roleName">
                                <Form.Control type="text" placeholder="Enter Office Name" autoComplete="off" value={state.roleName} onChange={handleChange} />
                                <Form.Label>Role Name<sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} className=" text-right" xs={3} controlId="active">
                                <Form.Check
                                    type="switch"
                                    id="active"
                                    label="Status"
                                    checked={state.active} value={state.active} onChange={handleChange}
                                />
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={6} controlId="desc">
                                <Form.Control as="textarea" rows="2" placeholder="Enter Description" value={state.desc} onChange={handleChange} />
                                <Form.Label>Description</Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <b>Functionalities : </b>
                        {permisnJson.permissions.map((permission, idx) => (
                            <React.Fragment key={idx}>
                                {permission.hospital &&
                               <Form.Row className="medicationh">
                                   <div class="col-sm-3"><span><b>Hospital Creation : </b> </span></div>
                                   
                                <Form.Group as={Col} xs={3} controlId="addHosp">
                                <Form.Check
                                    type="checkbox"
                                    id="AH"
                                    custom
                                    label={permission.hospital[0].name}
                                    checked={state.addHosp}
                                    value={state.addHosp} onChange={handleChangePermission(permission.hospital[0])}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="viewHosp">
                                <Form.Check
                                    type="checkbox"
                                    id="VH"
                                    custom
                                    label={permission.hospital[1].name}
                                    checked={state.viewHosp}
                                    value={state.editHosp} onChange={handleChangePermission(permission.hospital[1])}
                                />
                            </Form.Group>
                             <Form.Group as={Col} xs={3} controlId="editHosp">
                                <Form.Check
                                    type="checkbox"
                                    id="EH"
                                    custom
                                    label={permission.hospital[2].name}
                                    checked={state.editHosp}
                                    value={state.editHosp} onChange={handleChangePermission(permission.hospital[2])}
                                />
                            </Form.Group>
                            </Form.Row>
                            }
                            {permission.patient &&
                               <Form.Row className="medicationh">
                                   <div class="col-sm-3"><span><b>Patient Admission :</b></span></div>
                                <Form.Group as={Col} xs={3} controlId="addPatient">
                                <Form.Check
                                    type="checkbox"
                                    id="AP"
                                    custom
                                    label={permission.patient[0].name}
                                    checked={state.addPatient}
                                    value={state.addPatient} onChange={handleChangePermission(permission.patient[0])}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="viewPatient">
                                <Form.Check
                                    type="checkbox"
                                    id="VP"
                                    custom
                                    label={permission.patient[1].name}
                                    checked={state.viewPatient}
                                    value={state.viewPatient} onChange={handleChangePermission(permission.patient[1])}
                                />
                            </Form.Group>
                             <Form.Group as={Col} xs={3} controlId="editPatient">
                                <Form.Check
                                    type="checkbox"
                                    id="EP"
                                    custom
                                    label={permission.patient[2].name}
                                    checked={state.editPatient}
                                    value={state.editPatient} onChange={handleChangePermission(permission.patient[2])}
                             
                                />
                            </Form.Group>
                            </Form.Row>
                            }

                              {permission.lab &&
                               <Form.Row className="medicationh">
                                  <div class="col-sm-3"> <span><b>Lab Data :</b></span></div>
                                <Form.Group as={Col} xs={3} controlId="addLab">
                                <Form.Check
                                    type="checkbox"
                                    id="AL"
                                    label={permission.lab[0].name}
                                    custom
                                    checked={state.addLab}
                                    value={state.addLab} onChange={handleChangePermission(permission.lab[0])}
                             
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="viewLab">
                                <Form.Check
                                    type="checkbox"
                                    id="VL"
                                    label={permission.lab[1].name}
                                    custom
                                    checked={state.viewLab}
                                    value={state.viewLab} onChange={handleChangePermission(permission.lab[1])}
                                />
                            </Form.Group>
                             <Form.Group as={Col} xs={3} controlId="editLab">
                                <Form.Check
                                    type="checkbox"
                                    id="EL"
                                    label={permission.lab[2].name}
                                    custom
                                    checked={state.editLab}
                                    value={state.editLab} onChange={handleChangePermission(permission.lab[2])}
                                />
                            </Form.Group>
                            </Form.Row>
                            } 
                            {permission.drug &&
                               <Form.Row className="medicationh">
                                  <div class="col-sm-3"> <span><b>Drug Chart:</b></span></div>
                                <Form.Group as={Col} xs={3} controlId="addDrug">
                                <Form.Check
                                    type="checkbox"
                                    id="AD"
                                    label={permission.drug[0].name}
                                    custom
                                    checked={state.addDrug}
                                    value={state.addDrug} onChange={handleChangePermission(permission.drug[0])}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="viewDrug">
                                <Form.Check
                                    type="checkbox"
                                    id="VD"
                                    label={permission.drug[1].name}
                                    checked={permission.drug[1].code}
                                    value={permission.drug[1].code} onChange={handleChange}
                                    custom
                                    checked={state.viewDrug}
                                    value={state.viewDrug} onChange={handleChangePermission(permission.drug[1])}
                                />
                            </Form.Group>
                             <Form.Group as={Col} xs={3} controlId="editDrug">
                                <Form.Check
                                    type="checkbox"
                                    id="ED"
                                    label={permission.drug[2].name}
                                    custom
                                    checked={state.editDrug}
                                    value={state.editDrug} onChange={handleChangePermission(permission.drug[2])}
                                />
                            </Form.Group>
                            </Form.Row>
                            } 
                            {permission.criticalCare &&
                               <Form.Row className="medicationh">
                                  <div class="col-sm-3"> <span><b>Critical Care Notes :</b></span></div>
                                <Form.Group as={Col} xs={3} controlId="addCCare">
                                <Form.Check
                                    type="checkbox"
                                    id="ACCN"
                                    label={permission.criticalCare[0].name}
                                    custom
                                    checked={state.addCCare}
                                    value={state.addCCare} onChange={handleChangePermission(permission.criticalCare[0])}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="viewCCare">
                                <Form.Check
                                    type="checkbox"
                                    id="VCCN"
                                    label={permission.criticalCare[1].name}
                                    custom
                                    checked={state.viewCCare}
                                    value={state.viewCCare} onChange={handleChangePermission(permission.criticalCare[1])}

                                />
                            </Form.Group>
                            </Form.Row>
                            } 
                            {permission.user &&
                               <Form.Row className="medicationh">
                                 <div class="col-sm-3">  <span><b>User Management :</b></span></div>
                                <Form.Group as={Col} xs={3} controlId="addUser">
                                <Form.Check
                                    type="checkbox"
                                    id="AU"
                                    label={permission.user[0].name}
                                    custom
                                    checked={state.addUser}
                                    value={state.addUser} onChange={handleChangePermission(permission.user[0])}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="viewUser">
                                <Form.Check
                                    type="checkbox"
                                    id="VU"
                                    label={permission.user[1].name}
                                    custom
                                    checked={state.viewUser}
                                    value={state.viewUser} onChange={handleChangePermission(permission.user[1])}
                                />
                            </Form.Group>
                             <Form.Group as={Col} xs={3} controlId="editUser">
                                <Form.Check
                                    type="checkbox"
                                    id="EU"
                                    label={permission.user[2].name}
                                    custom
                                    checked={state.editUser}
                                    value={state.editUser} onChange={handleChangePermission(permission.user[2])}
                                />
                            </Form.Group>
                            </Form.Row>
                            } 
                            {permission.role &&
                               <Form.Row className="medicationh">
                                   <div class="col-sm-3"> <span><b>Role Management :</b></span></div>
                                <Form.Group as={Col} xs={3} controlId="addRole">
                                <Form.Check
                                    type="checkbox"
                                    id="AR"
                                    label={permission.role[0].name}
                                    custom
                                    checked={state.addRole}
                                    value={state.addRole} onChange={handleChangePermission(permission.role[0])}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="viewRole">
                                <Form.Check
                                    type="checkbox"
                                    id="VR"
                                    label={permission.role[1].name}
                                    custom
                                    checked={state.viewRole}
                                    value={state.viewRole} onChange={handleChangePermission(permission.role[1])}
                                />
                            </Form.Group>
                             <Form.Group as={Col} xs={3} controlId="editRole">
                                <Form.Check
                                    type="checkbox"
                                    id="ER"
                                    label={permission.role[2].name}
                                    custom
                                    checked={state.editRole}
                                    value={state.editRole} onChange={handleChangePermission(permission.role[2])}
                                />
                            </Form.Group>
                            </Form.Row>
                            }
                            {permission.spoc &&
                               <Form.Row className="medicationh">
                                   <div class="col-sm-3"> <span><b>SPOC :</b></span></div>
                                <Form.Group as={Col} xs={3} controlId="userName">
                                <Form.Check
                                    type="checkbox"
                                    id="ASPOC"
                                    label={permission.spoc[0].name}
                                    custom
                                    checked={state.addSpoc}
                                    value={state.addSpoc} onChange={handleChangePermission(permission.spoc[0])}
                                />
                            </Form.Group>
                            <Form.Group as={Col} xs={3} controlId="userName">
                                <Form.Check
                                    type="checkbox"
                                    id="VSPOC"
                                    label={permission.spoc[1].name}
                                    custom
                                    checked={state.viewSpoc}
                                    value={state.viewSpoc} onChange={handleChangePermission(permission.spoc[1])}
                                />
                            </Form.Group>
                            </Form.Row>
                            }
                            </React.Fragment>
                            ))}
                        <Form.Row>
                            
                            
                        </Form.Row>
                        <div className="text-right buttonGrop pb-0">
                            {!state.enabelUpdate && (
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    onClick={createRole}
                                    loading={state.loadOfice}
                                    type="submit"
                                > Save</Button>
                            )}
                            {state.enabelUpdate && (
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    onClick={clear}
                                    loading={state.loadOfice}
                                    type="submit"
                                > Clear</Button>
                            )}
                            {state.enabelUpdate && (
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    onClick={updateUser}
                                    loading={state.loadOfice}
                                    type="submit"
                                > Update</Button>
                            )}

                        </div>
                    </div>
                </div>
                <div className="cardMainlayout p-0">
                    <div className="cardMainlayout p-0">
                               {state.rolesList.length!==0 &&
                                   <BootstrapTable
                                    bootstrap4
                                    keyField="id"
                                    classes ="markup filterTbl"
                                    data={state.rolesList}
                                    columns={ columns }
                                    defaultSorted={ defaultSorted }
                                    filter={ filterFactory()}
                                    pagination={ paginationFactory() }
                                    />
                               }
                               {state.rolesList.length===0 &&
                                <span>No records found</span>
                               }
                            </div>
                </div>
            </div>
        
        </>
    )
}
export default RoleManagement