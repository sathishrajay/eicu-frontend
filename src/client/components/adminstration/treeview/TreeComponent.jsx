import React, { useEffect, useState, useContext } from 'react';
var cities = require('../../masters/Cities.json');

function TreeComponent(props) {
    const [locationData, setLocationData] = useState(props.locationData);

    const fetchCityName = (cityCode) => {
        let cityArray = cities.filter(item => item.ccode === cityCode);
        if(cityArray.legth>0){
            return cityArray[0].cname;
            }else{
              return '';
            }
    }
      
    const fetchStateName = (stateCode) => {
        if(stateCode === "TS") {return "Telangana"};
        if(stateCode === "AP") {return "Andhra Pradesh"};
        return stateCode;
    } 

    return (
        <div class="treeview-colorful w-20 border border-secondary mx-4 my-4">
            {locationData !== undefined && locationData !== null && 
            locationData.map((state, index) =>{
                return(
                        <ul class="treeview-colorful-list mb-3">
                            <li class="treeview-colorful-items">
                                <a class="treeview-colorful-items-header">
                                    <i class="fas fa-plus-circle"></i>
                                    <span><i class="far fa-envelope-open ic-w mx-1"></i>{fetchStateName(state.stateName)}</span>
                                </a>
                                <ul class="nested">
                                    {state.cities.map((city, index) => {
                                    return (
                                    <li class="treeview-colorful-items">
                                    <a class="treeview-colorful-items-header">
                                        <i class="fas fa-plus-circle"></i>
                                        <span><i class="far fa-calendar-alt ic-w mx-1"></i>{fetchCityName(city.cityName)}</span>
                                    </a>
                                    <ul class="nested">
                                     {city.hospitals.map((hosp, index) => {
                                         return(
                                        <li class="treeview-colorful-items">
                                            <a class="treeview-colorful-items-header">
                                                <i class="fas fa-plus-circle"></i>
                                                <span><i class="far fa-calendar-alt ic-w mx-1"></i>{hosp.name}</span>
                                            </a>
                                            <ul class="nested">
                                            {hosp.bed_details.map((bed, index) => {
                                                return(
                                                    <li>
                                                <a href={'/eicu/admin/location/' + hosp.code +'/'+bed.bed_id} class="treeview-colorful-items-header">
                                                    <span><i class="far fa-calendar-alt ic-w mx-1"></i>Bed {bed.bed_id}</span>
                                                </a></li>
                                                )
                                            })}
                                            </ul>
                                        </li>)
                                     })}
                                    </ul>
                                    </li> )}
                                    )}
                            </ul>
                        </li>
                    </ul>
                ) 
            })
        }
        </div>
    )
}

export default TreeComponent
