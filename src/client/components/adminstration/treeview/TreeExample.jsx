import React, { PureComponent } from 'react';
import { Treebeard } from 'react-treebeard';
import PatientInfoService from '../../../services/administration/PatientInfoService';

class TreeExample extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { data: props.locationData };
    this.onToggle = this.onToggle.bind(this);
    const decorators = {
      Container: (props) => {
          return (
              <div style={props.backgroundColor="yellow"}>
              </div>
          );
      }
  };
  }
  

  onToggle(node, toggled) {
    const { cursor, data } = this.state;

    if (cursor) {
      this.setState(() => ({ cursor, active: false }));
    }
    node.active = true;
    if (node.children) {
      node.toggled = toggled;
    }
    this.setState(() => ({ cursor: node, data: Object.assign({}, data) }));
    if (node.isBed !== undefined && node.isBed !== null && node.isBed === true) {
      this.props.reDirectUrl(node);
    }
    
  }

  render() {
    const { data } = this.state;
    return (
      <Treebeard
        style={treeStyle}
        data={data}
        onToggle={this.onToggle}
        decorators={this.decorators}
      />
    );
  }
}
export default TreeExample


const treeStyle = {
  tree: {
    base: {
      listStyle: 'none',
      backgroundColor: 'white',
      margin: 0,
      padding: 0,
      color: 'rgb(35,31,32)',
      fontFamily: '"Helvetica Neue", "Open Sans", Arial, sans-serif',
      fontSize: '1.3rem'
    },
    node: {
      base: {
        position: 'relative'
      },
    link: {
      cursor: 'pointer',
      position: 'relative',
      padding: '0px 5px',
      display: 'block'
    },
    activeLink: {
      background: 'rgb(102 134 175)',
      borderradius: '4px'
    },
    toggle: {
      base: {
        position: 'relative',
        display: 'inline-block',
        verticalAlign: 'top',
        marginLeft: '-5px',
        height: '24px',
        width: '24px'
      },
      wrapper: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        margin: '-7px 0 0 -7px',
        height: '14px'
      },
      height: 14,
      width: 14,
      arrow: {
        fill: 'rgb(35,31,32)',
        strokeWidth: 0
      }
    },
    header: {
      base: {
        display: 'inline-block',
        verticalAlign: 'top',
        color: 'rgb(0,0,128)'
      },
      connector: {
        width: '2px',
        height: '12px',
        borderLeft: 'solid 2px black',
        borderBottom: 'solid 2px black',
        position: 'absolute',
        top: '0px',
        left: '-21px'
      },
      title: {
        lineHeight: '24px',
        verticalAlign: 'middle'
      }
    },
    subtree: {
      listStyle: 'none',
      paddingLeft: '19px'
    },
    loading: {
      color: '#E2C089'
      }
    }
  }
}