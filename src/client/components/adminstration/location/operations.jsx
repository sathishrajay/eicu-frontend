import React from 'react';
import PatientInfoService from '../../../services/administration/PatientInfoService';

/**
 * @description: CRUD operations for Patient
 * @author: Praveen Varma
 * @param {*}
 * @function Branch
 * @date : 13-08-2020
 */


// ========================================================================
//  Branch Create Operation
//  This will create Branch and save the details 
// ========================================================================
export const getPatient = async (obj) => {
    let finalRes=[];
    await PatientInfoService.retrivePatientInfo(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}






