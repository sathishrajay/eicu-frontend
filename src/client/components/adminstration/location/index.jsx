import React, { useContext, useEffect, useState } from 'react';
import { Col, Row, Tab, Tabs } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { LocationContext } from "../../../../App";
import AdmissionForm from '../../adminstration/patientInfo/admissionForm';
import CriticalCareForm from '../../adminstration/patientInfo/criticalCareForm';
import DrugChart from '../../adminstration/patientInfo/drugChart';
import LabData from '../../adminstration/patientInfo/labData';
import SPOC from '../../adminstration/patientInfo/poc';
import TreeExample from '../../adminstration/treeview/TreeExample';
import Breadcrumb from '../../common/breadcrumb/Breadcrumb';
import Footer from '../../common/Footer';
import Header from '../../common/Header';
import InnerLoader from '../../common/InnerLoader';
import MainMenu from '../../common/MainMenu';
import { getPatient} from './operations';
import { Context} from "../../../../App";
import LocationService from '../../../services/administration/LoationService';
import VitalGraph from '../../../components/adminstration/patientInfo/vital/VitalGraph';

function Location(props) {
  const history = useHistory();
  const [locationData, setLocationData] = useState();
  const [enableTree, setEnableTree] = useState(false);
  const [patientInfo, setPatientInfo] = useState();
  const [data, setData] = useState();
  const [enablePatient, setEnablePatient] = useState(false);
  const [labSuc, setLabSuc] = useState(false);
  const [pData, setPdata] = useState();
  const [defaultBedInfo, setDefaultBedInfo] = useState()
  const [key, setKey] = useState('Admission Document');
  const [isLoadingMore, setIsLoadingMore,] = useState(false);
  const [locationContext, setLocationContext] = useContext(LocationContext);
  const [context, setContext] = useContext(Context);
  const [patientSuc, setPatientSuc] = useState(false);
  const [drugSuc, setDrugSuc] = useState(false);
  const [careSuc, setCareSuc] = useState(false);
  const [spocSuc, setSpocSuc] = useState(false);

  const[labEnbl, setLabEnbl] = useState(false);
  const[drugEnbl, setDrugEnbl] = useState(false);
  const[careEnbl, setCareEnbl] = useState(false);
  const[spocEnbl, setSpocEnbl] = useState(false);
  const[vitalEnbl, setVitalEnbl] = useState(false);

  useEffect(() => {
    setIsLoadingMore(true);
    if(locationContext.locations !== undefined && locationContext.locations!==""){
      let payload = LocationService.constructJsonDataForTree(locationContext.locations);
      let cityState = getStateAndCityForHosp(payload);
      let finalData = childData(payload, cityState);
      JSON.stringify("== Location level structure  ==== "+JSON.stringify(finalData));
      setLocationData(finalData);
      setEnableTree(true);
      setLocationContext(locationContext.locations);
    }
  }, [locationContext.locations === ""]);


  const getStateAndCityForHosp = (payload) => {
    let cityState={};
    let statesArry = payload.children;
    for (let s in statesArry) {
      let stateData = statesArry[s];
      let cityArry = stateData.children;
      for (let c in cityArry) {
        let cityData = cityArry[c];
        let hspArray = cityData.children;
        for (let h in hspArray) {
          if(hspArray[h].hcode === props.match.params.hspCode){
            cityState.city=cityData.name;
            cityState.state=statesArry[s].name
            return cityState;
          }
        }
      }
    }
    return cityState;
  }


  const childData = (dataFinal, cityState) => {
    let hspCode = props.match.params.hspCode;
    let city = cityState.city;
    let state = cityState.state;
    let selData = {};

    let statesArry = dataFinal.children;
    for (let s in statesArry) {
      let stateData = statesArry[s];
      if (stateData.name === state) {
        stateData.active = true;
        stateData.toggled = true;
        let cityArry = stateData.children;
        for (let c in cityArry) {
          let cityData = cityArry[c];
          if (cityData.name === city) {
            cityData.active = true;
            cityData.toggled = true;

            let hspArray = cityData.children;
            for (let h in hspArray) {
              let hsp = hspArray[h];
              if (hsp.hcode === hspCode) {
                hsp.active = true;
                hsp.toggled = true;
                let bedList = hsp.children;

                let bedIdProps =props.match.params.bedNum;
                let selectedBed = bedList.filter((item) => item.bed_id === Number(bedIdProps));
                if(selectedBed !== null && selectedBed.length > 0){
                  let bedStatus = selectedBed[0].status;
                  if(bedStatus === 'occupy'){
                    for(let b in hsp.children){
                      if(Number(hsp.children[b].bed_id) === Number(props.match.params.bedNum)){
                        hsp.children[b].active=true;
                      }else{
                        hsp.children[b].active=false;
                      }
                    }
                    let bedReqJson = {
                      hid:selectedBed[0].hospital_id,
                      bed_no: props.match.params.bedNum
                    }
                    
                    let defaultBed= {
                      bed_id: props.match.params.bedNum,
                      hospital_id: selectedBed[0].hospital_id,
                      hcode: hsp.hcode,
                      email:hsp.email !== undefined ? hsp.email : "",
                      status: selectedBed[0].status,
                      hospName:hsp.name,
                      hospEmail:hsp.email,
                      active: true
                    }
                    
                    setDefaultBedInfo(defaultBed);
                   
                    getPatient(bedReqJson).then((resp) => {
                      
                      if (resp.suc) {
                        setPatientInfo(resp.res);
                        if(resp.res!==undefined && resp.res!==null){
                        let drugDsc="";
                        if(resp.res.drugReactions){
                          drugDsc = resp.res.drugDesc;
                        }
                        const payload = {
                          "name": resp.res.name,
                          "age": resp.res.age,
                          "mrNum": resp.res.mrNum,
                          "sex": resp.res.sex,
                          "patientId": resp.res._id,
                          "email":hsp.email !== undefined ? hsp.email : "",
                          "drugReactions":drugDsc
                        }
                        setPdata(payload);
                        setIsLoadingMore(false);
                      }
                      }else{
                        setIsLoadingMore(false);
                      }
                      });
                  }else{
                    for(let bed in bedList){
                      if(Number(bedList[bed].bed_id) === Number(props.match.params.bedNum)){
                        bedList[bed].active =true;
                        let defaultBed= {
                          bed_id: props.match.params.bedNum,
                          hospital_id: bedList[bed].hospital_id,
                          hcode: hsp.hcode,
                          status: bedList[bed].status,
                          hospName:hsp.name,
                          email:hsp.email !== undefined ? hsp.email : "",
                          name: (bedList[bed].name !== null && bedList[bed].name !== '') ? ("Bed "+bedList[bed].bed_id +" ("+bedList[bed].name+")") : ("Bed "+bedList[bed].bed_id+ " ("+ bedList[bed].status+")"),
                          active: true
                        }
                        setDefaultBedInfo(defaultBed);
                        setIsLoadingMore(false);
                      }else{
                        bedList[bed].active =false;
                        setIsLoadingMore(false);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    let finald = {
      name: "Locations",
      toggled: true,
      active: true,
      styleCls : "locationLevel",
      children: statesArry
    }
    JSON.stringify("== Location tree structure ==== "+JSON.stringify(dataFinal));
    return finald;
  }

  const hospitalInfo =(data) =>{
   setData(data);
   setEnablePatient(true)
  }

  const patientData =(data) =>{
    setPatientSuc(false);
    setPdata(data);
    setPatientSuc(true);
    if(context.logindata.permissions!==undefined && context.logindata.permissions.includes("VL")){
      setKey("Lab Data");
      setLabEnbl(true);
      setDrugEnbl(false);
      setCareEnbl(false);
      setSpocSuc(false)
    }else{
      setKey("spoc");
      setSpocSuc(true)
      setDrugEnbl(false);
      setCareEnbl(false);
      setLabEnbl(false);
    }
   }

   const addmissionDoc =(data) =>{
    setPdata(data);
    setKey("Admission Document");
   }


   const popultePatientData =(data) =>{
    setData(data);
    setEnablePatient(true)
   } 

   const loadDrugChart =(obj) =>{
    setSpocEnbl(false)
    if(context.logindata.permissions!==undefined && context.logindata.permissions.includes("VD")){
      setKey("Drug Chart");
      setLabEnbl(false);
      setDrugEnbl(true);
      setCareEnbl(false);
      setSpocSuc(false)
      setLabSuc(true)
    }else{
      setKey("Critical Care Notes");
      setLabEnbl(false);
      setDrugEnbl(false);
      setCareEnbl(true);
      setSpocSuc(false)
      setLabSuc(false)
    }
    setSpocEnbl(true)
   }

   const loadSpoc =(obj) =>{
    setSpocEnbl(false);
    if(context.logindata.permissions!==undefined && context.logindata.permissions.includes("VSPOC")){
      setKey("spoc");
      setSpocEnbl(true)
      setLabEnbl(false);
      setDrugEnbl(false);
      setCareEnbl(false);
      setSpocSuc(true)
    }else{
      setKey("Drug Chart");
      setSpocEnbl(false)
      setLabEnbl(false);
      setDrugEnbl(true);
      setCareEnbl(false);
      setSpocSuc(false)
    }
    setSpocEnbl(true);
   }

   const loadCriticalCare =(data) =>{
    setDrugSuc(false);
    if(context.logindata.permissions!==undefined && context.logindata.permissions.includes("VCCN")){
       setKey("Critical Care Notes");
        setLabEnbl(false);
        setDrugEnbl(false);
        setCareEnbl(true);
    }
    setDrugSuc(true);
   }


   const reDirectUrl =(data) =>{
    //history.push("/eicu/admin/location/"+data.hcode+"/"+data.bed_id);
    window.location = "/eicu/admin/location/"+data.hcode+"/"+data.bed_id;
  }

 


 
  const onDemand = (k) => {
    setKey(k);
    if(k === "Lab Data"){
      setLabEnbl(true);
      setDrugEnbl(false);
      setCareEnbl(false);
      setVitalEnbl(false);
      setSpocEnbl(false)
    }else if(k === "Drug Chart"){
      setDrugEnbl(true);
      setLabEnbl(false);
      setCareEnbl(false);
      setVitalEnbl(false);
      setSpocEnbl(false)
    }else if(k === "Critical Care Notes"){
      setCareEnbl(true)
      setDrugEnbl(false);
      setLabEnbl(false);
      setVitalEnbl(false);
      setSpocEnbl(false)
    }else if(k === "Admission Document"){
      setCareEnbl(false)
      setDrugEnbl(false);
      setLabEnbl(false);
      setVitalEnbl(false);
      setSpocEnbl(false)
    }else if(k === "spoc"){
      setCareEnbl(false)
      setDrugEnbl(false);
      setLabEnbl(false);
      setSpocEnbl(true)
    }else if(k === "View Vital Data"){
      //getPatientData();
      //window.open("/eicu/stasis/"+defaultBedInfo.hospital_id);
      setVitalEnbl(true);
      setCareEnbl(false);
      setDrugEnbl(false);
      setLabEnbl(false); 
      setSpocEnbl(false)
    }
  }


  return (
    <>
      <Header />
      <MainMenu active='Location' />
      <Breadcrumb activePage="Locations" > </Breadcrumb>
      <div className="container">
      {isLoadingMore ?  <InnerLoader/>:""}
        <div className="cardMainlayout main-wrapper">
        {enableTree === true && 
          <Row>
            <Col xs="3">
              <TreeExample locationData={locationData} sendData={hospitalInfo} sendPatientInfo={popultePatientData}
              sendPData ={addmissionDoc}  reDirectUrl={reDirectUrl}/>
            </Col>
            <Col xs="9 border-left">
              <Tabs variant="pills" activeKey={key}
              onSelect={(k) => onDemand(k)}
              className="flex-row"  id="uncontrolled-tab-example">
                {context.logindata.permissions!==undefined && context.logindata.permissions.includes("VP") &&
                <Tab eventKey="Admission Document" title="Admission Document">
                  <AdmissionForm sendHospInfo={defaultBedInfo} patientInfo={patientInfo} sendPatientInfo={patientData}/>
                </Tab>
                 }
                 {context.logindata.permissions!==undefined && context.logindata.permissions.includes("VL") &&  
                <Tab disabled={pData===undefined} eventKey="Lab Data" title="Lab Data">
                  {labEnbl && <LabData  patientData={pData} sendLab={loadSpoc} showMsg={patientSuc} />
                  }
                </Tab>
                 }
                 {context.logindata.permissions!==undefined && context.logindata.permissions.includes("VSPOC") && 
                <Tab disabled={pData===undefined} eventKey="spoc" title="SPOC">
                  {spocEnbl && <SPOC patientData={pData} showMsg={spocSuc} sendSpoc={loadDrugChart}/>}
                </Tab>
                }
                 {context.logindata.permissions!==undefined && context.logindata.permissions.includes("VD") && 
                <Tab disabled={pData===undefined} eventKey="Drug Chart" title="Drug Chart">
                 {drugEnbl &&<DrugChart showMsg={labSuc} patientData={pData} sendDrug={loadCriticalCare} />}
                </Tab>
                 }
                 {context.logindata.permissions!==undefined && context.logindata.permissions.includes("VCCN") && 
                <Tab disabled={pData===undefined} eventKey="Critical Care Notes" title="Critical Care Notes">
                  {careEnbl && <CriticalCareForm patientData={pData} showMsg={drugSuc}/>}
                </Tab>
                }
                
                {/* <Tab disabled={pData===undefined} eventKey="View Vital Data" title="View Vital Data">
                  {vitalEnbl && <VitalGraph patientData={pData}/>}
                </Tab> */}
              </Tabs>
            </Col>
          </Row>
          }
        </div>
      </div>
      <Footer />
    </>
  )
}

export default Location
