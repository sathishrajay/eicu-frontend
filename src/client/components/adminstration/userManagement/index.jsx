import React, { useEffect, useReducer, useState , useContext } from 'react';
import { Col, Row, Tab, Tabs } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import Button from '../../common/buttons/Button';
import Footer from '../../common/Footer';
import Header from '../../common/Header';
import Icon from '../../common/icomoonIcon/Icon';
import MainMenu from '../../common/MainMenu';
import { creation, retriveHospitals, update } from './operations';
import Role from '../roleManagement';
import User from './CreateUser';
import { Context} from "../../../../App";


/**
 * @description:This function will creaet Hospitales and this will provide the ADD,EDIT and Remove functionalities
 * @author: Lakshmi
 * @param {*}
 * @function Hospital
 * @date : 13-08-2020
 */


function UserManagement() {
    const [context, setContext] = useContext(Context);
    const [key, setKey] = useState('Role');
    
    //===================
    // Template Part
    //===================

    return (
        <>
       <Header />
            <MainMenu active='User Management' />
            <div className="container">
                <div className="cardMainlayout pt-0">
                    <div className="contentArea main-wrapper">
            <Col xs="9 border-left">
              <Tabs variant="pills" activeKey={key}
              onSelect={(k) => setKey(k)}
              className="flex-row"  id="uncontrolled-tab-example">
                <Tab eventKey="Role" title="Create Role">
                  <Role />
                </Tab>
                {context.logindata.permissions!==undefined && context.logindata.permissions.includes("VU") &&
                <Tab eventKey="User" title="Create User">
                  <User />
                </Tab>
                 }
                </Tabs>
                </Col>
              
                </div>
            </div>
            </div>
            <Footer />
        </>
    )
}
export default UserManagement