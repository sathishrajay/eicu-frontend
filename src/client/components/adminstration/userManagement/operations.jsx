import React from 'react';
import UserRoleManagementService from '../../../services/administration/UserRoleManagementService';

/**
 * @description: CRUD operations for Hospital
 * @author: Lakshmi
 * @param {*}
 * @function User
 * @date : 13-08-2020
 */



// ========================================================================
//  Branch Create Operation
//  This will create Branch and save the details 
// ========================================================================
export const creation = async (obj) => {
    let finalRes=[];
    await UserRoleManagementService.saveUser(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}


// ========================================================================
//  Branch Update Operation
//  This function will call the service to delete the selected branch
// ========================================================================
export const update = async (obj) => {
    let finalRes = {};
    await UserRoleManagementService.saveUser(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

// ========================================================================
// All Branch Data List
//  This function update the state values while clicking on Edit
// ========================================================================
export const getUsers = async () => {
    let users = [];
    await UserRoleManagementService.getUserList().then(response => {
        if (response.data.suc) {
            users = response;
        }
    });
    return Promise.resolve(users);
}


// ========================================================================
// All Branch Data List
//  This function update the state values while clicking on Edit
// ========================================================================
export const getRoleMenu = async () => {
    let roles = [];
    await UserRoleManagementService.getRoleMenus().then(response => {
        roles = response;
    });
    return Promise.resolve(roles);
}





