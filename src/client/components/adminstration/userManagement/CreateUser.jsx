import React, { useEffect, useReducer, useState , useContext } from 'react';
import { Col, Form, Table, Alert } from "react-bootstrap";
import { useHistory } from 'react-router-dom';
import Button from '../../common/buttons/Button';
import Footer from '../../common/Footer';
import Header from '../../common/Header';
import Icon from '../../common/icomoonIcon/Icon';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { creation, update, getUsers, getRoleMenu } from './operations';
import { Context} from "../../../../App";
import Select from 'react-dropdown-select';
import LocationService from '../../../services/administration/LoationService';
import { LocationContext } from "../../../../App";
var cities = require('../../masters/Cities.json');

/**
 * @description:This function will creaet Hospitales and this will provide the ADD,EDIT and Remove functionalities
 * @author: Lakshmi
 * @param {*}
 * @function Hospital
 * @date : 13-08-2020
 */

/**
 * Initial State Declaration
 */
const initialState = {
    _id: undefined,userName:"",isActive:true,firstName:"",lastName:"",email:"",userRegdNum:"",roleId:0,contactNum:"",usrid:"",notiMessage:"",notiMessageShow:false,notiVarient:"",rolesMenu:[],sta:"",cty:"",hospitals:[]
};
let finalList=[];
//=====================================
// Reducer function for userReact Hook
// which will update the state
//=====================================
const reducer = (state, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...state,_id: undefined,userName:"",isActive:true,firstName:"",lastName:"",email:"",userRegdNum:"",roleId:0,contactNum:"",notiMessage:"",notiMessageShow:false,notiVarient:"",sta:"",cty:"",hospitals:[],citiesMenu:[]
            };
        case 'editHospital':
            return Object.assign(state, action.payload);
        default:
            return { ...state, [action.type]: action.payload };
    }
};

function CreateUser(props) {
    const [state, dispatch] = useReducer(reducer, initialState);
    const [context, setContext] = useContext(Context);
    const history = useHistory();
    const [response, setResponse] =useState({userList:[]})
    const [menuResp, setmenuResp] =useState([])
    const [enabelUpdate , setEnabelUpdate] = useState(false)
    const [value,setValue] = useState()
    const [locationContext, setLocationContext] = useContext(LocationContext);
    const [respData, setRespData] =useState([])
    const [data , setData] = useState([])
    const [city , setCity] = useState()


    useEffect(() => {
        LocationService.fetchMenus().then((resp) => {
            if (resp != undefined && resp.data !== undefined && resp.data.res !== undefined && resp.data.res !== null ) {
                //setLocationContext({locations:resp.data.res})
                //console.log("zzzzzzzzzzzzzzzzzzzzzzz"+JSON.stringify(resp.data.res));
                setRespData(resp.data.res)
            }else{
                //setLocationContext()
            }
          });
        let menuRespons=[];
        dispatch({ type: "citiesList", payload: cities })
        dispatch({ type: 'isActive', payload: true })
           getUsers().then(response => {
                if(response.data.suc){
                    getRoleMenu().then(menuRes => {
                        if(menuRes.data.suc){
                         setmenuResp(menuRes.data.res);
                         menuRespons.push(menuRes.data.res);
                        let rolesMenuList = menuRes.data.res.map((role) =>
                        <option value={role._id}>{role.roleName}</option>
                        );
                        dispatch({ type: "rolesMenu", payload: rolesMenuList })
                        }
                        loadUsers(response.data.res,menuRes.data.res);
                       })
                      
                }
        })
    }, []);

    
    
    const loadUsers=(response,menuResponse)=>{
        let data=[];
        for(let val of response){
            var roleName="";
        if(menuResponse && menuResponse!==undefined){
            let roles=menuResponse.filter((s, sidx) => s._id === val.roleId);
            if(roles[0]!==undefined && roles[0].roleName!==undefined){
                roleName=roles[0].roleName;
            }
        }
        data.push({uname: val.userName,name:val.firstName+" "+val.lastName,regdNumber:(val.userRegdNum!==undefined)?val.userRegdNum:"", rName:roleName, status:(val.isActive)?"Active":"Inactive", options: <>{context.logindata.permissions!==undefined && context.logindata.permissions.includes("EU") && <a onClick={editUser(val)}><Icon className="edit ml-1 mr-1" color="#475F7B" size={15} icon="edit" /></a>}</> })
        }
        setResponse({userList:data})
    }

    // OnChange event we will call this and update the state
    const handleChange = (e) => {
        const { id, value } = e.target;
            dispatch({ type: id, payload: value });
            if (e.target.id === "isActive") {
                if (state.isActive) {
                  dispatch({ type: 'isActive', payload: false })
                } else {
                  dispatch({ type: 'isActive', payload: true })
                }
              }
    }

    const validateUser=()=>{
        if(state.userName===""){
        dispatch({ type: 'notiMessageShow', payload: true })
        dispatch({ type: 'notiMessage', payload: 'Please enter User name' })
        dispatch({ type: 'notiVarient', payload: 'danger' })
        window.scrollTo(0, 0);
        hidemessage();
        return false;
        }
        if(state.firstName===""){
        dispatch({ type: 'notiMessageShow', payload: true })
        dispatch({ type: 'notiMessage', payload: 'Please enter First name' })
        dispatch({ type: 'notiVarient', payload: 'danger' })
        window.scrollTo(0, 0);
        hidemessage();
        return false;
        }
        if(state.lastName===""){
        dispatch({ type: 'notiMessageShow', payload: true })
        dispatch({ type: 'notiMessage', payload: 'Please enter Last name' })
        dispatch({ type: 'notiVarient', payload: 'danger' })
        window.scrollTo(0, 0);
        hidemessage();
        return false;
        }
        if(state.email===""){
        dispatch({ type: 'notiMessageShow', payload: true })
        dispatch({ type: 'notiMessage', payload: 'Please enter Email Id'})
        dispatch({ type: 'notiVarient', payload: 'danger' })
        window.scrollTo(0, 0);
        hidemessage();
        return false;
        }
        if(state.contactNum===""){
        dispatch({ type: 'notiMessageShow', payload: true })
        dispatch({ type: 'notiMessage', payload: 'Please enter Contact'})
        dispatch({ type: 'notiVarient', payload: 'danger' })
        window.scrollTo(0, 0);
        hidemessage();
        return false;
        }
        if(state.roleId===0){
        dispatch({ type: 'notiMessageShow', payload: true })
        dispatch({ type: 'notiMessage', payload: 'Please select Role' })
        dispatch({ type: 'notiVarient', payload: 'danger' })
        window.scrollTo(0, 0);
        hidemessage();
        return false;
        }
        if(state.sta===""){
        dispatch({ type: 'notiMessageShow', payload: true })
        dispatch({ type: 'notiMessage', payload: 'Please select State' })
        dispatch({ type: 'notiVarient', payload: 'danger' })
        window.scrollTo(0, 0);
        hidemessage();
        return false;
        }
        if(city===undefined){
        dispatch({ type: 'notiMessageShow', payload: true })
        dispatch({ type: 'notiMessage', payload: 'Please select City' })
        dispatch({ type: 'notiVarient', payload: 'danger' })
        window.scrollTo(0, 0);
        hidemessage();
        return false;
        }
        if(data.length===0){
        dispatch({ type: 'notiMessageShow', payload: true })
        dispatch({ type: 'notiMessage', payload: 'Please select Hospital' })
        dispatch({ type: 'notiVarient', payload: 'danger' })
        window.scrollTo(0, 0);
        hidemessage();
        return false;
        }
        return true;
        }

    //Add User
    const createUser = () => {
        if(validateUser()){
            state.usrid=context.logindata._id;
            let hospList=[];
            if(data.length!==0){
                for(let val of data){
                    hospList.push(val.value);
                }
                state.hosptIds=hospList;
            }
        creation(state).then((resp) => {
            if (resp.suc) {
                getUsers().then(response => {
                    if(response.data.suc){
                    loadUsers(response.data.res);
                    }
                })
                dispatch({ type: 'clear', payload: true })
                dispatch({ type: 'notiMessageShow', payload: true })
                dispatch({ type: 'notiMessage', payload: 'User Created successfully' })
                dispatch({ type: 'notiVarient', payload: 'success' })
                setData([])
                setCity([])
            }else{
                dispatch({ type: 'notiMessageShow', payload: true })
                dispatch({ type: 'notiMessage', payload: 'User Creattion Failed' })
                dispatch({ type: 'notiVarient', payload: 'danger' })
            }
            window.scrollTo(0, 0);
            hidemessage();
        });
    }
    }
    //Update Hospital
    const updateUser = () => {
        if(validateUser()){
        let hospList=[];
        if(data.length!==0){
            for(let val of data){
                hospList.push(val.value);
            }
        }
        const payload={
            _id:state._id,
            userName:state.userName,
            firstName:state.firstName,
            lastName:state.lastName,
            isActive:state.isActive,
            email:state.email,
            contactNum:state.contactNum,
            userRegdNum:state.userRegdNum,
            usrid:context.logindata._id,
            roleId:state.roleId,
            hosptIds:hospList
        }
        creation(payload).then((response) => {
            if (response.suc) {
                getUsers().then(response => {
                    if(response.data.suc){
                    loadUsers(response.data.res,menuResp);
                    }
                })
                dispatch({ type: 'clear', payload: true })
                dispatch({ type: 'enabelUpdate', payload: false })
                dispatch({ type: 'notiMessageShow', payload: true })
                dispatch({ type: 'notiMessage', payload: 'User updated successfully' })
                dispatch({ type: 'notiVarient', payload: 'success' })
                setData([])
                setCity([])
                setValue([])
                setCity([])
            }else{
                dispatch({ type: 'notiMessageShow', payload: true })
                dispatch({ type: 'notiMessage', payload: 'User Not updated.' })
                dispatch({ type: 'notiVarient', payload: 'danger' })
            }
            window.scrollTo(0, 0);
            hidemessage();
        });
    }
    }
    
    // Edit Hospital
        const editUser = (obj) => () => {
        dispatch({ type: '_id', payload: obj._id })
        dispatch({ type: 'isActive', payload: obj.isActive })
        dispatch({ type: 'userName', payload: obj.userName })
        dispatch({ type: 'firstName', payload: obj.firstName })
        dispatch({ type: 'lastName', payload: obj.lastName })
        dispatch({ type: 'email', payload: obj.email })
        dispatch({ type: 'userRegdNum', payload: obj.userRegdNum })
        dispatch({ type: 'contactNum', payload: obj.contactNum })
        dispatch({ type: 'roleId', payload: obj.roleId })
        dispatch({ type: 'enabelUpdate', payload: true })
        if(obj.hosptIds!==undefined &&  obj.hosptIds.length!==0){
        let hosps=[];
        let ctylist=[];
        var state="";
        var city="";
        let hospitalsList=[];
        LocationService.fetchMenus().then((resp) => {
            if (resp != undefined && resp.data !== undefined && resp.data.res !== undefined && resp.data.res !== null ) {
                for(let st of resp.data.res){
                    for(let city of st.cities){
                     city.hospitals.forEach((b1) => obj.hosptIds.forEach((b2)=>{
                        if(b1.id===b2){
                        const payload={
                            value:b1.id,
                            label:b1.name
                        }
                        hosps.push(payload);
                        hospitalsList.push(city.hospitals);
                        let cityList=cities.filter((s, sidx) => s.ccode===city.cityName)
                         const data={
                            value:cityList[0].ccode,
                            label:cityList[0].cname
                         }
                         ctylist.push(data);
                         state=st.stateName;
                         dispatch({ type: 'sta', payload: st.stateName })
                        } 
                     }
                     ));
                     
                     let cList=getUniqData(ctylist);
                     setCity(cList)
                     setValue(hosps)
                 }
                 }
                 let list=[];
                 if(hospitalsList.length!==0){
                    for(let val of hospitalsList[0]){
                          const payload={
                              value:val.id,
                              label:val.name
                          }
                          list.push(payload);
                    }
                 }
                 dispatch({ type: "hospitals", payload: list })
                 let citiesList=[];
                 let cityList=cities.filter((s, sidx) => s.sta===state)
                        for(let val of cityList){
                            const payload={
                                value:val.ccode,
                                label:val.cname
                            }
                            citiesList.push(payload);
                        }
                        dispatch({ type: "citiesMenu", payload: citiesList })
            }
          });
        
        }
        window.scrollTo(0, 0);
    }

    const getUniqData=(jsonarray)=>{
        console.log('getUniqData Entered----')
        let myArrSerialized = jsonarray.map(e => JSON.stringify(e));
        const mySetSerialized = new Set(myArrSerialized);
        const myUniqueArrSerialized = [...mySetSerialized];
        const myUniqueArr = myUniqueArrSerialized.map(e => JSON.parse(e));
        return myUniqueArr;
        }

    const handleRoleChange = (e) => {

            const { id, value } = e.target;
            dispatch({ type: id, payload: value });
      }

      const hidemessage=() =>{
        setTimeout(function () {
            dispatch({ type: 'notiMessageShow', payload: false })
            dispatch({ type: 'notiMessage', payload: '' })
            dispatch({ type: 'notiVarient', payload: '' })
        }, 5000);
      }
     
      const columns = [{
        dataField: 'uname',
        text: 'User Name',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'name',
        text: 'Name',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'regdNumber',
        text: 'Regd. Number',
        sort: false,
        filter:false
      },
      {
        dataField: 'rName',
        text: 'Role',
        sort: false,
        filter:textFilter()
      },
      {
        dataField: 'status',
        text: 'Status',
        sort: false,
        filter: ""
      },
      
      {
        dataField: 'options',
        text: 'Actions',
        class:'tes'
      }];
    
      const defaultSorted = [{
        dataField: 'id',
        order: 'asc'
      }];

      const clear = () => {
        dispatch({ type: 'clear', payload: true })
        dispatch({ type: 'enabelUpdate', payload: false })
        setData([])
        setCity([])
        setValue()
    }

    const handleStaChange = (e) => {
        const { id, value } = e.target;
        let cities=[];
        let cityList=state.citiesList.filter((s, sidx) => s.sta===value)
         for(let val of cityList){
             const payload={
                 value:val.ccode,
                 label:val.cname
             }
             cities.push(payload);
         }
        dispatch({ type: "citiesMenu", payload: cities })
        dispatch({ type: id, payload: value });
  }

  const resetState=()=>{
    dispatch({ type: 'clear', payload: true })
    
  }

  const pickData=(data)=>{
    setData(data)
  }


  const pickCity=(data)=>{
    let hospList=[];
    dispatch({ type: 'hospitals', payload: [] })
    let stateData=respData.filter((s, sidx) => s.stateName===state.sta);
    if(data!==undefined &&  data.length!==0){
        for(let val of data){
        let cityData=stateData[0].cities.filter((s, sidx) => s.cityName===val.value);
        if(cityData!==undefined){
           for(let val of cityData){
              for(let city of val.hospitals){
                  const payload ={
                      value:city.id,
                      label:city.name
                  }
               hospList.push(payload);
              }
           }
        }
    }
    }
     //finalList.push(state.hospitals)
     dispatch({ type: 'hospitals', payload: hospList })
     setCity(data)
     setData([])
  }
  
    
    //===================
    // Template Part
    //===================

    return (
        <>
            <div className="container">
                <div className="cardMainlayout pt-0">
                    <div className="contentArea">
                    <Alert className="notification" variant={state.notiVarient} show={state.notiMessageShow}
                onClose={() => dispatch({ type: "notiMessageShow", payload: false })}
                dismissible> {state.notiMessage}</Alert>
                        <Form.Row>
                            <Form.Group as={Col} xs={4} controlId="userName">
                                <Form.Control type="text" placeholder="Enter Office Name" autoComplete="off" value={state.userName} onChange={handleChange} />
                                <Form.Label>User Name<sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} xs={4} controlId="userRegdNum">
                                <Form.Control type="text" placeholder="Enter Office Name" autoComplete="off" value={state.userRegdNum} onChange={handleChange} />
                                <Form.Label>Registration Number<sup>*</sup></Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} className=" text-right" xs={3} controlId="isActive">
                                <Form.Check
                                    type="switch"
                                    id="isActive"
                                    label="Status"
                                    checked={state.isActive} value={state.isActive} onChange={handleChange}
                                />
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} xs={4} controlId="firstName">
                                <Form.Control type="text" placeholder="Enter First name" value={state.firstName} onChange={handleChange} />
                                <Form.Label>First Name <sup>*</sup></Form.Label>
                            </Form.Group>
            
                            <Form.Group as={Col} xs={4} controlId="lastName">
                                <Form.Control type="text" placeholder="Enter Last name" value={state.lastName} onChange={handleChange} />
                                <Form.Label>Last Name <sup>*</sup></Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                        <Form.Group as={Col} xs={4} controlId="email">
                                <Form.Control type="text" placeholder="Enter Email" value={state.email} onChange={handleChange} />
                                <Form.Label>Email<sup>*</sup></Form.Label>
                                <span>Note : Password will be send through Email Id</span>
                            </Form.Group>
                            <Form.Group as={Col} xs={4} controlId="contactNum">
                                <Form.Control type="text" placeholder="Enter Number" value={state.contactNum} onChange={handleChange} />
                                <Form.Label>Contact Number<sup>*</sup></Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                        <Form.Group as={Col} xs={4} controlId="roleId">
                                <Form.Control as="select" value={state.roleId} onChange={handleRoleChange}>
                                   <option value="">Select Role</option>
                                    {state.rolesMenu}
                                </Form.Control>
                                <Form.Label>Roles <sup>*</sup></Form.Label>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                        <Form.Group as={Col} xs={4} controlId="sta">
                                <Form.Control as="select" value={state.sta} onChange={handleStaChange}>
                                    <option value="">Select State</option>
                                    <option value="TS">Telanagana</option>
                                    <option value="AP">Andhra Pradesh</option>
                                </Form.Control>
                                <Form.Label>State <sup>*</sup></Form.Label>
                            </Form.Group>
                            </Form.Row>
                            <Form.Row>
                            <Form.Group
                                as={Col}
                                xs={4}
                                className="selectplugin"
                                controlId="selectedValue"
                                >
                                <Select
                                multi
                                options={state.citiesMenu}
                                values={city}
                                onChange={(value) => (setCity(value), pickCity(value))}
                                />
                                <Form.Label>Cities<sup>*</sup></Form.Label>
                                </Form.Group>
                            
                            </Form.Row>
                            <Form.Row>
                            <Form.Group
                                as={Col}
                                xs={4}
                                className="selectplugin"
                                controlId="selectedValue"
                                >
                                <Select
                                multi
                                options={state.hospitals}
                                values={value}
                                onChange={(value) => (setValue(value), pickData(value))}
                                />
                                <Form.Label>Hospitals<sup>*</sup></Form.Label>
                                </Form.Group>
                        </Form.Row>
                        <div className="text-right buttonGrop pb-0">
                            {!state.enabelUpdate && context.logindata.permissions!==undefined && context.logindata.permissions.includes("AU") && (
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    onClick={createUser}
                                    loading={state.loadOfice}
                                    type="submit"
                                > Save</Button>
                            )}
                            {state.enabelUpdate && (
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    onClick={clear}
                                    loading={state.loadOfice}
                                    type="submit"
                                > Clear</Button>
                            )}
                            {state.enabelUpdate && (
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    onClick={updateUser}
                                    loading={state.loadOfice}
                                    type="submit"
                                > Update</Button>
                            )}

                        </div>
                    </div>
                </div>
                <div className="cardMainlayout p-0">
                    <div className="cardMainlayout p-0">
                               {response.userList.length!==0 &&
                                   <BootstrapTable
                                    bootstrap4
                                    keyField="id"
                                    classes ="markup filterTbl"
                                    data={response.userList}
                                    columns={ columns }
                                    defaultSorted={ defaultSorted }
                                    filter={ filterFactory()}
                                    pagination={ paginationFactory() }
                                    />
                               }
                               {response.userList.length===0 &&
                                <span>No records found</span>
                               }
                            </div>
                </div>
            </div>
        
        </>
    )
}
export default CreateUser