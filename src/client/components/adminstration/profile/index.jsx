import React, { useEffect, useState, useReducer } from 'react';
import { Modal, Row, Col, Alert, Form } from "react-bootstrap";
import {retriveProfile } from './operations';
import {creation} from '../userManagement/operations';
import Button from '../../common/buttons/Button';
export const USER_AUTH_DATA = "userAuthData";

const initialState = {
    "_id":"",  "firstName": "","lastName": "", "mobile": "","email": ""
 };

const reducer = (state, action) => {
    switch (action.type) {
        case 'clear':
            return {
                ...state
            };
        default:
            return { ...state, [action.type]: action.payload };
    }
};

function Profile(props) {
    const [show, setShow] = useState(props.openPopup);
    const [loggedInData, setLoggedInData] = useState({});
    const [state, dispatch] = useReducer(reducer, initialState);
    const [profileURL, setprofileURL] = useState();
    const [edit, setEdit] = useState(false);
    const [id, setId] =useState("");
    const [successMsg, setSuccessMsg] =useState("");

    useEffect(() => {
        var userData = localStorage.getItem(USER_AUTH_DATA);
        setLoggedInData(JSON.parse(userData));
        fetchUser(JSON.parse(userData));
    }, []);

    useEffect(() => {
    }, [loggedInData]);

    const handleClose = () => {
        props.handleClose(false);
    }

    const fileUploadRes=() => (resp) =>{
        try {
            /* if(lab.labdata[idx].reportURL){
            const finalUrls = lab.labdata[idx].reportURL.filter((item) => item.name !== resp.name);
            lab.labdata[idx].reportURL=finalUrls;
            } */
        } catch (err) {
        }
    }

    const fetchUser = async (data) => {
         await retriveProfile(data._id).then((resp) => {
            if (resp.suc) {
                console.log(" Res == "+JSON.stringify(resp));
                let data = resp.res;
                dispatch({ type: 'firstName', payload: data.firstName });
                dispatch({ type: 'lastName', payload: data.lastName });
                dispatch({ type: 'mobile', payload: data.contactNum });
                dispatch({ type: 'email', payload: data.email });
                setId(data._id);
            }
        } );
    }

    const updateUser = async () => {
        let payload={
            _id : id,
            firstName : state.firstName,
            lastName : state.lastName,
            email: state.email,
            contactNum: state.mobile
        }
        console.log("Req : "+JSON.stringify(payload));
        await creation(payload).then((resp) => {
            if (resp.suc) {
                setSuccessMsg("Updated successfully");
                setEdit(false);
            }
        } );
    }

    const handleChange = (e) => {
        const { id, value } = e.target;
        if (id === "food") {
            if (state.foodReactions) {
                dispatch({ type: 'foodReactions', payload: false })
            } else {
                dispatch({ type: 'foodReactions', payload: true })
            }
        } else {
            dispatch({ type: id, payload: value });
        }
    }


    return (
        <>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                size="lg"
                keyboard={false}>
                <Modal.Header closeButton>
                    <Modal.Title>{loggedInData.firstName + " " + loggedInData.lastName}</Modal.Title>
                    <Button
                        size="xs"
                        variant="outline-primary"
                        onClick={() => setEdit(true)}
                        type="submit"
                        loading={state.loadOfice}
                    > Edit</Button>
                </Modal.Header>
                <Modal.Body>
                    <div className="container">
                        <div className="cardMainlayout pt-0 mt-0">
                            <div className="contentArea">
                                <Form.Row>
                                    <Form.Group as={Col} xs={3} controlId="firstName">
                                        <Form.Control type="text" placeholder="First Name" autoComplete="off" value={state.firstName} onChange={handleChange} disabled={!edit}/>
                                        <Form.Label>First Name <sup>*</sup></Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} xs={3} controlId="lastName">
                                        <Form.Control type="text" placeholder="Last Name" autoComplete="off" value={state.lastName} onChange={handleChange}  disabled={!edit}/>
                                        <Form.Label>Last Name <sup>*</sup></Form.Label>
                                    </Form.Group>
                                </Form.Row>

                                <Form.Row>
                                    <Form.Group as={Col} xs={3} controlId="mobile">
                                        <Form.Control type="text" placeholder="Mobile" autoComplete="off" value={state.mobile} onChange={handleChange} disabled={!edit} />
                                        <Form.Label>Mobile <sup>*</sup></Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} xs={3} controlId="email">
                                        <Form.Control type="text" placeholder="Email" autoComplete="off" value={state.email} onChange={handleChange} disabled={!edit} />
                                        <Form.Label>Email <sup>*</sup></Form.Label>
                                    </Form.Group>
                                </Form.Row>
                                {edit && <>
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    onClick={updateUser}
                                    type="submit"
                                    loading={state.loadOfice}
                                > Update</Button>
                                <Button
                                    size="xs"
                                    variant="outline-primary"
                                    onClick={() => setEdit(false)}
                                    type="submit"
                                    loading={state.loadOfice}
                                > Cancel</Button> </>
                                }
                            </div>
                        </div>
                    </div>

                </Modal.Body>
            </Modal>
        </>)
}

export default Profile
