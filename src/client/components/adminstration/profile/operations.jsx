import React from 'react';
import ProfileService from '../../../services/administration/ProfileService'

/**
 * @description: CRUD operations for Patient
 * @author: Praveen Varma
 * @param {*}
 * @function Lab
 * @date : 27-02-2021
 */



// ========================================================================
//  Branch Create Operation
//  This will create Branch and save the details 
// ========================================================================
export const saveProfile = async (obj) => {
    let finalRes=[];
    await ProfileService.saveProfile(obj).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

export const retriveProfile = async (logedInId) => {
    let finalRes=[];
    await ProfileService.retriveProfile(logedInId).then(response => {
        finalRes = response.data;
    });
    return Promise.resolve(finalRes);
}

