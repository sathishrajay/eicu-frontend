import axios from "axios";
import api from "../../../api.json";

const API_URL = process.env.REACT_APP_B2B_URL;

var cities = require('../../components/masters/Cities.json');
export const USER_AUTH_DATA = "userAuthData";

/**
 * @description: Integrated the LocationService
 * @author: Praveen Varma
 * @param {*} req
 */
class LocationService {
  async fetchLocations() {
    return await axios.get(api.fetchLocations);
  }

  async fetchMenus() {
    var userData = localStorage.getItem(USER_AUTH_DATA);
    let loggedInId = JSON.parse(userData)._id;
    return await axios.get(api.fetchMenus+"/"+loggedInId);
  }

  async nameSearch(req) {
    return await axios.post(api.nameSearch, req);
  }

  constructJsonDataForTree(testData){
    let stateChild = [];
    let cityChild = [];
    let hspChild = [];
    let bedChild = [];
    for (let d in testData) {
        let statedata = testData[d];
        let cityArray = statedata.cities;
        for (let cD in cityArray) {
            let cityData = cityArray[cD];
            let hspArray = cityData.hospitals;
                for(let hsp in hspArray){
                   let hspD = hspArray[hsp];
                   let bedArray = hspD.bed_details;
                        for(let bed in bedArray){
                            let bedD =bedArray[bed];
                            let bedInfo = {
                                isBed:true,
                                bed_id:bedD.bed_id,
                                hospital_id:hspD.id,
                                hcode:hspD.code,
                                email:hspD.email !== undefined ? hspD.email : "",
                                name: (bedD.name !== null && bedD.name !== '') ? ("Bed "+bedD.bed_id +" ("+bedD.name+")") : ("Bed "+bedD.bed_id+ " ("+ bedD.status+")"),
                                status:bedD.status
                            }
                            bedChild.push(bedInfo);
                        }
                   let hs={
                    id:hspD.id,
                    name: hspD.name,
                    email:hspD.email !== undefined ? hspD.email : "",
                    hcode:hspD.code,
                    children: bedChild
                   }
                   bedChild=[];
                   hspChild.push(hs)
                }
            let chdchildren = {
                name: this.fetchCityName(cityData.cityName),
                children:hspChild
            }
            hspChild=[];
            cityChild.push(chdchildren);
        }
        let inP = {
            name: this.fetchStateName(statedata.stateName),
            children: cityChild
        }
        cityChild= [];
        stateChild.push(inP);
    }
    let dataFinal = {
        name: "Locations",
        active:true,
        toggled:true,
        children: stateChild
    }
    return dataFinal;
  }


  fetchCityName = (cityCode) => {
    let cityArray = cities.filter(item => item.ccode === cityCode);
    if(cityArray.length>0){
      return cityArray[0].cname;
    }else{
      return '';
    }
  }
  
  fetchStateName = (stateCode) => {
    if(stateCode === "TS") {return "Telangana"};
    if(stateCode === "AP") {return "Andhra Pradesh"};
    return stateCode;
  } 


  trendingDataMaster = () => {
    let data = [
        {value : "totalLeucocytes", label : "Total Leucocytes"},
        {value : "haemoglobin", label : "Haemoglobin"},
        {value : "platelets", label: "Platelets"},
        {value : "bloodUrea",label : "Blood Urea"},
        {value  :"serCreatinine", label : "Ser. Creatinine" },
        {value : "totalBilirubin",label : "Total Bilirubin"},
        {value : "sgptAlt", label : "ALT/SGPT"},
        {value : "sgptAst",label : "AST/SGPT" },
        {value :"sodium", label : "Sodium" },
        {value : "potassium",label : "Potassium"},
        {value : "bicarbonate",label : "Bicarbonate"},
        {value : "chlorides", label : "Chlorides"},
        {value : "inr",label : "INR"}
    ];
    return data;
  }

}

export default new LocationService();
