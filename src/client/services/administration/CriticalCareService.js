import axios from "axios";
import api from "../../../api.json";

const API_URL = process.env.REACT_APP_B2B_URL;

/**
 * @description: Integrated the Critical care service
 * @author: Praveen Varma
 * @param {*} req
 */
class CriticalCareService {
  async saveCriticalCareInfo(obj) {
    return await axios.post(api.saveCriticalCareInfo, obj);
  }

  async updateCriticalCareInfo(obj) {
    return await axios.put(api.updateCriticalCareInfo + obj.id, obj);
  }

  async retriveCriticalCareInfo(obj) {
    return await axios.get(api.retriveCriticalCareInfo + "/" + obj.patientId);
  }

  async fetchHistoryBasedId(obj) {
    return await axios.get(api.retrieveCCHistory + "/" + obj._id);
  }
}

export default new CriticalCareService();
