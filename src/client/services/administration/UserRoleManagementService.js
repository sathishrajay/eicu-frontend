import axios from "axios";
import api from "../../../api.json";

const API_URL = process.env.REACT_APP_B2B_URL;

/**
 * @description: Integrated the Hospital service
 * @author: Lakshmi
 * @param {*} req
 */
class UserRoleManagementService {
  
  async getUserList() {
    return await axios.post(api.getUsers);
  }

  async saveUser(obj) {
    return await axios.post(api.saveUser, obj);
  }

  async getRolesList() {
    return await axios.get(api.getRoles);
  }

  async saveRole(obj) {
    return await axios.post(api.saveRole, obj);
  }

  async getRoleMenus() {
    return await axios.get(api.getRoleMenu);
  }
}

export default new UserRoleManagementService();
