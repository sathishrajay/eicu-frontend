import axios from "axios";
import api from "../../../api.json";

/**
 * @description: Integrated the Hospital service
 * @author: Praveen Varma
 * @param {*} req
 */
class ProfileService {
  
  async saveProfile(obj) {
    return await axios.post(api.updateUser, obj);
  }

  async retriveProfile(logedInId) {
    return await axios.get(api.fetchUser+"/"+logedInId);
  }

}

export default new ProfileService();
