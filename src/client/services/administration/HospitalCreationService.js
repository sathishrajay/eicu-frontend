import axios from "axios";
import api from "../../../api.json";

const API_URL = process.env.REACT_APP_B2B_URL;

/**
 * @description: Integrated the Hospital service
 * @author: Lakshmi
 * @param {*} req
 */
class HospitalCreationService {
  async getHospitalsList() {
    return await axios.get(api.retriveHospital);
  }

  async saveHospital(obj) {
    return await axios.post(api.saveHospital, obj);
  }

  async updateHospital(obj) {
    return await axios.post(
      `${api.updateHosp}/${obj._id}`,
      obj
    );
  }
}

export default new HospitalCreationService();
