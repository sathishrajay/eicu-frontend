import axios from "axios";
import api from "../../../api.json";

const API_URL = process.env.REACT_APP_B2B_URL;

/**
 * @description: Integrated the Hospital service
 * @author: Lakshmi
 * @param {*} req
 */
class PatientStasisService {
  
  async gerMrCodes(obj) {
    return await axios.post(api.fetchMrCodes, obj);
  }

  async authentication() {
    return await axios.get(api.checkAuthenticate);
  }

  async getPatientData(code,key) {
   return await axios.get(api.getPatientData+code, {
        headers: {"authtoken":key}
      });
  }

  async callRenewAuth() {
    return await axios.get(api.getNewAuthKey);
  }
}

export default new PatientStasisService();
