import axios from "axios";
import api from "../../../api.json";

const API_URL = process.env.REACT_APP_B2B_URL;

/**
 * @description: Integrated the Hospital service
 * @author: Lakshmi
 * @param {*} req
 */
class PatientInfoService {
  async savePatientInfo(obj) {
    return await axios.post(api.savePatient, obj);
  }

  async retrivePatientInfo(obj) {
    return await axios.post(api.retrivePatient, obj);
  }

  async dischargePatient(obj) {
    return await axios.post(api.dischargePatient, obj);
  }

  async saveLabInfo(obj) {
    return await axios.post(api.saveLab, obj);
  }

  async retriveLabInfo(obj) {
    return await axios.post(api.getLab, obj);
  }
  async saveDrug(obj) {
    return await axios.post(api.saveDrug, obj);
   }
  async getDrugData(obj) {
    return await axios.post(api.getDrug, obj);
  }

  async stopDrug(obj) {
    return await axios.post(api.stopDrug, obj);
  }

  async fetchLabTrendingData(obj) {
    return await axios.post(api.fetchLabTrendingData, obj);
  }

  async saveSpoc(obj) {
    return await axios.post(api.saveSpoc, obj);
   }

   async getSpocData(id) {
    return await axios.get(api.getSpocData+id);
  }

  async senPrescription(obj) {
    return await axios.post(api.sendPrescription, obj);
  }

  async fetchOpdHistoryBasedId(obj) {
    return await axios.get(api.fetchOpdHistoryView + "/" + obj._id);
  }
}

export default new PatientInfoService();
