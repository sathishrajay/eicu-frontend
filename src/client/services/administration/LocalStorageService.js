import React from 'react'
export const LOCATION_PAYLOAD = 'locationPayload'

class LocalStorageService {
    constructor() {
    }

    saveLocationPayload(info) {
        localStorage.setItem(LOCATION_PAYLOAD, JSON.stringify(info));
    }

    fetchLocationPayload() {
        if (localStorage.getItem(LOCATION_PAYLOAD) !== undefined && localStorage.getItem(LOCATION_PAYLOAD) !== null) {
            return JSON.parse(localStorage.getItem(LOCATION_PAYLOAD));
        } else {
            return null;
        }
    }

    deleteSelectedStorage(key) {
        let result ={};
        if (localStorage.getItem(LOCATION_PAYLOAD) !== null && localStorage.getItem(LOCATION_PAYLOAD).length > 0) {
            let listData = JSON.parse(localStorage.getItem(LOCATION_PAYLOAD));
            result = listData.filter(function (obj) {
                return listData[obj] !== key 
            })
        }
        return result;
    }

    updateData(state, data){
        if (localStorage.getItem(LOCATION_PAYLOAD) !== null && localStorage.getItem(LOCATION_PAYLOAD).length > 0) {
         let exsitngData = localStorage.getItem(LOCATION_PAYLOAD);
         exsitngData.key(data)
        }
    }
}
export default new LocalStorageService()
