import axios from "axios";
import api from "../../../api.json";

const API_URL = process.env.REACT_APP_B2B_URL;

export const USER_NAME_SESSION_ATTRIBUTE_NAME = "authenticatedUser";

/**
 * @author: Lakshmi
 * @description: Integrated the Login and Roles service
 * @param {*} Service
 */
class AuthenticationService {
  async userLogin(userData) {
    return await axios.post(api.login, userData);
  }

  async userForgotPw(email) {
    return await axios.post(api.forgotPwd, {
      email: email,
    });
  }

  async changePassowrd(changeData) {
    return await axios.post(api.changePwdUrl, changeData);
  }

  async getNotifications(usrId) {
    return await axios.get(api.fetchNotofication+"/"+usrId);
  }

  async clearNotifications(usrId) {
    return await axios.get(api.clearNotification+"/"+usrId);
  }

  async updateNotifications(usrId) {
    return await axios.post(api.updateNotification+"/"+usrId);
  }

  
  logout() {
    sessionStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
  }
}

export default new AuthenticationService();
