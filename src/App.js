import React, { Component, useContext ,useState } from 'react';
import './App.scss';
import '../src/client/assets/animation/animation.css';
import RouterApp from './client/components/router';
export const Context = React.createContext();
export const LocationContext = React.createContext();

function App(props) {
  const [context, setContext] = useState({logindata:""});
  const [locationContext, setLocationContext] = useState({locations:""});

  return (

    <>
      {console.log('..REACT_APP_BASE_URL...............', process.env.REACT_APP_BASE_URL,process.env.NODE_ENV)}
      <Context.Provider value={[context, setContext]}>
      <LocationContext.Provider value={[locationContext, setLocationContext]}>
      <RouterApp />
      </LocationContext.Provider>
      </Context.Provider>
    </>
  );
}




export default App;